package bongolive.apps.proregister.db;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by USER on 27-Sep-16.
 */
public class VolleyCode {

    private void requestData(final String email, final String password) {

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST, "URL_HERE", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.trim().contains("ARRAY_NAME_HERE")) {

//                    jsonString = response.toString();
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        JSONArray jsonArray = jsonObject.getJSONArray("server_login");
                        int count = 0;
                        while (count < jsonArray.length()) {
                            JSONObject jo = jsonArray.getJSONObject(count);
                            //Capture id and name from json object
                            String id = jo.getString("id");
                            String name = jo.getString("name");
                            System.out.println("DATA: " + id + " " + name);
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (response.trim().contains("server_error")) {
                    //Display the message when the user inf not correct
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                show error message here
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
