package bongolive.apps.proregister;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;

import java.io.File;
import java.util.Locale;

import bongolive.apps.proregister.db.ClearSyncedFiles;
import bongolive.apps.proregister.db.ExportDatabase;
import bongolive.apps.proregister.utils.Constants;

public class FragmentManagerClass extends AppCompatActivity {

    public static final String TAG = FragmentManagerClass.class.getName();
    static AppPreference appPreference;
    private Toolbar mToolbar;
    boolean tabletSize =false;
    static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.fragment_main);
        appPreference = new AppPreference(this);
        setUpToolbar();
        context = this;

        SettingsFragment settingFragment = SettingsFragment.newInstance();
        getFragmentManager().beginTransaction().add( R.id.fragment_container, settingFragment ).commit();
    }

    private void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        try {
            setSupportActionBar(mToolbar);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
//                actionBar.setDisplayShowHomeEnabled(true);
            }
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    public static class SettingsFragment extends PreferenceFragment implements SharedPreferences
            .OnSharedPreferenceChangeListener {

        ListPreference listsync,listlang;
        CheckBoxPreference chexport,chfiles,chprint,chksendlogs;

        public SettingsFragment() {
        }

        public static SettingsFragment newInstance() {
            SettingsFragment fragment = new SettingsFragment();
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            listlang = (ListPreference)findPreference(Constants.PREFLANG);
            listsync = (ListPreference)findPreference(Constants.PREFSYNC);
            chexport = (CheckBoxPreference) findPreference(Constants.EXPORT_DB);
            chfiles = (CheckBoxPreference) findPreference(Constants.CLEARFILES);
            chprint = (CheckBoxPreference) findPreference(Constants.PREFRECEIPT);
            chksendlogs = (CheckBoxPreference) findPreference(Constants.SENDLOGS);

            boolean export = appPreference.getSettings(Constants.EXPORT_DB);
            final boolean files = appPreference.getSettings(Constants.CLEARFILES);
            chexport.setChecked(export);
            if(export)
                chexport.setSelectable(true);


            chexport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                        ExportDatabase ex = new ExportDatabase(context, chexport);
                        ex.execute();
                    return false;
                }
            });
            chfiles.setChecked(files);
            if(files)
                chfiles.setSelectable(true);
            chfiles.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                        new ClearSyncedFiles(context,chfiles).execute();
                    return false;
                }
            });

            chprint.setChecked(appPreference.getSettings(Constants.PREFRECEIPT));
            chksendlogs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    if(chksendlogs.isChecked()) {
                        ExportDatabase ex = new ExportDatabase(context, chexport);
                        ex.execute();
                        File sdcard = Environment.getExternalStorageDirectory() ;
                        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                        if (!folder.exists()) {
                            chksendlogs.setChecked(false);
                            return true;
                        } else {
                            String str = Constants.getIMEINO(context);
                            File file = new File(folder.getAbsoluteFile(), str);
                            Constants.sendEmail(context, file);
                            chksendlogs.setChecked(files);
                        }
                    }
                    return false;
                }
            });

            setListPreferenceData(listlang);
            setListPreferenceDataSync(listsync);
            listlang.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    setListPreferenceData(listlang);
                    return false;
                }
            });

            listsync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    setListPreferenceDataSync(listsync);
                    return false;
                }
            });
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if(key.equals(Constants.PREFLANG))
            {
                Preference langpref = findPreference(key);
                langpref.setSummary(sharedPreferences.getString(key,""));
            }
            if(key.equals(Constants.PREFSYNC))
            {
                Preference syncpref = findPreference(key);
                syncpref.setSummary(sharedPreferences.getString(key,""));
            }
        }

        protected static void setListPreferenceData(ListPreference lp) {

            lp.setEntries(R.array.pref_default_language_titles);
            lp.setEntryValues(R.array.pref_default_language_values);
            lp.setDefaultValue(0);
        }

        protected static void setListPreferenceDataSync(ListPreference lp) {

            lp.setEntries(R.array.pref_sync_frequency_titles);
            lp.setEntryValues(R.array.pref_sync_frequency_values);
            lp.setDefaultValue(2);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        appPreference = new AppPreference(getApplicationContext());
        appPreference.setDefaultLanguage();
        String languageToLoad = appPreference.getDefaultLanguage();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        newConfig = new Configuration();
        newConfig.locale = locale;
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.i("event", "captured");
            startActivity(new Intent(this, MainScreen.class));
            finish();
            return true;
        } else
            return super.onKeyUp(keyCode, event);
    }
}
