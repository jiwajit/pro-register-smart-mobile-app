/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ItemListAdapter extends BaseAdapter {
    int action;
    private LayoutInflater mInflater;
    boolean tabletsize ;
    private List<ItemList> mItems = new ArrayList<ItemList>();

    public ItemListAdapter() {

    }

    public ItemListAdapter(Context context, List<ItemList> items, int source) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        action = source;
        tabletsize = context.getResources().getBoolean(R.bool.isTablet);

    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ItemViewHolder();
            holder.txtName5 = (TextView) convertView.findViewById(R.id.txtitem5);
            holder.txtindex = (TextView) convertView.findViewById(R.id.txtindex);
            holder.txtName1 = (TextView) convertView.findViewById(R.id.txtitem1);
            holder.txtName2 = (TextView) convertView.findViewById(R.id.txtitem2);
            holder.txtName3 = (TextView) convertView.findViewById(R.id.txtitem3);
            holder.txtName4 = (TextView) convertView.findViewById(R.id.txtitem4);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        ItemList cl = mItems.get(position);

            holder.txtName5.setText(cl.getItemname5());
            holder.txtName1.setText(cl.getItemname1());
            holder.txtName2.setText(cl.getItemname2());
            holder.txtName3.setText(cl.getItemname3());
            holder.txtindex.setText(String.valueOf(position + 1));
            holder.txtName4.setText(cl.getItemname4());
            long id = cl.getId();
            return convertView;

    }

    static class ItemViewHolder {
        TextView txtName5;
        TextView txtindex;
        TextView txtName1;
        TextView txtName2;
        TextView txtName3;
        TextView txtName4;
    }

}
