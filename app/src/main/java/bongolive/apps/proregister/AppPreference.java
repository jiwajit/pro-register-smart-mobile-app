/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import bongolive.apps.proregister.db.Customers;
import bongolive.apps.proregister.utils.Constants;


/**
 * Created by nasznjoka on 1/2/2015.
 */
public class AppPreference {
    private SharedPreferences _sharedPrefs, _settingsPrefs;
    private SharedPreferences.Editor _prefsEditor;

    public AppPreference(Context context) {
        this._sharedPrefs = context.getSharedPreferences(Constants.CLIENT_DATA, Activity.MODE_MULTI_PROCESS);
        PreferenceManager.setDefaultValues(context, R.xml.preferences, false);
        this._settingsPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this._prefsEditor = _sharedPrefs.edit();
    }

    public int getReceiptStatu() {
        return _sharedPrefs.getInt(Constants.PREFRECEIPT, 0);
    }

    public int getVatStatus() {
        return _sharedPrefs.getInt(Constants.PREFTAX, 0);
    }

    public int getDiscountStatus() {
        return _sharedPrefs.getInt(Constants.PREFDISCOUNT, 0);
    }

    public String getDefaultLanguage() {
        return _settingsPrefs.getString(Constants.PREFLANG, "");
    }

    public String getDefaultSync() {
        return _settingsPrefs.getString(Constants.PREFSYNC, "");
    }

    public String getDefaultCurrency() {
        return _settingsPrefs.getString(Constants.PREFCURRENCY, "");
    }

    public int getPrintWithNoPayment() {
        return _sharedPrefs.getInt(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT, 0);
    }

    public boolean check_firstrun() {
        return _sharedPrefs.getBoolean(Constants.FIRSTRUN, true);
    }

    public int get_accounttype() {
        return _sharedPrefs.getInt(Constants.DEVICE_ACCOUNTTYPE, 2);
    }

    public String[] getAccountInfo() {
        String[] info = {
                _sharedPrefs.getString(Constants.BUSINESSNAME, ""),
                _sharedPrefs.getString(Constants.CONTACTP, ""),
                _sharedPrefs.getString(Constants.PHONE, "")
        };
        return info;
    }

    public String[] getLastLocation() {
        String[] info = {
                _sharedPrefs.getString(Customers.LAT, "0"),
                _sharedPrefs.getString(Customers.LONGTUDE, "0")
        };
        return info;
    }


    public String getLastSync() {
        return _sharedPrefs.getString(Constants.LASTSYNC, "");
    }

    public String getSubPhoto() {
        return _sharedPrefs.getString(Constants.SUBPHOTO, "");
    }

    public String getSubIdPhoto() {
        return _sharedPrefs.getString(Constants.SUBIDPHOTO, "");
    }

    public String getSubSignPhoto() {
        return _sharedPrefs.getString(Constants.SUBSIGNPHOTO, "");
    }

    public String getAuthkey() {
        return _sharedPrefs.getString(Constants.AUTHTOKEN, "");
    }

    public boolean getSettings(String string) {
        return _sharedPrefs.getBoolean(string, false);
    }

    public String getTitle() {
        return _sharedPrefs.getString(Customers.TITLE, "");
    }

    public String getJob() {
        return _sharedPrefs.getString(Customers.JOB, "");
    }

    public String getGender() {
        return _sharedPrefs.getString(Customers.GENDER, "");
    }

    public String getFname() {
        return _sharedPrefs.getString(Customers.FNAME, "");
    }

    public String getSname() {
        return _sharedPrefs.getString(Customers.SURNAME, "");
    }

    public String getMname() {
        return _sharedPrefs.getString(Customers.MNAME, "");
    }

    public String getRegion() {
        return _sharedPrefs.getString(Customers.REGION, "");
    }

    public String getDistrict() {
        return _sharedPrefs.getString(Customers.DISTRICT, "");
    }

    public String getStreet() {
        return _sharedPrefs.getString(Customers.STREET, "");
    }

    public String getAddress() {
        return _sharedPrefs.getString(Customers.ADDRESS, "");
    }

    public String getBdate() {
        return _sharedPrefs.getString(Customers.BDATE, "");
    }

    public String getSmartNo() {
        return _sharedPrefs.getString(Customers.MOBILE, "");
    }

    public String getSerial() {
        return _sharedPrefs.getString(Customers.SIMSERIAL, "");
    }

    public String getImei() {
        return _sharedPrefs.getString(Customers.IMEI, "");
    }/* user  */

    public String getIdType() {
        return _sharedPrefs.getString(Customers.IDTYPE, "");
    }

    public String getIdNo() {
        return _sharedPrefs.getString(Customers.IDNO, "");
    }

    public String getCitizenship() {
        return _sharedPrefs.getString(Customers.CITIZENSHIP, "");
    }/* id  */


    public String getTel() {
        return _sharedPrefs.getString(Customers.TELEPHONE, "");
    }

    public String getEmail() {
        return _sharedPrefs.getString(Customers.EMAIL, "");
    }/* contacts  */

    public String getAudio() {
        return _sharedPrefs.getString(Constants.VOICENOTE, "");
    }/* voice note  */

    public void saveVatChoice(int id) {
        _prefsEditor.putInt(Constants.PREFTAX, id);
        _prefsEditor.commit();
    }

    public void setDefaultLanguage() {
        _sharedPrefs.edit().putString(Constants.PREFLANG, "sw");
        _sharedPrefs.edit().commit();
    }

    public void saveDiscountStatus(int id) {
        _prefsEditor.putInt(Constants.PREFDISCOUNT, id);
        _prefsEditor.commit();
    }

    public void savePrintWithNoPaymentStatus(int id) {
        _prefsEditor.putInt(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT, id);
        _prefsEditor.commit();
    }

    public void setDefaultCurrency(String id) {
        _prefsEditor.putString(Constants.PREFCURRENCY, id);
        _prefsEditor.commit();
    }

    public void saveSubscriberPhoto(String id) {
        _prefsEditor.putString(Constants.SUBPHOTO, id);
        _prefsEditor.commit();
    }

    public void saveLocation(String[] loc) {
        _prefsEditor.putString(Customers.LAT, loc[0]);
        _prefsEditor.putString(Customers.LONGTUDE, loc[1]);
        _prefsEditor.commit();
    }

    public void saveSettings(boolean[] loc) {
        _prefsEditor.putBoolean(Constants.EXPORT_DB, loc[4]);
        _prefsEditor.putBoolean(Constants.CLEARFILES, loc[3]);
        _prefsEditor.putBoolean(Constants.RECORD_AUDIO, loc[2]);
        _prefsEditor.putBoolean(Constants.TRACKING_GPS, loc[0]);
        _prefsEditor.putBoolean(Constants.PREFRECEIPT, loc[1]);
        _prefsEditor.commit();
    }

    public void saveSubscriberIdPhoto(String id) {
        _prefsEditor.putString(Constants.SUBIDPHOTO, id);
        _prefsEditor.commit();
    }

    public void saveAudio(String id) {
        System.out.println(" path is " + id);
        _prefsEditor.putString(Constants.VOICENOTE, id);
        _prefsEditor.commit();
    }

    public void saveSubscriberSignPhoto(String id) {
        _prefsEditor.putString(Constants.SUBSIGNPHOTO, id);
        _prefsEditor.commit();
    }

    public void clearPhotos(String source) {
        if (source == null) {
            _prefsEditor.remove(Constants.SUBPHOTO);
            _prefsEditor.remove(Constants.SUBIDPHOTO);
            _prefsEditor.remove(Constants.SUBSIGNPHOTO);
        } else {
            if (source.equals(Constants.SUBPHOTO))
                _prefsEditor.remove(Constants.SUBPHOTO);
            if (source.equals(Constants.SUBIDPHOTO))
                _prefsEditor.remove(Constants.SUBIDPHOTO);
            if (source.equals(Constants.SUBSIGNPHOTO))
                _prefsEditor.remove(Constants.SUBSIGNPHOTO);
        }
        _prefsEditor.commit();
    }

    public void clearAddress() {
        _prefsEditor.remove(Constants.PREFADDRESS);
        _prefsEditor.commit();
    }

    public void clearAudio() {
        _prefsEditor.remove(Constants.VOICENOTE);
        _prefsEditor.commit();
    }

    public void clearStates() {
        /*
        * subscriber info
        * */
        _prefsEditor.remove(Customers.TITLE);
        _prefsEditor.remove(Customers.GENDER);
        _prefsEditor.remove(Customers.FNAME);
        _prefsEditor.remove(Customers.SURNAME);
        _prefsEditor.remove(Customers.MNAME);
        _prefsEditor.remove(Customers.REGION);
        _prefsEditor.remove(Customers.DISTRICT);
        _prefsEditor.remove(Customers.STREET);
        _prefsEditor.remove(Customers.ADDRESS);
        _prefsEditor.remove(Customers.JOB);
        _prefsEditor.remove(Customers.MOBILE);
        _prefsEditor.remove(Customers.BDATE);
        _prefsEditor.remove(Customers.SIMSERIAL);
        _prefsEditor.remove(Customers.IMEI);/* user  */

        /*
        * subscriber id
        * */
        _prefsEditor.remove(Customers.IDNO);
        _prefsEditor.remove(Customers.IDTYPE);
        _prefsEditor.remove(Customers.CITIZENSHIP);/* id  */

        /**
         * subscriber contacts
         */
        _prefsEditor.remove(Customers.TELEPHONE);
        _prefsEditor.remove(Customers.EMAIL);/* user  */
        _prefsEditor.commit();
    }

    public void clearIDINFO(){
        /*
        * subscriber id
        * */
        _prefsEditor.remove(Customers.IDNO);
        _prefsEditor.remove(Customers.IDTYPE);
        _prefsEditor.remove(Customers.CITIZENSHIP);/* id  */
        _prefsEditor.commit();
    }

    public void clearProductCode() {
        _prefsEditor.remove(Constants.PREFPRODUCTSKU);
        _prefsEditor.commit();
    }

    /* user  */
    public void saveTitle(String address) {
        _prefsEditor.putString(Customers.TITLE, address);
        _prefsEditor.commit();
    }

    public void saveGender(String address) {
        _prefsEditor.putString(Customers.GENDER, address);
        _prefsEditor.commit();
    }

    public void saveFn(String address) {
        _prefsEditor.putString(Customers.FNAME, address);
        _prefsEditor.commit();
    }

    public void saveSn(String address) {
        _prefsEditor.putString(Customers.SURNAME, address);
        _prefsEditor.commit();
    }

    public void saveMn(String address) {
        _prefsEditor.putString(Customers.MNAME, address);
        _prefsEditor.commit();
    }

    public void saveReg(String address) {
        _prefsEditor.putString(Customers.REGION, address);
        _prefsEditor.commit();
    }

    public void saveDist(String address) {
        _prefsEditor.putString(Customers.DISTRICT, address);
        _prefsEditor.commit();
    }

    public void saveStreet(String address) {
        _prefsEditor.putString(Customers.STREET, address);
        _prefsEditor.commit();
    }

    public void saveAddress(String address) {
        _prefsEditor.putString(Customers.ADDRESS, address);
        _prefsEditor.commit();
    }

    public void saveBdate(String address) {
        _prefsEditor.putString(Customers.BDATE, address);
        _prefsEditor.commit();
    }

    public void saveJob(String address) {
        _prefsEditor.putString(Customers.JOB, address);
        _prefsEditor.commit();
    }

    public void savePhone(String address) {
        _prefsEditor.putString(Customers.MOBILE, address);
        _prefsEditor.commit();
    }

    public void saveSerial(String address) {
        _prefsEditor.putString(Customers.SIMSERIAL, address);
        _prefsEditor.commit();
    }

    public void saveImei(String address) {
        _prefsEditor.putString(Customers.IMEI, address);
        _prefsEditor.commit();
    }

    /* user  */
    /* id  */
    public void saveCitizen(String address) {
        _prefsEditor.putString(Customers.CITIZENSHIP, address);
        _prefsEditor.commit();
    }

    public void saveIdType(String address) {
        _prefsEditor.putString(Customers.IDTYPE, address);
        _prefsEditor.commit();
    }

    public void saveIdNo(String address) {
        _prefsEditor.putString(Customers.IDNO, address);
        _prefsEditor.commit();
    }
/* id  */

    public void saveTel(String address) {
        _prefsEditor.putString(Customers.TELEPHONE, address);
        _prefsEditor.commit();
    }

    public void saveEmail(String address) {
        _prefsEditor.putString(Customers.EMAIL, address);
        _prefsEditor.commit();
    }
/* id  */

    public void savePrintReceiptChoice(int id) {
        _prefsEditor.putInt(Constants.PREFRECEIPT, id);
        _prefsEditor.commit();
    }

    public String getDbnm() {
        return this._sharedPrefs.getString(Constants.KEY_DB, "");
    }

    public String getDbpath() {
        return this._sharedPrefs.getString(Constants.KEY_DBPATH, "");
    }


    public void reset_dbname() {
        this._sharedPrefs.edit().remove(Constants.KEY_DB).commit();
        Log.v("PATHRESETTING", "PATH is reset " + getDbnm());
    }

    public void reset_path() {
        this._sharedPrefs.edit().remove(Constants.KEY_DBPATH).commit();
        Log.v("PATHRESETTING", "PATH is reset " + getDbpath());
    }

    public void save_auth(String vals) {
        _prefsEditor.putString(Constants.AUTHTOKEN, vals);
        _prefsEditor.commit();
    }


    public void saveDbPath(String paramString) {
        this._prefsEditor.putString(Constants.KEY_DBPATH, paramString);
        this._prefsEditor.commit();
    }

    public void saveDbnm(String paramString) {
        this._prefsEditor.putString(Constants.KEY_DB, paramString);
        this._prefsEditor.commit();
    }


    public void save_buss(String vals) {
        _prefsEditor.putString(Constants.BUSINESSNAME, vals);
        _prefsEditor.commit();
    }


    public void save_contactp(String vals) {
        _prefsEditor.putString(Constants.CONTACTP, vals);
        _prefsEditor.commit();
    }


    public void save_phone(String vals) {
        _prefsEditor.putString(Constants.PHONE, vals);
        _prefsEditor.commit();
    }

    public void save_accounttype(int vals) {
        _prefsEditor.putInt(Constants.DEVICE_ACCOUNTTYPE, vals);
        _prefsEditor.commit();
    }

    public void clear_authentications() {
        _prefsEditor.remove(Constants.DEVICE_ACCOUNTTYPE);
        _prefsEditor.remove(Constants.AUTHTOKEN);
        _prefsEditor.remove(Constants.BUSINESSNAME);
        _prefsEditor.remove(Constants.CONTACTP);
        _prefsEditor.remove(Constants.PHONE);
        _prefsEditor.commit();
    }

    public void set_firstrun() {
        _prefsEditor.putBoolean(Constants.FIRSTRUN, false);
        _prefsEditor.commit();
    }

    public void save_last_sync(String date) {
        _prefsEditor.putString(Constants.LASTSYNC, date);
        _prefsEditor.commit();
    }


}
