package bongolive.apps.proregister;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.zj.btsdk.BluetoothService;
import com.zj.btsdk.PrintPic;

import org.apache.log4j.Logger;

import bongolive.apps.proregister.utils.Log4Helper;


public class PrintingScreen extends AppCompatActivity {
    MenuItem menupair,menuprint;

	private static final int REQUEST_ENABLE_BT = 2;
	BluetoothService mService = null;
	BluetoothDevice con_dev = null;
	private static final int REQUEST_CONNECT_DEVICE = 1;  //��ȡ�豸��Ϣ
	String[] subsriber = null, header = null;
    TextView txtmsg;
    ActionBar ab;
    Logger log = Log4Helper.getLogger(PrintingScreen.class.getSimpleName());
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.printing_screen);
        ab = getSupportActionBar();
		mService = new BluetoothService(this, mHandler);
		//�����������˳�����
		if( mService.isAvailable() == false ){
            Toast.makeText(this, getString(R.string.strnobluetooth), Toast.LENGTH_LONG).show();
            finish();
		}

		Intent intent = getIntent();
		subsriber = intent.getStringArrayExtra("subscriber");
        if(subsriber != null) {
			System.out.println("size is " + subsriber.length);
			header = new String[]{getString(R.string.title),getString(R.string.strgender),getString(R.string
                    .strname),getString(R.string.strmname),getString(R.string.strsname),
                    getString(R.string.strregion),getString(R.string.strdistict),
                    getString(R.string.strstreet),getString(R.string.stradd),getString(R.string.strbdate),getString(R.string
                    .strjob),getString(R.string.strphno),getString(R.string.strsimserial),getString(R.string.strimei),
                    getString(R.string.strcitizenship),getString(R.string.stridno),getString(R.string.idtype),
                    getString(R.string.strtel),getString(R.string.strmail)};
		}
        txtmsg = (TextView)findViewById(R.id.txtmsg);
        String msg = "";
        for(int i = 0; i < subsriber.length; i++){
            msg += header[i]+" :  "+subsriber[i]+"\n";
        }
        txtmsg.setText(msg);

        ab.setDisplayHomeAsUpEnabled(false);
	}

    @Override
    public void onStart() {
    	super.onStart();
    	//����δ�򿪣�������
		if( mService.isBTopen() == false)
		{
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}
    }
    
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mService != null)
			mService.stop();
		mService = null;
	}

    /**
     * ����һ��Handlerʵ�������ڽ���BluetoothService�෵�ػ�������Ϣ
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case BluetoothService.MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:   //������
                	Toast.makeText(getApplicationContext(), "Connect successful",
							Toast.LENGTH_SHORT).show();
                    menuprint.setVisible(true);
                    break;
                case BluetoothService.STATE_CONNECTING:  //��������
                	Log.d("��������", "��������.....");
                    break;
                case BluetoothService.STATE_LISTEN:     //�������ӵĵ���
                case BluetoothService.STATE_NONE:
                	Log.d("��������", "�ȴ�����.....");
                    break;
                }
                break;
            case BluetoothService.MESSAGE_CONNECTION_LOST:    //�����ѶϿ�����
                Toast.makeText(getApplicationContext(), "Device connection was lost",
						Toast.LENGTH_SHORT).show();
                menuprint.setVisible(false);
                break;
            case BluetoothService.MESSAGE_UNABLE_CONNECT:     //�޷������豸
            	Toast.makeText(getApplicationContext(), "Unable to connect device",
						Toast.LENGTH_SHORT).show();
            	break;
            }
        }
        
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_ENABLE_BT:      //���������
            if (resultCode == Activity.RESULT_OK) {   //�����Ѿ���
            	Toast.makeText(this, "Bluetooth open successful", Toast.LENGTH_LONG).show();
            } else {                 //�û������������
            	finish();
            }
            break;
        case  REQUEST_CONNECT_DEVICE:     //��������ĳһ�����豸
        	if (resultCode == Activity.RESULT_OK) {   //�ѵ�������б��е�ĳ���豸��
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);  //��ȡ�б������豸��mac��ַ
                con_dev = mService.getDevByMac(address);

                mService.connect(con_dev);
            }
            break;
        }
    } 

    //��ӡͼ��
    @SuppressLint("SdCardPath")
	private void printImage() {
    	byte[] sendData = null;
    	PrintPic pg = new PrintPic();
    	pg.initCanvas(384);     
    	pg.initPaint();
    	pg.drawImage(0, 0, "/mnt/sdcard/icon.jpg");
    	sendData = pg.printDraw();
    	mService.write(sendData);   //��ӡbyte������
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.print, menu);
            menupair = menu.findItem(R.id.action_pair);
            menuprint = menu.findItem(R.id.action_print);
            menuprint.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case android.R.id.home:
                mService.stop();
                finish();
            case R.id.action_pair:
                Intent serverIntent = new Intent(PrintingScreen.this,DeviceListActivity.class);      //��������һ����Ļ
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                break;
            case R.id.action_print:
                String msg = ""; CharSequence title = "";

                title = Html.fromHtml(getString(R.string.strtitle));
                for(int i = 0; i < subsriber.length; i++){
                    msg += header[i]+" :  "+subsriber[i]+"\n";
                }
                if( msg.length() > 0 ){
                    mService.sendMessage(title+" "+msg + "\n", "GBK");
                }
                break;
        }

        return false;

    }

}
