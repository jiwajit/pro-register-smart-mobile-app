/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
public class ItemList {
    String itemname1;
    String itemname2;
    String itemname3;
    String itemname4;
    String itemname5;
    String itemname6;
    long id;

    /*public ItemList(String n, String p, long i, String a){
        itemname = n;
        itemname1  = p ;
        itemname2 = a ;
        id = i ;
    }*/

    public ItemList(String itemname1, String itemname2, String itemname3, String itemname4, String itemname5, long id) {
        this.itemname1 = itemname1;
        this.itemname2 = itemname2;
        this.itemname3 = itemname3;
        this.itemname4 = itemname4;
        this.itemname5 = itemname5;
        this.id = id;
    }
    public ItemList(String itemname1, String itemname2, String itemname3, String itemname4, String itemname5, long
            id,String s6) {
        this.itemname1 = itemname1;
        this.itemname2 = itemname2;
        this.itemname3 = itemname3;
        this.itemname4 = itemname4;
        this.itemname5 = itemname5;
        this.id = id;
        this.itemname6 = s6;
    }

    public String getItemname1() {
        return itemname1;
    }

    public String getItemname2() {
        return itemname2;
    }

    public String getItemname3() {
        return itemname3;
    }

    public String getItemname4() {
        return itemname4;
    }

    public String getItemname5() {
        return itemname5;
    }
    public String getItemname6() {
        return itemname6;
    }

    public long getId() {
        return id;
    }
}
