/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proregister.db.Customers;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Validating;

public class SubscriberInformationAdapterDisplayer extends RecyclerView.Adapter<SubscriberInformationAdapterDisplayer.ItemViewHolder> implements View.OnClickListener{
    int action;
    Context context;
    boolean tabletsize ;
    private List<ItemList> mItems = new ArrayList<ItemList>();
    private View.OnClickListener onClickListener ;
    RecyclerView mRecyclerView;
    public SubscriberInformationAdapterDisplayer() {
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        ItemViewHolder vh = new ItemViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int i) {

        final ItemList cl = mItems.get(i);

        if(tabletsize) {
            holder.txtName5.setText(cl.getItemname5());
            holder.txtName1.setText(cl.getItemname1());
            holder.txtName2.setText(cl.getItemname2());
            holder.txtName3.setText(cl.getItemname3());
            holder.txtindex.setText(String.valueOf(i + 1));
            holder.txtName4.setText(cl.getItemname4());
            String s = cl.getItemname6();
            System.out.println("s is " + s);

            if (s.equals("REGISTRATION STATUS: 0")) {
                s = context.getString(R.string.strregstatus).toUpperCase(Locale.getDefault()) + ": " + context.getString(R
                        .string.strpending);
                holder.txtName6.setTextColor(context.getResources().getColor(R.color.pending));
            } else if (s.equals("REGISTRATION STATUS: 1")) {
                s = context.getString(R.string.strregstatus).toUpperCase(Locale.getDefault()) + ": " + context.getString(R
                        .string.strapproved);
                holder.txtName6.setTextColor(context.getResources().getColor(R.color.approved));
            } else if (s.equals("REGISTRATION STATUS: 2")) {
                s = context.getString(R.string.strregstatus).toUpperCase(Locale.getDefault()) + ": " + context.getString(R
                        .string.strrejected);
                holder.txtName6.setTextColor(context.getResources().getColor(R.color.rejected));
            }
            holder.txtName6.setText(s);
            System.out.println(" js is " + i);

        } else {

            String fnames = context.getString(R.string.strname).toUpperCase(Locale.getDefault()) +
                    ": " + cl.getItemname1().toUpperCase(Locale.getDefault());
            String snames = context.getString(R.string.strsname).toUpperCase(Locale.getDefault()) +
                    ": " + cl.getItemname2().toUpperCase(Locale.getDefault());
            String mobile = context.getString(R.string
                    .strphno).toUpperCase(Locale.getDefault()) + ": " + cl.getItemname3();
            String region = context.getString(R.string
                    .strregion).toUpperCase(Locale.getDefault()) + ": " + cl.getItemname4();
            String date = context.getString(R.string.strcreated).toUpperCase(Locale.getDefault()) +
                    ": " + cl.getItemname5();
            String status = context.getString(R.string.strregstatus).toUpperCase(Locale.getDefault());

            holder.txtName5.setText(fnames);
            holder.txtName1.setText(snames);
            holder.txtName2.setText(mobile);
            holder.txtName3.setText(region);
            holder.txtindex.setText(String.valueOf(i + 1));
            holder.txtName4.setText(date);
            String s = cl.getItemname6();
            System.out.println("s is " + s);
            if(!Validating.areSet(s))
                s = "0";

            if (s.equals("0")) {
                s =  context.getString(R.string.strpending);
                holder.txtName6.setTextColor(context.getResources().getColor(R.color.pending));
            } else if (s.equals("1")) {
                s = context.getString(R.string.strapproved);
                holder.txtName6.setTextColor(context.getResources().getColor(R.color.approved));
            } else if (s.equals("2")) {
                s = context.getString(R.string.strrejected);
                holder.txtName6.setTextColor(context.getResources().getColor(R.color.rejected));
            }
            holder.txtName6.setText(status +" : "+s);
        }
        final long id = cl.getId();
        holder.cardView.setOnClickListener(this);
    }


    public SubscriberInformationAdapterDisplayer(Context context2, List<ItemList> items, RecyclerView recyclerView) {
        context = context2;
        mItems = items;
        mRecyclerView = recyclerView;
        tabletsize = context2.getResources().getBoolean(R.bool.isTablet);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    @Override
    public void onClick(View v) {
        int j = mRecyclerView.getChildAdapterPosition(v);
        final ItemList cl = mItems.get(j);
        final long id = cl.getId();
        System.out.println("id is " + id);
        if (id > 0)
            showCustomer(id);

    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName5;
        public TextView txtindex;
        public TextView txtName1;
        public TextView txtName2;
        public TextView txtName3;
        public TextView txtName4;
        public TextView txtName6;
        public CardView cardView;

        public ItemViewHolder(View v) {
            super(v);
            txtName5 = (TextView) v.findViewById(R.id.txtitem5);
            txtindex = (TextView) v.findViewById(R.id.txtindex);
            txtName1 = (TextView) v.findViewById(R.id.txtitem1);
            txtName2 = (TextView) v.findViewById(R.id.txtitem2);
            txtName3 = (TextView) v.findViewById(R.id.txtitem3);
            txtName4 = (TextView) v.findViewById(R.id.txtitem4);
            txtName6 = (TextView) v.findViewById(R.id.txtitem6);
            cardView = (CardView) v.findViewById(R.id.card_view);
        }
    }
    private void showCustomer(final long i){
        LayoutInflater infl = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = infl.inflate(R.layout.list_items_details, null) ;
        boolean tabletsize;
        AppPreference appPreference = new AppPreference(context);

        final Dialog dialog = new Dialog(context, R.style.CustomDialog	);
        dialog.setCancelable(true);
        tabletsize = dialog.getContext().getResources().getBoolean(R.bool.isTablet);
        dialog.setContentView(view);
        final ArrayList<Customers> list;
        RecyclerView recyclerView;
        ItemListAdapterDetails adapter;
        RecyclerView.LayoutManager mLayoutManager;
        TextView txtheader;
        Button btnprint,btnedit;
        ImageView goback;

        txtheader = (TextView)dialog.findViewById(R.id.txtheader);
        btnprint = (Button)dialog.findViewById(R.id.btnSubmitPrint);
        btnedit = (Button)dialog.findViewById(R.id.btnEdit);
        goback = (ImageView)dialog.findViewById(R.id.goback);
        if(!appPreference.getSettings(Constants.PREFRECEIPT))
            btnprint.setEnabled(false);

        recyclerView = (RecyclerView)dialog.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(dialog.getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        list = Customers.getcustdetais(dialog.getContext(),i);
        System.out.println("clicked on " + i);
        System.out.println("EDIT SCREEN: ");

        adapter = new ItemListAdapterDetails(dialog.getContext(),list,dialog);
        recyclerView.setAdapter(adapter);
        final Customers cl = list.get(0);
        txtheader.setText(cl.getName());
        String status= cl.getStatus();

        if(status.equals("1"))
            btnedit.setVisibility(View.GONE);

        btnprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PrintingCenter.class);
                String[] finalvals = new String[]{cl.getTitle(), cl.getGender(), cl.getName(),cl.getMname(), cl
                        .getSname(), cl
                        .getRegion(), cl.getDistrict(), cl.getStreet(), cl.getAddress(), cl.getBdate(), cl.getOccuption(),
                        cl.getPoneno(), cl.getSimserial(), cl.getImei(), cl.getCitizenship(), cl.getIdno(), cl.getIdtype()
                        , cl.getTelephone(), cl.getEmail()};
                intent.putExtra("subscriber", finalvals);
                context.startActivity(intent);
            }
        });

        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] finalvals = new String[]{cl.getTitle(), cl.getGender(), cl.getName(), cl.getSname(), cl
                        .getRegion(), cl.getDistrict(), cl.getStreet(), cl.getAddress(), cl.getBdate(), cl.getOccuption(),
                        cl.getPoneno(), cl.getSimserial(), cl.getImei(), cl.getCitizenship(), cl.getIdno(), cl.getIdtype()
                        , cl.getTelephone(), cl.getEmail(), cl.getMname() };
//                System.out.println("EDIT SCREEN PHONE: "+cl.getPoneno().substring(3 ,12));
                AppPreference appPreference = new AppPreference(dialog.getContext());
                appPreference.saveTitle(cl.getTitle());
                appPreference.saveGender(cl.getGender());
                appPreference.saveFn(cl.getName());
                appPreference.saveSn(cl.getSname());
                appPreference.saveReg(cl.getRegion());
                appPreference.saveDist(cl.getDistrict());
                appPreference.saveStreet(cl.getStreet());
                appPreference.saveAddress(cl.getAddress());
                appPreference.saveBdate(cl.getBdate());
                appPreference.saveJob(cl.getOccuption());
                appPreference.savePhone(cl.getPoneno().substring(3 ,12));
                appPreference.saveSerial(cl.getSimserial());
                appPreference.saveImei(cl.getImei());
                appPreference.saveMn(cl.getMname());
                appPreference.saveCitizen(cl.getCitizenship());
                appPreference.saveIdNo(cl.getIdno());
                appPreference.saveIdType(cl.getIdtype());
                appPreference.saveTel(cl.getTelephone());
                appPreference.saveSubscriberPhoto(cl.getSubphoto());
                appPreference.saveSubscriberIdPhoto(cl.getIdphoto());
                appPreference.saveSubscriberSignPhoto(cl.getSignaturephoto());
                System.out.println("DBPHONE: "+cl.getPoneno().substring(3 ,12));
                appPreference.saveEmail(cl.getEmail());

                Intent intent = new Intent(dialog.getContext(), Register.class);
                intent.putExtra("update","update");
                dialog.getContext().startActivity(intent);
                Log.i("SUBSCRIBERINFORMATIONADAPTERVIEWER" ,"HERE");
            }
        });
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
