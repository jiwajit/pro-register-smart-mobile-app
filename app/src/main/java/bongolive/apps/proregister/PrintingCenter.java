package bongolive.apps.proregister;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.telpo.tps550.api.TelpoException;
import com.telpo.tps550.api.printer.ThermalPrinter;

import org.apache.log4j.Logger;

import java.io.File;

import bongolive.apps.proregister.utils.Log4Helper;


public class PrintingCenter extends AppCompatActivity {
    MenuItem menupair, menuprint;

    String[] subsriber = null, header = null;
    TextView txtmsg ,txtcopyright;
    ActionBar ab;
    public static String printContent;
    private int leftDistance = 0;
    private int lineDistance;
    private int wordFont;
    private int printGray;
    private ProgressDialog progressDialog;
    private final static int MAX_LEFT_DISTANCE = 255;
    ProgressDialog dialog;
    private String Result;
    private Boolean nopaper = false;
    private boolean LowBattery = false;
    private final boolean isClose = false;// 关闭程序
//    String picturePath;
    MyHandler handler;
    private static String printVersion;
    private final int NOPAPER = 3;
    private final int LOWBATTERY = 4;
    private final int PRINTVERSION = 5;
    private final int PRINTBARCODE = 6;
    private final int PRINTQRCODE = 7;
    private final int PRINTPAPERWALK = 8;
    private final int PRINTCONTENT = 9;
    private final int CANCELPROMPT = 10;
    private final int PRINTERR = 11;
    private final int OVERHEAT = 12;
    private final int MAKER = 13;
    private final int PRINTPICTURE = 14;
    private final int EXECUTECOMMAND = 15;
    private AppCompatImageView smartlogo;
    private Bitmap bitmap;

    private boolean stop = false;
    private static final String TAG = "ConsoleTestActivity";
    Logger log = Log4Helper.getLogger(TAG);
    AppPreference preferences;
    boolean tabletSize = false;
    String imageUri;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        setContentView(R.layout.printing_screen);
        ab = getSupportActionBar();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        smartlogo = (AppCompatImageView)findViewById(R.id.smartprintlogo);
        bitmap = ((BitmapDrawable)smartlogo.getDrawable()).getBitmap();
//        bitmap.setWidth(99);
        imageUri = "drawable://" + R.drawable.drawer_header;

        Intent intent = getIntent();
        subsriber = intent.getStringArrayExtra("subscriber");
        if (subsriber != null) {
            System.out.println("size is " + subsriber.length);
            header = new String[]{getString(R.string.title), getString(R.string.strgender), getString(R.string
                    .strname), getString(R.string.strsname), getString(R.string.strmname), getString(R.string.strregion),
                    getString(R.string.strdistict), getString(R.string.strstreet), getString(R.string.stradd), getString(R.string.strbdate), getString(R.string
                    .strjob), getString(R.string.strphno), getString(R.string.strsimserial), getString(R.string.strimei),
                    getString(R.string.strcitizenship), getString(R.string.stridno), getString(R.string.idtype),
                    getString(R.string.strtel), getString(R.string.strmail)};
        }
        txtmsg = (TextView) findViewById(R.id.txtmsg);
        txtcopyright = (TextView) findViewById(R.id.copy);
        String msg = "";
        if (subsriber != null && subsriber.length > 0) {
            for (int i = 0; i < subsriber.length; i++) {
                msg += header[i] + " :  " + subsriber[i] + "\n";
            }
        } else {
            msg = "First Name : Test\n"
                    + "Last Name : Kimoka\n"
                    + "Middle Name : \n"
                    + "Birth Date : 1990-03-24\n"
                    + "Nationality : Tanzanian\n"
                    + "Region : Dar es salaam\n"
                    + "District : Kinondoni\n"
                    + "Suburb/Village : Kibangu\n"
                    + "Occupation : Teacher\n"
                    + "Gender : Male\n";
        }
        txtmsg.setText(msg);
//        txtmsg.setCompoundDrawablesWithIntrinsicBounds(
//                R.drawable.drawer_header, 0, 0, 0);
        handler = new MyHandler();
        LowBattery = false;

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    ThermalPrinter.start(PrintingCenter.this);
                    ThermalPrinter.reset();
                    printVersion = ThermalPrinter.getVersion();
                    int status = ThermalPrinter.checkStatus();
                    Log.e("STATUS", "Status is " + status);
                    if (status == ThermalPrinter.STATUS_OK)
                        Log.e("STATUS", "Statatus is ok");
                } catch (TelpoException e) {
                    log.error("Telpro printer error " + e.getMessage());
                } finally {
                    if (printVersion != null) {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "1";
                        handler.sendMessage(message);
                    } else {
                        Message message = new Message();
                        message.what = PRINTVERSION;
                        message.obj = "0";
                        handler.sendMessage(message);
                    }
                    ThermalPrinter.stop(PrintingCenter.this);
                }
            }
        }).start();

        ab.setDisplayHomeAsUpEnabled(false);
    }

    private final BroadcastReceiver printReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_NOT_CHARGING);
                int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
                //TPS390在低电量时不论有没接外电，均不允许打印
                if (Build.MODEL.equals("TPS390")) {
                    if (level * 5 <= scale) {
                        LowBattery = true;
                    } else {
                        LowBattery = false;
                    }
                } else {
                    if (status != BatteryManager.BATTERY_STATUS_CHARGING) {
                        if (level * 5 <= scale) {
                            LowBattery = true;
                        } else {
                            LowBattery = false;
                        }
                    } else {
                        LowBattery = false;
                    }
                }
            }
        }
    };

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.print, menu);
        menupair = menu.findItem(R.id.action_pair);
        menupair.setVisible(false);
        menuprint = menu.findItem(R.id.action_print);
        menuprint.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_print:
                String msg = "";
                CharSequence title = "";

                title = Html.fromHtml(getString(R.string.strtitle));
                if (subsriber != null && subsriber.length > 0) {
                    for (int i = 0; i < subsriber.length; i++) {
                        msg += header[i] + " :  " + subsriber[i] + "\n";
                    }
                } else {
                    msg = txtmsg.getText().toString();

                }
                if (msg.length() > 0) {
                    if (LowBattery == true) {
                        handler.sendMessage(handler.obtainMessage(LOWBATTERY, 1, 0, null));
                    } else {
                        printContent = msg;
                        if (!nopaper) {
                            setTitle(title);
                            progressDialog = ProgressDialog.show(PrintingCenter.this, getString(R.string
                                    .bl_dy), getString(R.string.printing_wait));
                            handler.sendMessage(handler.obtainMessage(PRINTCONTENT, 1, 0, null));
                        } else {
                            Toast.makeText(PrintingCenter.this, getString(R.string.ptintInit), Toast
                                    .LENGTH_LONG).show();
                        }
                    }
                }
                break;
        }
        return false;

    }

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (stop == true)
                return;
            switch (msg.what) {
                case NOPAPER:
                    noPaperDlg();
                    break;
                case LOWBATTERY:
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(PrintingCenter.this);
                    alertDialog.setTitle(R.string.operation_result);
                    alertDialog.setMessage(getString(R.string.LowBattery));
                    alertDialog.setPositiveButton(getString(R.string.dlg_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    alertDialog.show();
                    break;
                case PRINTCONTENT:
                    new contentPrintThread().start();
                    break;
                case PRINTPICTURE:
                    new printPicture().start();
                    break;
                case CANCELPROMPT:
                    if (progressDialog != null && !PrintingCenter.this.isFinishing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                    break;
                case OVERHEAT:
                    AlertDialog.Builder overHeatDialog = new AlertDialog.Builder(PrintingCenter.this);
                    overHeatDialog.setTitle(R.string.operation_result);
                    overHeatDialog.setMessage(getString(R.string.overTemp));
                    overHeatDialog.setPositiveButton(getString(R.string.dlg_ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    overHeatDialog.show();
                    break;
                default:
//                    Toast.makeText(PrintingCenter.this, "Print Error!", Toast.LENGTH_LONG).show();
                    break;
            }
        }
    }

    private void noPaperDlg() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(PrintingCenter.this);
        dlg.setTitle(getString(R.string.noPaper));
        dlg.setMessage(getString(R.string.noPaperNotice));
        dlg.setCancelable(false);
        dlg.setPositiveButton(R.string.sure, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ThermalPrinter.stop(PrintingCenter.this);
            }
        });
        dlg.show();
    }
    private String picturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/111.bmp";

    private class contentPrintThread extends Thread {
        @Override
        public void run() {
            super.run();
            setName("Content Print Thread");
            try {
                ThermalPrinter.start(PrintingCenter.this);
                ThermalPrinter.reset();
                ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_MIDDLE);
                ThermalPrinter.setLeftIndent(leftDistance);
                ThermalPrinter.setLineSpace(lineDistance);
                if (wordFont == 4) {
                    ThermalPrinter.setFontSize(2);
                    ThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 3) {
                    ThermalPrinter.setFontSize(1);
                    ThermalPrinter.enlargeFontSize(2, 2);
                } else if (wordFont == 2) {
                    ThermalPrinter.setFontSize(2);
                } else if (wordFont == 1) {
                    ThermalPrinter.setFontSize(1);
                }
//                System.out.println("BITMAP WIDTH: "+bitmap.getWidth());
//                ThermalPrinter.setGray(printGray);
                ThermalPrinter.setGray(5);
                ThermalPrinter.printLogo(bitmap);
                ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_LEFT);
//                ThermalPrinter.addString();
                ThermalPrinter.addString(getString(R.string.info)+"\n"+printContent);
//                ThermalPrinter.addString(getString(R.string.line));
                ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_MIDDLE);
                ThermalPrinter.addString(getString(R.string.smarttz_2016));
                ThermalPrinter.printString();
                ThermalPrinter.clearString();
                ThermalPrinter.walkPaper(100);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
                Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                } else {
                    handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                }
            } finally {
                handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                if (nopaper) {
                    handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                    nopaper = false;
                    return;
                }
                ThermalPrinter.stop(PrintingCenter.this);
                Log.v(TAG, "The Print Progress End !!!");
                if (isClose) {
                    finish();
                }
            }
        }
    }

    private class printPicture extends Thread {

        @Override
        public void run() {
            super.run();
            setName("PrintPicture Thread");
            try {
                ThermalPrinter.start(PrintingCenter.this);
                ThermalPrinter.reset();
                ThermalPrinter.setGray(printGray);
                ThermalPrinter.setAlgin(ThermalPrinter.ALGIN_MIDDLE);
                File file = new File(picturePath);
                if (file.exists()) {
                    ThermalPrinter.printLogo(BitmapFactory.decodeFile(picturePath));
                    ThermalPrinter.walkPaper(100);
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(PrintingCenter.this, getString(R.string.not_find_picture), Toast
                                    .LENGTH_LONG).show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                Result = e.toString();
                if (Result.equals("com.telpo.tps550.api.printer.NoPaperException")) {
                    nopaper = true;
                } else if (Result.equals("com.telpo.tps550.api.printer.OverHeatException")) {
                    handler.sendMessage(handler.obtainMessage(OVERHEAT, 1, 0, null));
                } else {
                    handler.sendMessage(handler.obtainMessage(PRINTERR, 1, 0, null));
                }
            } finally {
                handler.sendMessage(handler.obtainMessage(CANCELPROMPT, 1, 0, null));
                if (nopaper) {
                    handler.sendMessage(handler.obtainMessage(NOPAPER, 1, 0, null));
                    nopaper = false;
                    return;
                }
                ThermalPrinter.stop(PrintingCenter.this);
                Log.v(TAG, "The PrintPicture Progress End !!!");
                if (isClose) {
                    finish();
                }
            }
        }
    }

}
