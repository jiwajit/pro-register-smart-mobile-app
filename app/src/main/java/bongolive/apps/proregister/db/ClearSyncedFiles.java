package bongolive.apps.proregister.db;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.util.Log;

import java.io.File;
import java.io.IOError;
import java.util.ArrayList;

import bongolive.apps.proregister.AlertDialogManager;
import bongolive.apps.proregister.R;
import bongolive.apps.proregister.utils.Constants;

/**
 * Created by nasznjoka on 2/22/2015.
 */
public class ClearSyncedFiles extends AsyncTask<String, Integer, String[]> {
    Context ctx;
    CheckBoxPreference chk;

    public ClearSyncedFiles(Context context, CheckBoxPreference checkBoxPreference) {
        this.ctx = context;
        this.chk = checkBoxPreference;
    }

    ProgressDialog dialog;
    int progress = 0, max = 0;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(ctx);
        dialog.setTitle(ctx.getString(R.string.strloadingfiles));
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (dialog != null) {
            double diff = (double) max - progress;
            double diffperc = (diff / max) * 100;
            double perc = 100 - diffperc;
            String prg1 = String.valueOf((int) perc);
            dialog.setMessage(ctx.getString(R.string.strdeleting) + " " + prg1 + "%");
            dialog.setCancelable(true);
        }
    }

    @Override
    protected String[] doInBackground(String[] va) {
        ArrayList<String> list = MultimediaContents.getAllSyncedMedias(ctx);
        max = list.size();
        File sdcard = Environment.getExternalStorageDirectory();
        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
        if (!folder.exists()) {
            return null;
        }
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                File file = new File(list.get(i));
                Log.e("FILENAME", "FILENAME IS " + list.get(i));
                if (file.exists()) {
                    try {
                        boolean del = file.delete();
                        if (del)
                            publishProgress(i, list.size());
                        else
                            publishProgress(i, list.size());
                    } catch (IOError e) {
                        e.printStackTrace();
                    }
                } else {
                    publishProgress(i, list.size());
                }
                progress += 1;
            }
        }
        return va;
    }

    @Override
    protected void onPostExecute(String[] va) {
        super.onPostExecute(va);
        if (dialog.isShowing())
            dialog.dismiss();
        AlertDialogManager dialog = new AlertDialogManager();
        String str = Constants.getIMEINO(ctx);
        dialog.showAlertDialog(ctx, ctx.getString(R.string.strautoclearfiles), ctx.getString(R
                .string.strmediapurged), false);
        this.chk.setChecked(true);
        chk.setChecked(true);
    }
}
