/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.UrlCon;


/**
 * Created by nasznjoka on 3/26/15.
 */
public class Tracking {
    public static final String TABLENAME = "tracking";
    public static final String ID = "tracker_id";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String DATE = "date";
    public static final String ISSYNCED = "issynced";

    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderInstance.AUTHORITY + "/" + TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.simreg.tracking" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.simreg.tracking" ;
    private static final String TAG = Tracking.class.getName();
    private static final String ARRAYNAME2 = "datapointLists";
    static UrlCon js ;
    public static String ARRAYNAME = "datapointList" ;

    public static int getLocationCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }

    public static int storeLocation(Context context,double[] vals){
        int loc = getLocationCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(LAT,vals[0]);
        cv.put(LNG, vals[1]);
        cv.put(DATE, Constants.getDate());
       cr.insert(BASEURI,cv);
        if(loc + 1 == getLocationCount(context)){
            Log.v("DATASTORAGE", "DATA IS STORED");
            purgeData(context);
            return 1;
        }
      return 0;
    }

    public static JSONObject sendUpdates(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = ISSYNCED + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGLOCATIONUPDATES);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LNG, c.getString(c.getColumnIndexOrThrow(LNG)));
                    j.put(DATE, c.getString(c.getColumnIndexOrThrow(DATE)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put("datapoints",(Object)jarr);
                array = js.getJson(Constants.SERVER, jsonObject);
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }

    public static void processInformation(Context context) throws JSONException {
        JSONObject json = sendUpdates(context);
        try {
            if(!TextUtils.isEmpty(json.getString(Constants.SUCCESS))){
                String res = json.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                    if(json.has(ARRAYNAME)){
                        JSONObject jsonObject = json.getJSONObject(ARRAYNAME);
                        if(jsonObject.has(ARRAYNAME2)){
                            JSONArray jsonArray = jsonObject.getJSONArray(ARRAYNAME2);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                int locid = jsonObject1.getInt(ID);
                                boolean OK = updateSync(context, locid, 1);
                                if (OK == true) {
                                    Log.v("updated", "the order is synced " + locid);
                                }
                            }
                        }
                    }

                }
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static boolean updateSync(Context context, int id, int flag)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ISSYNCED, flag);
        String where =  ID+ " = "+ id ;
        if(cr.update(BASEURI, values, where, null) == 1)
        {
            return true ;
        }   else {
            return false ;
        }
    }

    public static int purgeData(Context context){
        ContentResolver cr = context.getContentResolver();
        int data = getLocationCount(context);
        if(data > 50){
            Cursor c = cr.query(BASEURI, new String[]{ID},null,null,null);
            if(c.moveToLast()){
                    String locid = c.getString(c.getColumnIndexOrThrow(ID));
                    deleteDataPoint(context,locid);
            }
        }
        return 0;
    }

    public static int deleteDataPoint(Context context, String data){
        ContentResolver cr = context.getContentResolver();
        String where = ID +" = "+ data +
                " AND " + ISSYNCED + " = 1";
        if(cr.delete(BASEURI, where, null) > 0){
            Log.v("DELETING DATA", "LOC  ID " + data);
            return 1;
        }
        return 0;
    }

}
