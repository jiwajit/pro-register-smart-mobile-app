/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHandler extends SQLiteOpenHelper {
    public static final String DBNAME = "sim_reg";
    private static final int DBVERSION = 2;
    private static DatabaseHandler mInstance = null;
    Context _context;

    public static DatabaseHandler getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DatabaseHandler(ctx.getApplicationContext());
        }
        return mInstance;
    }

    private DatabaseHandler(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CUSTOMERS = "CREATE TABLE " + Customers.TABLENAME + " (" +
                Customers.ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                Customers.FNAME + " VARCHAR(50) , " +
                Customers.SURNAME + " VARCHAR(50) , " +
                Customers.MNAME + " VARCHAR(50) , " +
                Customers.REFERENCE + " INTEGER DEFAULT 0, " +
                Customers.DISTRICT + " VARCHAR(50) , " +
                Customers.STREET + " VARCHAR(50) , " +
                Customers.REGION + " VARCHAR(50) , " +
                Customers.CITIZENSHIP + " VARCHAR(50) , " +
                Customers.IDNO + " VARCHAR(50) , " +
                Customers.IDTYPE + " VARCHAR(20) , " +
                Customers.MOBILE + " VARCHAR(20) , " +
                Customers.TELEPHONE + " VARCHAR(20) , " +
                Customers.TITLE + " VARCHAR(20) , " +
                Customers.GENDER + " VARCHAR(20) , " +
                Customers.IMEI + " VARCHAR(20) , " +
                Customers.BDATE + " VARCHAR(30) , " +
                Customers.UPDATED + " DATETIME , " +
                Customers.CREATED + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  , " +
                Customers.EMAIL + " VARCHAR(50) , " +
                Customers.SIMSERIAL + " VARCHAR(50) , " +
                Customers.JOB + " VARCHAR(100) , " +
                Customers.SERVICES + " VARCHAR(150) , " +
                Customers.LOCALUNIQUEID + " VARCHAR(100) , " +
                Customers.COMMENTS + " TEXT , " +
                Customers.STATUS + " INTEGER DEFAULT 0, " +
                Customers.REGID + " VARCHAR(20) DEFAULT 0 , " +
                Customers.LAT + " DOUBLE , " +
                Customers.LONGTUDE + " DOUBLE , " +
                Customers.SYNCSTATUS + " INTEGER DEFAULT 0, " +
                Customers.ADDRESS + " VARCHAR(50) )";

        String AGENT = "CREATE TABLE " + Agent.TABLENAME + " (" +
                Agent.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                Agent.NAME + " INTEGER ," +
                Agent.DISTRICT + " DATETIME DEFAULT CURRENT_TIMESTAMP ," +
                Agent.REFERENCE + " VARCHAR(20) ," +
                Agent.REGION + " VARCHAR(20) ," +
                Agent.EMAIL + " VARCHAR(100) ," +
                Agent.MOBILE + " VARCHAR(50) ," +
                Agent.AGENTNO + " VARCHAR(50) ," +
                Agent.STREET + " VARCHAR(50) ," +
                Agent.LOCALAGENTID + " VARCHAR(100) ," +
                Agent.PASS + " VARCHAR(50)," +
                Agent.CREATED + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ," +
                Agent.SYNCSTATUS + " INTEGER DEFAULT 0 )";


        String TRACKING = "CREATE TABLE " + Tracking.TABLENAME + " (" +
                Tracking.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                Tracking.LAT + " DOUBLE ," +
                Tracking.LNG + " DOUBLE ," +
                Tracking.ISSYNCED + " INTEGER NOT NULL DEFAULT 0 , " +
                Tracking.DATE + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  )";

        String MULTM = "CREATE TABLE " + MultimediaContents.TABLENAME + " (" +
                MultimediaContents.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                MultimediaContents.ISSYNCED + " INTEGER NOT NULL DEFAULT 0 ," +
                MultimediaContents.MEDIATYPE + " INTEGER NOT NULL DEFAULT 0 ," +
                MultimediaContents.CUSTOMERORAGENT + " INTEGER NOT NULL DEFAULT 0 ," +
                MultimediaContents.FOREIGNKEY + " INTEGER(11) ," +
                MultimediaContents.LOCALMEDIAID + " VARCHAR(100) ," +
                MultimediaContents.REFERENCE + " VARCHAR(100) NOT NULL DEFAULT 0," +
                MultimediaContents.NAME + " VARCHAR(100) NOT NULL ," +
                MultimediaContents.SIZE + " VARCHAR(30) NOT NULL DEFAULT 0," +
                MultimediaContents.TYPE + " VARCHAR(25) NOT NULL ," +
                MultimediaContents.CONTENT + " VARCHAR(150) NOT NULL," +
                MultimediaContents.DATE + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  )";

        db.execSQL(CUSTOMERS);
        db.execSQL(AGENT);
        db.execSQL(TRACKING);
        db.execSQL(MULTM);

        System.out.println("database created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("LOG_TAG", "upgrading database from version "
                + oldVersion + "to version " + newVersion + "all data will be dropped");

        String CUSTOMER = " ALTER TABLE " + Customers.TABLENAME + " ADD COLUMN " + Customers.COMMENTS + " " +
                "TEXT";

        if (!Customers.columnExist(db, Customers.COMMENTS))
            db.execSQL(CUSTOMER);

    }

}
