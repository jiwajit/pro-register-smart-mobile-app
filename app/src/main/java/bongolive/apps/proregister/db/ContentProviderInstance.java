/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ContentProviderInstance extends ContentProvider {
    public static final String AUTHORITY = "bongolive.apps.proregister.db.ContentProviderInstance";
    Context _context;
    //static final HashMap<String, String> hashmap ;
    private static UriMatcher _uriMatcher;
    private static final int GENERAL_AGENT = 1;
    private static final int SPECIFIC_AGENT = 2;
    private static final int GENERAL_CUSTOMER = 3;
    private static final int SPECIFIC_CUSTOMER = 4;
    private static final int GENERAL_LOC = 5;
    private static final int SPECIFIC_LOC = 6;
    private static final int GENERAL_MULT = 7;
    private static final int SPECIFIC_MULT = 8;

    static {
        _uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        _uriMatcher.addURI(AUTHORITY, Agent.TABLENAME, GENERAL_AGENT);
        _uriMatcher.addURI(AUTHORITY, Agent.TABLENAME + "/#", SPECIFIC_AGENT);
        _uriMatcher.addURI(AUTHORITY, Customers.TABLENAME, GENERAL_CUSTOMER);
        _uriMatcher.addURI(AUTHORITY, Customers.TABLENAME + "/#", SPECIFIC_CUSTOMER);
        _uriMatcher.addURI(AUTHORITY, Tracking.TABLENAME, GENERAL_LOC);
        _uriMatcher.addURI(AUTHORITY, Tracking.TABLENAME + "/#", SPECIFIC_LOC);
        _uriMatcher.addURI(AUTHORITY, MultimediaContents.TABLENAME, GENERAL_MULT);
        _uriMatcher.addURI(AUTHORITY, MultimediaContents.TABLENAME + "/#", SPECIFIC_MULT);
    }

    @Override
    public boolean onCreate() {
        try {
            DatabaseHandler.getInstance(getContext());
        } catch (SQLiteException ex) {
            throw new Error(ex.toString());
        }
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        switch (_uriMatcher.match(uri)) {
            case GENERAL_CUSTOMER:
                qb.setTables(Customers.TABLENAME);
                break;
            case SPECIFIC_CUSTOMER:
                qb.setTables(Customers.TABLENAME);
                qb.appendWhere(Customers.ID + " =  " + uri.getLastPathSegment());
                break;
            case GENERAL_AGENT:
                qb.setTables(Agent.TABLENAME);
                break;
            case SPECIFIC_AGENT:
                qb.setTables(Agent.TABLENAME);
                qb.appendWhere(Agent.ID + " =  " + uri.getLastPathSegment());
                break;
            case GENERAL_LOC:
                qb.setTables(Tracking.TABLENAME);
                break;
            case SPECIFIC_LOC:
                qb.setTables(Tracking.TABLENAME);
                qb.appendWhere(Tracking.ID + " =  " + uri.getLastPathSegment());
                break;
            case GENERAL_MULT:
                qb.setTables(MultimediaContents.TABLENAME);
                break;
            case SPECIFIC_MULT:
                qb.setTables(MultimediaContents.TABLENAME);
                qb.appendWhere(MultimediaContents.ID + " =  " + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException(" UNKOWN URI :" + uri);
        }
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (_uriMatcher.match(uri)) {
            case GENERAL_CUSTOMER:
                return Customers.GENERAL_CONTENT_TYPE;
            case SPECIFIC_CUSTOMER:
                return Customers.SPECIFIC_CONTENT_TYPE;
            case GENERAL_AGENT:
                return Agent.GENERAL_CONTENT_TYPE;
            case SPECIFIC_AGENT:
                return Agent.SPECIFIC_CONTENT_TYPE;
            case GENERAL_LOC:
                return Tracking.GENERAL_CONTENT_TYPE;
            case SPECIFIC_LOC:
                return Tracking.SPECIFIC_CONTENT_TYPE;
            case GENERAL_MULT:
                return MultimediaContents.GENERAL_CONTENT_TYPE;
            case SPECIFIC_MULT:
                return MultimediaContents.SPECIFIC_CONTENT_TYPE;
            default:
                throw new IllegalArgumentException(" Unkown TYPE " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase();
        long id;
        switch (_uriMatcher.match(uri)) {
            case GENERAL_CUSTOMER:
                id = db.insertOrThrow(Customers.TABLENAME, null, values);
                break;
            case GENERAL_AGENT:
                id = db.insertOrThrow(Agent.TABLENAME, null, values);
                break;
            case GENERAL_LOC:
                id = db.insertOrThrow(Tracking.TABLENAME, null, values);
                break;
            case GENERAL_MULT:
                id = db.insertOrThrow(MultimediaContents.TABLENAME, null, values);
                break;
            default:
                throw new IllegalArgumentException(" error invalid uri " + uri);
        }
        Uri inserturi = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(inserturi, null);
        return inserturi;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase();
        int count = 0;
        switch (_uriMatcher.match(uri)) {
            case GENERAL_CUSTOMER:
                count = db.delete(Customers.TABLENAME, selection, selectionArgs);
                break;
            case SPECIFIC_CUSTOMER:
                count = db.delete(Customers.TABLENAME, Customers.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            case GENERAL_AGENT:
                count = db.delete(Agent.TABLENAME, selection, selectionArgs);
                break;
            case SPECIFIC_AGENT:
                count = db.delete(Agent.TABLENAME, Agent.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            case GENERAL_LOC:
                count = db.delete(Tracking.TABLENAME, selection, selectionArgs);
                break;
            case SPECIFIC_LOC:
                count = db.delete(Tracking.TABLENAME, Tracking.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            case GENERAL_MULT:
                count = db.delete(MultimediaContents.TABLENAME, selection, selectionArgs);
                break;
            case SPECIFIC_MULT:
                count = db.delete(MultimediaContents.TABLENAME, MultimediaContents.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            default:
                throw new IllegalArgumentException(" invalid uri " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase();
        int count;
        switch (_uriMatcher.match(uri)) {
            case GENERAL_CUSTOMER:
                count = db.update(Customers.TABLENAME, values, selection, selectionArgs);
                break;
            case SPECIFIC_CUSTOMER:
                count = db.update(Customers.TABLENAME, values, Customers.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            case GENERAL_AGENT:
                count = db.update(Agent.TABLENAME, values, selection, selectionArgs);
                break;
            case SPECIFIC_AGENT:
                count = db.update(Agent.TABLENAME, values, Agent.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            case GENERAL_LOC:
                count = db.update(Tracking.TABLENAME, values, selection, selectionArgs);
                break;
            case SPECIFIC_LOC:
                count = db.update(Tracking.TABLENAME, values, Tracking.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            case GENERAL_MULT:
                count = db.update(MultimediaContents.TABLENAME, values, selection, selectionArgs);
                break;
            case SPECIFIC_MULT:
                count = db.update(MultimediaContents.TABLENAME, values, MultimediaContents.ID + " = ?", new String[]{uri.getLastPathSegment()});
                break;
            default:
                throw new IllegalArgumentException(" invalid uri " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

}
