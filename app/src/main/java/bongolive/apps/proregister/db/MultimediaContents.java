/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proregister.MainScreen;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.UrlCon;


/**
 * Created by nasznjoka on 3/26/15.
 */
public class MultimediaContents {
    public static final String TABLENAME = "multimedia";
    public static final String ID = "id";
    public static final String CONTENT = "content";
    public static final String SIZE = "size";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String FOREIGNKEY = "foreignkey";
    public static final String REFERENCE = "reference";
    public static final String CUSTOMERORAGENT = "customer_agent";//0 customer 1 agent
    public static final String MEDIATYPE = "media_type";//0 agent/customer photo, //1 agent/customer id photo, // 2
    // agent/custoer signature photo
    public static final String DATE = "date";
    public static final String LOCALMEDIAID = "med_id";
    public static final String ISSYNCED = "issynced";

    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderInstance.AUTHORITY + "/" + TABLENAME);
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.simreg.multimedia";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.simreg.multimedia";
    private static final String TAG = MultimediaContents.class.getName();
    private static final String ARRAYNAME2 = "datapointLists";
    static UrlCon js;
    public static String ARRAYNAME = "datapointList";

    public static int getCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount();
        _c.close();
        return count;
    }

    public static int storeMultimedia(Context context, String[] vals_) {
        int loc = getCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(CONTENT, vals_[0]);
        cv.put(NAME, vals_[1]);//content/name/type/customeroragent/signorid/id
        cv.put(TYPE, vals_[2]);//image/sound/video
        cv.put(CUSTOMERORAGENT, vals_[3]);//whoever has 1
        cv.put(MEDIATYPE, vals_[4]); //image/sound 0/1/2/3 user/id/signature/voice
        cv.put(FOREIGNKEY, vals_[5]);//customer/agent id
        cv.put(LOCALMEDIAID, vals_[6]);
        cv.put(DATE, Constants.getDate());
        System.out.println("unique id " + vals_[6]);
        cr.insert(BASEURI, cv);
        if (loc + 1 == getCount(context)) {
            Log.v("DATASTORAGE", "DATA IS STORED");
//            purgeData(context);
            return 1;
        }
        return 0;
    }


    public static JSONObject sendMultimediaOld(Context context) throws JSONException {
        ContentResolver cr = context.getContentResolver();
        String where = ISSYNCED + " = 0";
        Cursor c = cr.query(BASEURI, null, where, null, ID + " ASC LIMIT 2");
        js = new UrlCon();
        JSONObject jsonObject = new JSONObject();
        String array = null;
        try {
            jsonObject.put("tag", Constants.TAGMULTIMEDIA);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            if (c != null && c.moveToFirst()) {
                jsonObject.put(NAME, c.getString(c.getColumnIndexOrThrow(NAME)));
                jsonObject.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                String ty = c.getString(c.getColumnIndex(TYPE));
                jsonObject.put(TYPE, ty);
                jsonObject.put(CUSTOMERORAGENT, c.getString(c.getColumnIndexOrThrow(CUSTOMERORAGENT)));
                jsonObject.put(MEDIATYPE, c.getString(c.getColumnIndexOrThrow(MEDIATYPE)));
                jsonObject.put(FOREIGNKEY, c.getString(c.getColumnIndexOrThrow(FOREIGNKEY)));
                jsonObject.put(LOCALMEDIAID, c.getString(c.getColumnIndexOrThrow(LOCALMEDIAID)));
                if (ty.equals(Constants.MULT_IMAGE)) {
                    jsonObject.put(CONTENT, Constants.getbase64photo(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                } else if (ty.equals(Constants.MULT_VIDEO) || ty.equals(Constants.MULT_SOUND)) {
                    jsonObject.put(CONTENT, Constants.getBase64Vid(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                }
//                Constants.createFile(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        if (array != null) {
            JSONObject job = new JSONObject(array);

            return job;
        }
        return null;
    }

    public static JSONObject sendMultimedia(Context context) throws JSONException {
        ContentResolver cr = context.getContentResolver();
        String where = ISSYNCED + " = 0";
        Cursor c = cr.query(BASEURI, null, where, null, ID + " DESC LIMIT 4");
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGMULTIMEDIA);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array;
            if (c != null && c.moveToFirst()) {
                JSONArray jarr = new JSONArray();
                do {
                    JSONObject j = new JSONObject();
                    j.put(NAME, c.getString(c.getColumnIndexOrThrow(NAME)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    String ty = c.getString(c.getColumnIndex(TYPE));
                    j.put(TYPE, ty);
                    j.put(CUSTOMERORAGENT, c.getString(c.getColumnIndexOrThrow(CUSTOMERORAGENT)));
                    j.put(MEDIATYPE, c.getString(c.getColumnIndexOrThrow(MEDIATYPE)));
                    j.put(FOREIGNKEY, c.getString(c.getColumnIndexOrThrow(FOREIGNKEY)));
                    j.put(LOCALMEDIAID, c.getString(c.getColumnIndexOrThrow(LOCALMEDIAID)));
                    if (ty.equals(Constants.MULT_IMAGE)) {
                        j.put(CONTENT, Constants.getbase64photo(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                        Log.e("CONTENT VALUE: ", Constants.getbase64photo(c.getString(c.getColumnIndexOrThrow(CONTENT))));

                    } else if (ty.equals(Constants.MULT_VIDEO) || ty.equals(Constants.MULT_SOUND)) {
                        j.put(CONTENT, Constants.getBase64Vid(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                        Log.e("CONTENT VALUE: ", Constants.getBase64Vid(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                    }
                    jarr.put(j);
                } while (c.moveToNext());
                jsonObject.put("multimedia_array", (Object) jarr);
                Log.v("json", jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
                JSONObject job = new JSONObject(array);
                return job;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }


    public static void processInformation(Context context) throws JSONException {
        JSONObject json = sendMultimedia(context);
        if (json != null)
            try {
                if (json.has(Constants.SUCCESS)) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        if (json.has(ARRAYNAME)) {
                            JSONObject jsonObject = json.getJSONObject(ARRAYNAME);
                            if (jsonObject.has(ARRAYNAME2)) {
                                JSONArray jsonArray = jsonObject.getJSONArray(ARRAYNAME2);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    int locid = jsonObject1.getInt(ID);
                                    boolean OK = updateSync(context, locid, 1);
                                    if (OK == true) {
                                        Log.v("updated", "the media is synced " + locid);
                                    }
                                }
                            }
                        }

                    }
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
//                new MainScreen().displayBackgroundErrror("Could not send the phone number for registration");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
//                new MainScreen().displayBackgroundErrror("Could not send the phone number for registration");
            }
    }

    public static boolean updateSync(Context context, int id, int flag) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(ISSYNCED, flag);
        String where = ID + " = " + id;
        if (cr.update(BASEURI, values, where, null) == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static int purgeData(Context context) {
        ContentResolver cr = context.getContentResolver();
        int data = getCount(context);
        if (data > 50) {
            Cursor c = cr.query(BASEURI, new String[]{ID}, null, null, null);
            if (c.moveToLast()) {
                String locid = c.getString(c.getColumnIndexOrThrow(ID));
                deleteContent(context, locid);
            }
        }
        return 0;
    }

    public static int deleteContent(Context context, String data) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + data +
                " AND " + ISSYNCED + " = 1";
        if (cr.delete(BASEURI, where, null) > 0) {
            Log.v("DELETING DATA", "LOC  ID " + data);
            return 1;
        }
        return 0;
    }

    public static String getLastImage(Context context) {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = TYPE + " = " + DatabaseUtils.sqlEscapeString(Constants.MULT_IMAGE);
        Cursor c = cr.query(BASEURI, null, where, null, ID + " DESC LIMIT 1");
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getuserphoto(Context context, long id) {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = CUSTOMERORAGENT + " = " + Constants.IMG_TYPE_CUSTOMER + " AND " +
                MEDIATYPE + " =  " + Constants.IMGUSER_MEDIATYPE + " AND " + FOREIGNKEY + " " + " = " + id;
        Cursor c = cr.query(BASEURI, new String[]{CONTENT}, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getidhoto(Context context, long id) {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = CUSTOMERORAGENT + " = " + Constants.IMG_TYPE_CUSTOMER + " AND " +
                MEDIATYPE + " =  " + Constants.IMGID_MEDIATYPE + " AND " + FOREIGNKEY + " " + " = " + id;
        Cursor c = cr.query(BASEURI, new String[]{CONTENT}, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        System.out.println("photo " + bz + " id  " + id + " where " + where);
        return bz;
    }

    public static String getsignhoto(Context context, long id) {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = CUSTOMERORAGENT + " = " + Constants.IMG_TYPE_CUSTOMER + " AND " +
                MEDIATYPE + " =  " + Constants.IMGSIGN_MEDIATYPE + " AND " + FOREIGNKEY + " " + " = " + id;
        Cursor c = cr.query(BASEURI, new String[]{CONTENT}, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getLastClip(Context context) {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = TYPE + " = " + DatabaseUtils.sqlEscapeString(Constants.MULT_VIDEO);
        Cursor c = cr.query(BASEURI, null, where, null, ID + " DESC LIMIT 1");
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static ArrayList<String> getAllSyncedMedias(Context context) {
        ArrayList<String> bz = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = ISSYNCED + " = 1";
        Cursor c = cr.query(BASEURI, null, where, null, ID + " ASC ");
        try {
            if (c != null && c.moveToFirst()) {
                do {
                    String file = c.getString(c.getColumnIndex(CONTENT));
                    try {
                        if (file.length() > 0)
                            bz.add(file);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }
}
