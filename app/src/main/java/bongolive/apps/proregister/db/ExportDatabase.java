package bongolive.apps.proregister.db;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import bongolive.apps.proregister.AlertDialogManager;
import bongolive.apps.proregister.utils.Constants;

/**
 * Created by nasznjoka on 2/22/2015.
 */
public class ExportDatabase extends AsyncTask<String, Void, Boolean> {
	Context ctx;
	CheckBoxPreference chk;
	public ExportDatabase(Context context, CheckBoxPreference checkBoxPreference){
		this.ctx = context;
		this.chk = checkBoxPreference;
	}
//	private final ProgressDialog dialog = new ProgressDialog(ctx);

	// can use UI thread here
	protected void onPreExecute() {/*
		this.dialog.setMessage("Exporting database...");
		this.dialog.show();*/
	}

	// automatically done on worker thread (separate from UI thread)
	protected Boolean doInBackground(final String... args) {

		File dbFile =new File(Environment.getDataDirectory() + "/data/bongolive.apps.proregister/databases/sim_reg");
        Log.v("dbpath","path is "+ctx.getDatabasePath(DatabaseHandler.DBNAME).toString());

		File sdcard = Environment.getExternalStorageDirectory() ;

		File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);

		if (!folder.exists()) {
			folder.mkdirs();
		}

		String str = Constants.getIMEINO(this.ctx);
		File file = new File(folder.getAbsoluteFile(), str) ;

		try {
			try {

				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.copyFile(dbFile, file);
			return true;
		} catch (IOException e) {
			Log.e("mypck", e.getMessage(), e);
			return false;
		}
	}

	// can use UI thread here
	protected void onPostExecute(final Boolean success) {
		/*if (this.dialog.isShowing()) {
			this.dialog.dismiss();
		}*/
		if (success) {
			AlertDialogManager dialog = new AlertDialogManager();
			String str = Constants.getIMEINO(ctx);
			dialog.showAlertDialog(ctx, "DATABASE EXPORT", "Export successful! \n " +
					"please locate the back up in the memory card within a folder called " +
					Constants.MASTER_DIR+ "and a file called " + str, Boolean.valueOf(false));
			this.chk.setChecked(true);
			chk.setChecked(true);
		} else {
			Toast.makeText(ctx, "Export failed", Toast.LENGTH_SHORT).show();
			chk.setChecked(true);
		}
	}

	void copyFile(File src, File dst) throws IOException {
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

}
