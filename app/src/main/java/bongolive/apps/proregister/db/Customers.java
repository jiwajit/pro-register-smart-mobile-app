/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proregister.MainScreen;
import bongolive.apps.proregister.R;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Log4Helper;
import bongolive.apps.proregister.utils.UrlCon;
import bongolive.apps.proregister.utils.Validating;


public class Customers {
    public static final String TABLENAME = "reg_customer";
    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderInstance.AUTHORITY + "/" + TABLENAME);
    public static final String ID = "_id";
    public static final String FNAME = "fname";
    public static final String SURNAME = "surname";
    public static final String MNAME = "mname";
    public static final String REFERENCE = "customer_id";
    public static final String DISTRICT = "district";
    public static final String REGION = "region";
    public static final String STREET = "street";
    public static final String ADDRESS = "address";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String TITLE = "title";
    public static final String GENDER = "gender";
    public static final String BDATE = "bdate";
    public static final String SIMSERIAL = "simerial";
    public static final String IMEI = "imei";
    public static final String CITIZENSHIP = "citizenship";
    public static final String JOB = "job";
    public static final String IDNO = "idno";
    public static final String IDTYPE = "idype";
    public static final String TELEPHONE = "telefone";
    public static final String CREATED = "createdon";
    public static final String UPDATED = "updatedon";
    public static final String LOCALUNIQUEID = "cust_id";
    public static final String STATUS = "approved";
    public static final String COMMENTS = "comment";
    public static final String REGID = "regid";
    public static final String SERVICES = "services_subscribed";
    public static final String LAT = "lat";
    public static final String LONGTUDE = "lng";
    public static final String SYNCSTATUS = "ack";//0 new //1 synced //2 update //3 delete
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.simreg.customers";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.simreg.customers";

    public static final String GETARRAYNAME = "customerList";
    public static final String SENDARRAYNAME = "customer";
    public static final String SENDARRAYNAME2 = "customerSentList";
    private static final String TAG = Customers.class.getSimpleName();
    static Logger log = Log4Helper.getLogger(TAG);

    public Customers() {

    }

    public Customers(long i, String n, String p, String d, String t, String j, String s) {
        this.id = i;
        this.name = n;
        this.sname = p;
        this.regdate = d;
        this.poneno = t;
        this.region = j;
        this
                .status = s;
    }

    // for checking if the customer(sy_cust_id) is present in the database locally
    private static int isCustomerPresent(Context context, int custid) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, null, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                    if (i == custid) {
                        return 1;
                    }
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    // for checking if the customer(sy_cust_id) is present in the database locally
    private static boolean isCustomerPresent(Context context, String custid) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + custid;
        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, where, null, null);
        try {
            if (c.moveToFirst()) {
                return true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return false;
    }

    static boolean isUnique;

    // for checking if the customer(sy_cust_id) is present in the database locally
    public static boolean isCustomerUnique(Context context, String tel) {
        ContentResolver cr = context.getContentResolver();
        String where = MOBILE + " = " + DatabaseUtils.sqlEscapeString(tel);
        Cursor c = cr.query(BASEURI, new String[]{MOBILE}, MOBILE + " = " + "255" + tel, null, null);
//        Cursor c = cr.query(BASEURI, new String[]{MOBILE}, where, null, null);
        try {
            if (c.moveToFirst()) {
                System.out.println("found ");
                isUnique = false;
            } else {
                System.out.println(tel + " not found");
                isUnique = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return isUnique;
    }

    public static boolean isIDNumberUnique(Context context, String imei) {
        ContentResolver cr = context.getContentResolver();
        String where = MOBILE + " = " + DatabaseUtils.sqlEscapeString(imei);
        Cursor c = cr.query(BASEURI, new String[]{IDNO}, IDNO + " = " + imei, null, null);
//        Cursor c = cr.query(BASEURI, new String[]{MOBILE}, where, null, null);
        try {
            if (c.moveToFirst()) {
                System.out.println("found ");
                isUnique = false;
            } else {
                System.out.println(imei + " not found");
                isUnique = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return isUnique;
    }


    public static boolean columnExist(SQLiteDatabase context, String column) {
        boolean ret = false;
        String sql = " SELECT * FROM " + TABLENAME + " LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " + indext);
            if (indext != -1)
                ret = true;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static String getCustomerBusinessName(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(SURNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getCustomerBusinessNameLoca(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(SURNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getDefaultCustomer(Context context) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(SURNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    //help to know if the customer is updating or creating
    public static int isCustomerNew(Context context, String buziness, String mobile) {
        ContentResolver cr = context.getContentResolver();
        String where = SURNAME + " = " + DatabaseUtils.sqlEscapeString(buziness) + " AND " + MOBILE + " = " +
                mobile;
//        String where = BUZINESSNAME + " LIKE \'%" + buziness + "%\'" + " AND " + MOBILE + " = " + mobile;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.getCount() > 0) {
                return 1;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static boolean insertCustomer(Context context, String[] string) {
        int customercount = getCustomerCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TITLE, string[0]);
        values.put(GENDER, string[1]);
        values.put(FNAME, string[2]);
        values.put(SURNAME, string[3]);
        values.put(REGION, string[4]);
        values.put(DISTRICT, string[5]);
        values.put(STREET, string[6]);
        values.put(ADDRESS, string[7]);
        values.put(BDATE, string[8]);
        values.put(JOB, string[9]);
        String mob = string[10];
        if (!mob.startsWith("255"))
            mob = "255".concat(mob);
        values.put(MOBILE, mob);
        values.put(SIMSERIAL, string[11]);
        values.put(IMEI, string[12]);
        values.put(CITIZENSHIP, string[13]);
        values.put(IDNO, string[14]);
        values.put(IDTYPE, string[15]);
        String tel = string[16];
        if (!tel.startsWith("255"))
            tel = "255".concat(tel);
        values.put(TELEPHONE, tel);
        values.put(EMAIL, string[17]);
        values.put(LOCALUNIQUEID, string[18]);
        values.put(LAT, string[19]);
        values.put(LONGTUDE, string[20]);
        values.put(MNAME, string[21]);
        values.put(SERVICES, string[22]);
        System.out.println("unique id " + string[18]);
        cr.insert(BASEURI, values);
        if (getCustomerCount(context) == customercount + 1) {
            return true;
        } else {
            StringBuilder buffer = new StringBuilder();
            for (String each : string)
                buffer.append(",").append(each);

            log.info("failed to insert a new subscriber the details are " + buffer);
        }
        return false;
    }


    public static boolean update(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(TITLE, string[0]);
        values.put(GENDER, string[1]);
        values.put(FNAME, string[2]);
        values.put(SURNAME, string[3]);
        values.put(REGION, string[4]);
        values.put(DISTRICT, string[5]);
        values.put(STREET, string[6]);
        values.put(ADDRESS, string[7]);
        values.put(BDATE, string[8]);
        values.put(JOB, string[9]);

        values.put(SIMSERIAL, string[11]);
        values.put(IMEI, string[12]);
        values.put(CITIZENSHIP, string[13]);
        values.put(IDNO, string[14]);
        values.put(IDTYPE, string[15]);
        values.put(TELEPHONE, string[16]);
        values.put(EMAIL, string[17]);
        values.put(LAT, string[18]);
        values.put(LONGTUDE, string[19]);
        values.put(MNAME, string[20]);
        String where = MOBILE + " = " + "255"+string[10];
//        System.out.println("CUSTOMER PHONE: "+string[10]);
//        System.out.println("DB CUSTOMER PHONE: "+DatabaseUtils.sqlEscapeString(string[10]));
//        String where = MOBILE + " = " + DatabaseUtils.sqlEscapeString(string[10]);
        if (cr.update(BASEURI, values, where, null) > 0)
            return true;
        else {
            StringBuilder buffer = new StringBuilder();
            for (String each : string)
                buffer.append(",").append(each);

            log.info("failed to insert a new subscriber the details are " + buffer);
        }
        return false;
    }

    public static int deleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, "3");
        String where = ID + " = " + string;
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }


    public static String getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{ID}, null, null, null);
        try {
            String k;
            if (c.moveToLast()) {
                k = c.getString(c.getColumnIndexOrThrow(ID));
                return k;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "";
    }

    public static int confirmdeleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + string;
        if (cr.delete(BASEURI, where, null) > 0)
            return 1;
        return 0;
    }

    public static int updateCustomer(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(REFERENCE, string[0]);
        values.put(UPDATED, Constants.getDate());
        String where = ID + " = " + string[1];
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }

    public static boolean updateCustomerStatus(Context context, String[] string) {
        System.out.println("updating with status " + string[1]);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(REGID, string[2]);
        values.put(STATUS, string[1]);
        values.put(COMMENTS, string[3]);
        values.put(UPDATED, Constants.getDate());
        String where = REFERENCE + " = " + string[0];
        if (cr.update(BASEURI, values, where, null) > 0) {
            System.out.println("updated with status " + string[1]);
            return true;
        }
        return false;
    }

    public static int getCustomerCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

    public static JSONObject getCustomerJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGGETCUSTOMER);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        UrlCon js = new UrlCon();
        String job = js.getJson(Constants.SERVER, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject checkStatusJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGVALIDATECUSTMER);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        UrlCon js = new UrlCon();
        String job = js.getJson(Constants.SERVER, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                System.out.println("STATUS: "+json);
                return json;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject prepareCustomerToSend(Context context) throws JSONException {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = 0";
        Cursor c = cr.query(BASEURI, null, where, null, null);

        UrlCon js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSENDCUSTOMER);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array;
            if (c.moveToFirst()) {
                JSONArray jarr = new JSONArray();
                do {
                    JSONObject j = new JSONObject();
                    j.put(TITLE, c.getString(c.getColumnIndexOrThrow(TITLE)));
                    j.put(GENDER, c.getString(c.getColumnIndexOrThrow(GENDER)));
                    j.put(FNAME, c.getString(c.getColumnIndexOrThrow(FNAME)));
                    j.put(SURNAME, c.getString(c.getColumnIndexOrThrow(SURNAME)));
                    j.put(REGION, c.getString(c.getColumnIndexOrThrow(REGION)));
                    j.put(DISTRICT, c.getString(c.getColumnIndexOrThrow(DISTRICT)));
                    j.put(STREET, c.getString(c.getColumnIndexOrThrow(STREET)));
                    j.put(ADDRESS, c.getString(c.getColumnIndexOrThrow(ADDRESS)));
                    j.put(BDATE, c.getString(c.getColumnIndexOrThrow(BDATE)));
                    j.put(JOB, c.getString(c.getColumnIndexOrThrow(JOB)));
                    j.put(MOBILE, c.getString(c.getColumnIndexOrThrow(MOBILE)));
                    j.put(SIMSERIAL, c.getString(c.getColumnIndexOrThrow(SIMSERIAL)));
                    j.put(IMEI, c.getString(c.getColumnIndexOrThrow(IMEI)));
                    j.put(CITIZENSHIP, c.getString(c.getColumnIndexOrThrow(CITIZENSHIP)));
                    j.put(IDNO, c.getString(c.getColumnIndexOrThrow(IDNO)));
                    j.put(IDTYPE, c.getString(c.getColumnIndexOrThrow(IDTYPE)));
                    j.put(TELEPHONE, c.getString(c.getColumnIndexOrThrow(TELEPHONE)));
                    j.put(EMAIL, c.getString(c.getColumnIndexOrThrow(EMAIL)));
                    j.put(LOCALUNIQUEID, c.getString(c.getColumnIndexOrThrow(LOCALUNIQUEID)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(MNAME, c.getString(c.getColumnIndexOrThrow(MNAME)));
                    j.put(SERVICES, c.getString(c.getColumnIndexOrThrow(SERVICES)));
                    jarr.put(j);
                } while (c.moveToNext());
                jsonObject.put("incommingcustomers", (Object) jarr);
                System.out.println(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
                Log.i("SERVER URL : " ,""+Constants.SERVER);
                JSONObject job = new JSONObject(array);
                Log.i("RETURNED JSON ARRAY : " ,""+job);
                return job;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public static void sendCustomersToServer(Context context) throws JSONException {
        JSONObject json = prepareCustomerToSend(context);
        if (json != null)
            try {
                if (json.has(Constants.SUCCESS)) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        if (json.has(SENDARRAYNAME)) {
                            JSONObject jsonObject = json.getJSONObject(SENDARRAYNAME);
                            if (jsonObject.has(SENDARRAYNAME2)) {
                                JSONArray jsonArray = jsonObject.getJSONArray(SENDARRAYNAME2);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject js = jsonArray.getJSONObject(i);
                                    String pdn = js.getString(REFERENCE);
                                    String customerid = js.getString(ID);
                                    String[] stockvalue = new String[]{pdn, customerid};
                                    int custidpresent = isCustomerPresent(context, Integer.parseInt(pdn));
                                    if (Validating.areSet(stockvalue)) {// is the customer system id present locally?
                                        switch (custidpresent) {
                                            case 0:
                                                if (updateCustomer(context, stockvalue) == 1) {
                                                    updateSync(context, Integer.parseInt(pdn));
                                                }
                                            case 1:
                                                //the customer system id is present ignore the request
                                                break;
                                        }
                                    } else {
                                        log.info("reference and or customer id is nul they are " + pdn +
                                                " and " + customerid);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                log.error("SENDING FAILS");
//                new MainScreen().displayBackgroundErrror("Could not send the phone number for registration");
            }
    }

    public static void processValidation(Context context) throws JSONException {
        JSONObject json = checkStatusJson(context);
        if (json != null)
            try {
                if (json.has(Constants.SUCCESS)) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        if (json.has(GETARRAYNAME)) {
                            JSONArray jsonArray = json.getJSONArray(GETARRAYNAME);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject js = jsonArray.getJSONObject(i);
                                String pdn = js.getString(REFERENCE);
                                String status = js.getString(STATUS);
                                String regid = js.getString(REGID);
                                String comm = "No Comments";
                                if (js.has(COMMENTS))
                                    comm = js.getString(COMMENTS);
                                if (!Validating.areSet(comm))
                                    comm = context.getString(R.string.strnocomments);
                                String[] stockvalue = new String[]{pdn, status, regid, comm};
                                if (!Validating.areSet(status))
                                    status = "0";

                                if (Validating.areSet(stockvalue)) {// is the customer system id present locally?
                                    boolean custidpresent = isCustomerPresent(context, pdn);
                                    if (custidpresent) {
                                        updateCustomerStatus(context, stockvalue);
                                    } else {
                                        log.info("customer is not present in the device with id " +
                                                "" + pdn);
                                    }
                                } else {
                                    log.info("reference or status or regid or comment is null " +
                                            "ther are reference " + pdn + " status " + status + " regid " +
                                            "" + regid + " comment " + comm);
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
//                new MainScreen().displayBackgroundErrror("Could not send the phone number for registration");
                log.error("SENDING FAILD");
            }
    }

    public static int updateSync(Context context, int customerid) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, 1);
        String where = REFERENCE + " = " + customerid;
        if (cr.update(BASEURI, values, where, null) == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static int resetsync(Context context) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, 0);
        if (cr.update(BASEURI, values, null, null) == 1) {
            cr.update(MultimediaContents.BASEURI, values, null, null);
            return 1;
        } else {
            return 0;
        }
    }

    public static void sendAck(Context context, String[] requiredid) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKKEY, REFERENCE);
        jobs.put(REFERENCE, requiredid[0]);
        UrlCon js = new UrlCon();
        js.getJson(Constants.SERVER, jobs);
    }

    public static String[] getAllCustomers(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BASEURI, null, null, null, null);
        try {
            if (cursor.getCount() > 0) {
                String[] str = new String[cursor.getCount()];
                int i = 0;

                while (cursor.moveToNext()) {
                    str[i] = cursor.getString(cursor.getColumnIndex(SURNAME));
                    i++;
                }
                cursor.close();
                return str;
            } else {
                cursor.close();
                return new String[]{};
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int getCustomerid(Context context, String businessnm) {
//        businessnm = businessnm.replace(",", "\'\'");
        ContentResolver cr = context.getContentResolver();
        String where = SURNAME + " = " + DatabaseUtils.sqlEscapeString(businessnm);

        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int getCustomerlocalId(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = SURNAME + " = " + DatabaseUtils.sqlEscapeString(businessnm);

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static ArrayList<String> getSearchedCustomer(Context context, String businessnm) {
        ArrayList<String> custlist = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = SURNAME + " = " + DatabaseUtils.sqlEscapeString(businessnm) + " OR " + SURNAME + " = "
                + DatabaseUtils.sqlEscapeString(businessnm);
//        String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\' OR "+ CUSTOMERNAME + " LIKE \'%" + businessnm + "%'" ;

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    custlist.add(c.getString(c.getColumnIndexOrThrow(ID)));

                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static ArrayList<String> getAllBuz(Context context) {
        ArrayList<String> custlist = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();

        Cursor c = cr.query(BASEURI, new String[]{ID}, null, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    custlist.add(c.getString(c.getColumnIndexOrThrow(ID)));

                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static ArrayList<Customers> getItems(Context context) {
        ArrayList<Customers> custlist = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndex(Customers.FNAME));
                    String p = c.getString(c.getColumnIndex(Customers.SURNAME));
                    String d = c.getString(c.getColumnIndex(Customers.CREATED));
                    String t = c.getString(c.getColumnIndex(Customers.MOBILE));
                    String j = c.getString(c.getColumnIndex(Customers.REGION));
                    String s = c.getString(c.getColumnIndex(Customers.STATUS));
                    long i = c.getLong(c.getColumnIndex(Customers.ID));
                    custlist.add(new Customers(i, n, p, d, t, j, s));
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static int isCustomerSynced(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm;

        Cursor c = cr.query(BASEURI, new String[]{SYNCSTATUS}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(SYNCSTATUS));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static String[] getCustomers(Context context, long ref) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + ref;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        String[] custdetails;
        try {
            if (c.moveToFirst()) {
                custdetails = new String[]{c.getString(c.getColumnIndex(TITLE)),
                        c.getString(c.getColumnIndex(GENDER)), c.getString(c.getColumnIndex(FNAME)),
                        c.getString(c.getColumnIndex(SURNAME)), c.getString(c.getColumnIndex(REGION)),
                        c.getString(c.getColumnIndex(DISTRICT)), c.getString(c.getColumnIndex(STREET)),
                        c.getString(c.getColumnIndex(ADDRESS)), c.getString(c.getColumnIndex(BDATE)),
                        c.getString(c.getColumnIndex(JOB)), c.getString(c.getColumnIndex(MOBILE)),
                        c.getString(c.getColumnIndex(SIMSERIAL)), c.getString(c.getColumnIndex(IMEI)),
                        c.getString(c.getColumnIndex(CITIZENSHIP)), c.getString(c.getColumnIndex(IDNO)),
                        c.getString(c.getColumnIndex(IDTYPE)), c.getString(c.getColumnIndex(TELEPHONE)),
                        c.getString(c.getColumnIndex(SYNCSTATUS)), c.getString(c.getColumnIndex(CREATED)),
                        c.getString(c.getColumnIndex(EMAIL)), MultimediaContents.getuserphoto(context, ref),
                        MultimediaContents.getidhoto(context, ref), MultimediaContents.getsignhoto(context, ref)
                };
                return custdetails;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public static ArrayList<Customers> getcustdetais(Context context, long ref) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + ref;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        ArrayList<Customers> cd = new ArrayList<>();
        try {
            if (c.moveToFirst()) {
                cd.add(new Customers(
                        c.getString(c.getColumnIndex(TITLE)),
                        c.getString(c.getColumnIndex(GENDER)),
                        c.getString(c.getColumnIndex(FNAME)),
                        c.getString(c.getColumnIndex(SURNAME)),
                        c.getString(c.getColumnIndex(REGION)),
                        c.getString(c.getColumnIndex(DISTRICT)),
                        c.getString(c.getColumnIndex(STREET)),
                        c.getString(c.getColumnIndex(ADDRESS)),
                        c.getString(c.getColumnIndex(BDATE)),
                        c.getString(c.getColumnIndex(JOB)),
                        c.getString(c.getColumnIndex(MOBILE)),
                        c.getString(c.getColumnIndex(SIMSERIAL)),
                        c.getString(c.getColumnIndex(IMEI)),
                        MultimediaContents.getuserphoto(context, ref),
                        c.getString(c.getColumnIndex(CITIZENSHIP)),
                        c.getString(c.getColumnIndex(IDNO)),
                        c.getString(c.getColumnIndex(IDTYPE)),
                        MultimediaContents.getidhoto(context, ref),
                        c.getString(c.getColumnIndex(TELEPHONE)),
                        c.getString(c.getColumnIndex(EMAIL)),
                        MultimediaContents.getsignhoto(context, ref),
                        c.getString(c.getColumnIndex(CREATED)),
                        c.getString(c.getColumnIndex(UPDATED)),
                        c.getLong(c.getColumnIndex(ID)),
                        c.getInt(c.getColumnIndex(SYNCSTATUS)),
                        c.getString(c.getColumnIndex(REGID)),
                        c.getString(c.getColumnIndex(STATUS)),
                        c.getString(c.getColumnIndex(COMMENTS)),
                        c.getString(c.getColumnIndex(MNAME))
                ));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return cd;
    }
/*
    public static String getCustomerName(Context context, long reference) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + reference;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String custdetails = "";
        try {
            if (c.moveToFirst()) {
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                custdetails = b;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custdetails;
    }
*/


    public static DataPoint[] getTotalSubscribersGraph(Context context) {

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 0 then IFNULL(" + ID + "," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 1 then IFNULL(" + ID + ",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 2 then IFNULL(" + ID + ",0)   else 0 end) as " +
                "tuesday ," +
                "sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 3 then IFNULL(" + ID + ",0)   else 0 end) as " +
                "wednesday ," +
                "sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 4 then IFNULL(" + ID + ",0)   else 0 end) as " +
                "thursday ," +
                "sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 5 then IFNULL(" + ID + ",0)   else 0 end) as " +
                "friday ," +
                "sum(case  cast(strftime('%w'," + CREATED + ") as integer) when 6 then IFNULL(" + ID + ",0)   else 0 end) as " +
                "saturday " +
                "from " + TABLENAME + " where  strftime('%W'," + CREATED + ") = strftime('%W','now') group by strftime('%W','now') ";

        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if (c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }

    String title, gender, name, sname, mname, region, district, street, address, bdate, occuption, poneno,
            simserial, imei, subphoto,
            citizenship, idno, idtype, idphoto, telephone, email, signaturephoto, regdate, updated, comment, regid, status;
    long id;
    int ref, ack;


    public Customers(String title, String gender, String name, String sname, String region, String district, String
            street, String address, String bdate, String occuption, String poneno, String simserial, String imei,
                     String subphoto, String citizenship, String idno, String idtype, String idphoto, String
                             telephone, String email, String signaturephoto, String regdate, String upd, long id, int
                             ack, String mname) {
        this.title = title;
        this.gender = gender;
        this.name = name;
        this.sname = sname;
        this.region = region;
        this.district = district;
        this.street = street;
        this.address = address;
        this.bdate = bdate;
        this.occuption = occuption;
        this.poneno = poneno;
        this.simserial = simserial;
        this.imei = imei;
        this.subphoto = subphoto;
        this.citizenship = citizenship;
        this.idno = idno;
        this.idtype = idtype;
        this.idphoto = idphoto;
        this.telephone = telephone;
        this.email = email;
        this.signaturephoto = signaturephoto;
        this.regdate = regdate;
        this.id = id;
        this.ack = ack;
        this.updated = upd;
        this.mname = mname;
    }


    public Customers(String title, String gender, String name, String sname, String region, String district, String
            street, String address, String bdate, String occuption, String poneno, String simserial, String imei,
                     String subphoto, String citizenship, String idno, String idtype, String idphoto, String
                             telephone, String email, String signaturephoto, String regdate, String upd, long id, int
                             ack, String rid, String st, String comm, String mname) {
        this.title = title;
        this.gender = gender;
        this.name = name;
        this.sname = sname;
        this.region = region;
        this.district = district;
        this.street = street;
        this.address = address;
        this.bdate = bdate;
        this.occuption = occuption;
        this.poneno = poneno;
        this.simserial = simserial;
        this.imei = imei;
        this.subphoto = subphoto;
        this.citizenship = citizenship;
        this.idno = idno;
        this.idtype = idtype;
        this.idphoto = idphoto;
        this.telephone = telephone;
        this.email = email;
        this.signaturephoto = signaturephoto;
        this.regdate = regdate;
        this.id = id;
        this.ack = ack;
        this.updated = upd;
        this.regid = rid;
        this.status = st;
        this.comment = comm;
        this.mname = mname;
    }

    public String getTitle() {
        return title;
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getSname() {
        return sname;
    }

    public String getRegion() {
        return region;
    }

    public String getDistrict() {
        return district;
    }

    public String getStreet() {
        return street;
    }

    public String getAddress() {
        return address;
    }

    public String getBdate() {
        return bdate;
    }

    public String getOccuption() {
        return occuption;
    }

    public String getPoneno() {
        return poneno;
    }

    public String getSimserial() {
        return simserial;
    }

    public String getImei() {
        return imei;
    }

    public String getSubphoto() {
        return subphoto;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public String getIdno() {
        return idno;
    }

    public String getIdtype() {
        return idtype;
    }

    public String getIdphoto() {
        return idphoto;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEmail() {
        return email;
    }

    public String getRegid() {
        return regid;
    }

    public String getStatus() {
        return status;
    }

    public String getComment() {
        return comment;
    }

    public String getSignaturephoto() {
        return signaturephoto;
    }

    public String getRegdate() {
        return regdate;
    }

    public String getUpdated() {
        return updated;
    }

    public long getId() {
        return id;
    }

    public int getRef() {
        return ref;
    }

    public String getMname() {
        return mname;
    }

    public int getAck() {
        return ack;
    }
}
