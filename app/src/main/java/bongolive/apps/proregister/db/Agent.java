/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister.db;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.UrlCon;


public class Agent {
    public static final String TABLENAME = "customers";
    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderInstance.AUTHORITY + "/" + TABLENAME);
    public static final String ID = "_id";
    public static final String NAME = "agent_name";
    public static final String AGENTNO = "agent_no";
    public static final String REFERENCE = "system_agent_id";
    public static final String DISTRICT = "district";
    public static final String REGION = "region";
    public static final String STREET = "street";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String LOCALAGENTID = "agent_id";
    public static final String PASS = "pass";
    public static final String CREATED = "createdon";
    public static final String SYNCSTATUS = "ack";//0 new //1 synced //2 update //3 delete
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.simreg.agent";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.simreg.agent";

    public static final String GETARRAYNAME = "customerList";
    public static final String SENDARRAYNAME = "customer";
    public static final String SENDARRAYNAME2 = "customerSentList";
    private static final String TAG = "CUSTOMERS";

    int id, reference, sync;
    String cname, bname, area, city, mob, email, lat, lng, street;

    public Agent(int id, int ref, int sy, String cn, String bn
            , String ar, String ci, String mo, String em, String lt, String lng, String srt) {
        this.id = id;
        this.reference = ref;
        this.sync = sy;
        this.cname = cn;
        this.bname = bn;
        this.area = ar;
        this.city = ci;
        this.mob = mo;
        this.email = em;
        this.lat = lt;
        this.lng = lng;
        this.street = srt;
    }

    public Agent() {

    }


    public static int getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToLast())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    private static String getLocalUniqueId(Context context){
        return "";
    }


  /*  // for checking if the customer(sy_cust_id) is present in the database locally
    private static int isCustomerPresent(Context context, int custid) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, null, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                    if (i == custid) {
                        return 1;
                    }
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static String getCustomerBusinessName(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getCustomerBusinessNameLoca(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }
*/
/*

    public static int insertCustomer(Context context, String[] string) {
        int customercount = getCustomerCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(REFERENCE, string[2]);
        values.put(AREA, string[3]);
        values.put(CITY, string[4]);
        values.put(MOBILE, string[5]);
        values.put(EMAIL, string[6]);
        values.put(LAT, string[7]);
        values.put(LONGTUDE, string[8]);
        values.put(SYNCSTATUS, 0);
        values.put(STREET, string[10]);
        cr.insert(BASEURI, values);
        if (getCustomerCount(context) == customercount + 1) {
            return 1;
        }
        return customercount;
    }

    public static int updateCustomerDetails(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
//		String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\'"  ;
        String where = ID + " = " + string[5];

        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(AREA, "");
        values.put(CITY, "");
        values.put(MOBILE, string[2]);
        values.put(EMAIL, string[3]);
        values.put(LAT, 0);
        values.put(LONGTUDE, 0);
        values.put(SYNCSTATUS, 2);
        values.put(STREET, string[4]);

        int customercount = cr.update(BASEURI, values, where, null);

        return customercount;
    }
*/

    public static int deleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, "3");
        String where = ID + " = " + string;
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }

    public static int confirmdeleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + string;
        if (cr.delete(BASEURI, where, null) > 0)
            return 1;
        return 0;
    }

    public static int updateCustomer(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(REFERENCE, string[0]);
        String where = ID + " = " + string[1];
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }


    public static int getCustomerCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

    public static JSONObject getCustomerJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGGETCUSTOMER);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        UrlCon js = new UrlCon();
        String job = js.getJson(Constants.SERVER, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
