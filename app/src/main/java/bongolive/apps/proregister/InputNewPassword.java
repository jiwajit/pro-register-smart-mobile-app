package bongolive.apps.proregister;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.UrlCon;

public class InputNewPassword extends AppCompatActivity {

    private TextView resetpassword, cancel;
    private EditText mPasswordView, mRepasswordView;
    private ProgressDialog progressDialog;
    private UserNewPasswordTask mAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_new_password);
        mPasswordView = (EditText) findViewById(R.id.password);
        mRepasswordView = (EditText) findViewById(R.id.re_password);
        final String password = mPasswordView.getText().toString();
        resetpassword = (TextView) findViewById(R.id.btn_reset);
        cancel = (TextView) findViewById(R.id.btn_cancel);
        progressDialog = new ProgressDialog(getBaseContext(), R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.getWindow().setGravity(Gravity.CENTER);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        resetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPasswordView.getText().toString().equals("") && mRepasswordView.getText().toString().equals("")) {
                    mPasswordView.setError("fill the new password");
                } else if (mPasswordView.getText().toString() != mRepasswordView.getText().toString()) {
                    mRepasswordView.setError("password mismatch");
                } else if (mRepasswordView.getText().toString().length() < 4) {
                    mPasswordView.setError("password should be at least 4 characters");
                } else {
                    mAuthTask = new UserNewPasswordTask(mPasswordView.getText().toString());
                    mAuthTask.execute((Void) null);
//                    progressDialog.show();
                }
            }
        });

    }

    public class UserNewPasswordTask extends AsyncTask<Void, Void, JSONObject> {

        private final String newPassword;

        UserNewPasswordTask(String password) {
            this.newPassword = password;

        }

        @Override
        public JSONObject doInBackground(Void... params) {

            UrlCon js = new UrlCon();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("tag", Constants.TAGRESETPASSWORD);
                jsonObject.put(Constants.IMEI, Constants.getIMEINO(getBaseContext()));

                String array;
                JSONArray jarr = new JSONArray();
                JSONObject j = new JSONObject();
                j.put("password", newPassword);
                jarr.put(j);
                jsonObject.put("new_password", jarr);
                System.out.println("SENT DATA: " + jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
                JSONObject job = new JSONObject(array);
                System.out.println("RETURNED JSON DATA: " + job);
                return job;
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                if (result.toString().contains("success")) {
                    System.out.println("SUCCESS");
                    Intent intent = new Intent(getBaseContext(), InputNewPassword.class);
                    startActivity(intent);
                    finish();
                } else if (result.toString().contains("failed")) {
                    mPasswordView.setError("password change failed ,try again");
                }
//                progressDialog.dismiss();
            } catch (Exception e) {
            }

        }

        @Override
        protected void onCancelled() {
        }
    }


}
