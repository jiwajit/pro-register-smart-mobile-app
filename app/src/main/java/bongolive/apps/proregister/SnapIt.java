/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */


package bongolive.apps.proregister;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class SnapIt extends SurfaceView implements SurfaceHolder.Callback {

    SurfaceHolder mHolder;
    Camera mCamera;

    private String uniqueId;

    public SnapIt(Context context) {
        super(context);
    }

    public SnapIt ( Context context,Camera camera){
        super(context);
        mCamera = camera;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void setPreviewScreen(){
        try{
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (IOException e){
            Log.e("image_preview","error previewig image "+e.getMessage());
        }
    }

    public void removePreviewScreen(){
        try{
            Log.e("TABACT", "surfaceDestroyed()");
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);
                mCamera.release();
                mCamera = null;

        } catch (Exception e){
            Log.e("image_preview","error previewig image "+e.getMessage());
        }
    }

    public void surfaceChanged(SurfaceHolder holder,int format, int w, int h){
        if(mHolder.getSurface() == null){
            return;
        }

        try{
            mCamera.stopPreview();
        } catch (Exception e){
            e.printStackTrace();
        }

        try{
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (IOException e){
            e.getMessage();
        }

    }
    public boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    public static Camera getCameraInstance(){
        Camera c = null;
        try{
            c = Camera.open();
        } catch (Exception e){
            e.printStackTrace();
        }
        return c;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }


    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}