/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */


package bongolive.apps.proregister;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import bongolive.apps.proregister.utils.Constants;

public class SignatureDrawer extends Activity {

    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;
    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    AppPreference appPreference;
    int source;

    private String uniqueId;
    private Camera mCamera;
    private SnapIt snapIt;
    FrameLayout frameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        appPreference = new AppPreference(this);
        Intent intent = getIntent();
        source = intent.getExtras().getInt("source");
        System.out.println("SOURCE IS "+source);
        if(source == 0) {
        setContentView(R.layout.signature);

            tempDir = Environment.getExternalStorageDirectory() + "/proregister/.tmp/";
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

            prepareDirectory();
            uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + Math.random();
            current = uniqueId + ".png";
            mypath = new File(directory, current);


            mContent = (LinearLayout) findViewById(R.id.linearLayout);
            mSignature = new signature(this, null);
            mSignature.setBackgroundColor(Color.WHITE);
            mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mClear = (Button) findViewById(R.id.clear);
            mGetSign = (Button) findViewById(R.id.getsign);
            mGetSign.setEnabled(false);
            mCancel = (Button) findViewById(R.id.cancel);
            mView = mContent;


            mClear.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Cleared");
                    mSignature.clear();
                    mGetSign.setEnabled(false);
                }
            });

            mGetSign.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mView.setDrawingCacheEnabled(true);
                    String bitmap = mSignature.save(mView);
                    if (bitmap != null) {
                        Log.v("log_tag", "uri " + bitmap);
                        appPreference.saveSubscriberSignPhoto(bitmap);
                        Intent intent = new Intent();
                        intent.putExtra("data", bitmap);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            });

            mCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Canceled");
                    Bundle b = new Bundle();
                    Intent intent = new Intent();
                    intent.putExtras(b);
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
            });
        } else {
            setContentView(R.layout.snapit);
            mCamera = SnapIt.getCameraInstance();

            snapIt = new SnapIt(this,mCamera);
            snapIt.setPreviewScreen();
            frameLayout = (FrameLayout)findViewById(R.id.camera_preview);
            frameLayout.addView(snapIt);
            mClear = (Button) findViewById(R.id.clear);
            mClear.setVisibility(View.GONE);
            mGetSign = (Button) findViewById(R.id.getsign);
            mCancel = (Button) findViewById(R.id.cancel);
            mGetSign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCamera.takePicture(null,null,mPicture);
                }
            });
            mCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Canceled");
                    Bundle b = new Bundle();
                    Intent intent = new Intent();
                    intent.putExtras(b);
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
            });
        }
    }
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                Log.d(SignatureDrawer.class.getName(), "Error creating media file, check storage permissions: " );
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d(SignatureDrawer.class.getName(), "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(SignatureDrawer.class.getName(), "Error accessing file: " + e.getMessage());
            }
            if(pictureFile != null)
            {
                String path = pictureFile.getAbsolutePath().toString();
                System.out.println(path);
                Log.v("log_tag", "uri " + path);

                if(source == 1)
                    appPreference.saveSubscriberPhoto(path);
                if(source == 2)
                    appPreference.saveSubscriberIdPhoto(path);
                Intent intent = new Intent();
                intent.putExtra("data", path);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    };
    @Override
    protected void onDestroy() {
        Log.w("GetSignature", "onDestory");
        super.onDestroy();
    }

    private String getTodaysDate() {

        final Calendar c = Calendar.getInstance();
        int todaysDate =     (c.get(Calendar.YEAR) * 10000) +
                ((c.get(Calendar.MONTH) + 1) * 100) +
                (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:",String.valueOf(todaysDate));
        return(String.valueOf(todaysDate));

    }

    private String getCurrentTime() {

        final Calendar c = Calendar.getInstance();
        int currentTime =     (c.get(Calendar.HOUR_OF_DAY) * 10000) +
                (c.get(Calendar.MINUTE) * 100) +
                (c.get(Calendar.SECOND));
        Log.w("TIME:",String.valueOf(currentTime));
        return(String.valueOf(currentTime));

    }


    private boolean prepareDirectory()
    {
        try
        {
            if (makedirs())
            {
                return true;
            } else {
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Could not initiate File System.. Is Sdcard mounted properly?", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private boolean makedirs()
    {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory())
        {
            File[] files = tempdir.listFiles();
            for (File file : files)
            {
                if (!file.delete())
                {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }

    public class signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public String save(View v)
        {
            Uri uri = null;
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            String bitmap = null;
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap (mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
            }
            Canvas canvas = new Canvas(mBitmap);
            try
            {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
               /* mFileOutStream.flush();
                mFileOutStream.close();
                String url = MediaStore.Images.Media.insertImage(getContentResolver(), mBitmap, "title", null);
                Log.v("log_tag","url: " + url);
                bitmap = url;*/
                //In case you want to delete the file
                //boolean deleted = mypath.delete();
                //Log.v("log_tag","deleted: " + mypath.toString() + deleted);
                //If you want to convert the image to string use base64 converter
                bitmap = saveImage(mBitmap);
            }
            catch(Exception e)
            {
                Log.v("log_tag", e.toString());
            }
            return bitmap;
        }

        public void clear()
        {
            path.reset();
            invalidate();
        }

        private String saveImage(Bitmap bitmap) {

            String stored = null;

            File sdcard = Environment.getExternalStorageDirectory() ;

            File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
            folder.mkdir();
            String filename = String.valueOf(System.currentTimeMillis());
            File file = new File(folder.getAbsoluteFile(), filename ) ;
            if (file.exists())
                return stored ;

            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                stored = file.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stored;
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++)
                    {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string){
        }

        private void expandDirtyRect(float historicalX, float historicalY)
        {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY)
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    private static File getOutputMediaFile(int type){

        File sdcard = Environment.getExternalStorageDirectory() ;

        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
        if(!folder.exists())
        folder.mkdir();
        String filename = String.valueOf(System.currentTimeMillis());
        File file = new File(folder.getAbsoluteFile(), filename ) ;

        return file;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(source > 0)
            frameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(source > 0)
            frameLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(source > 0){
            snapIt.removePreviewScreen();
        }
    }
}