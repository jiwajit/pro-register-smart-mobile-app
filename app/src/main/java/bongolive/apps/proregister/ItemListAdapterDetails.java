/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proregister.db.Customers;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Validating;

public class ItemListAdapterDetails extends RecyclerView.Adapter<ItemListAdapterDetails.ItemViewHolder> {
    int action;
    Context context;
    Dialog dialog;
    boolean tabletsize ;
    private List<Customers> mItems = new ArrayList<Customers>();
    private View.OnClickListener onClickListener ;
    public ItemListAdapterDetails() {
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.customer_details, viewGroup, false);
        ItemViewHolder vh = new ItemViewHolder(v);
        v.setOnClickListener(onClickListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder h, final int i) {
        final Customers cl = mItems.get(i);
        h.txttile.setText(cl.getTitle());
        h.txtgender.setText(cl.getGender());
        h.txtnm.setText(cl.getName());
        h.txtsnm.setText(cl.getSname());
        h.txtmnm.setText(cl.getMname());
        h.txtreg.setText(cl.getRegion());
        h.txtdis.setText(cl.getDistrict());
        h.txtstreet.setText(cl.getStreet());
        h.txtadd.setText(cl.getAddress());
        h.txtbdt.setText(cl.getBdate());
        h.txtoccup.setText(cl.getOccuption());
        h.txtphn.setText(cl.getPoneno());
        h.txtserial.setText(cl.getSimserial());
        h.txtimei.setText(cl.getImei());
        if (getimage(cl.getSubphoto()))
            h.subphoto.setImageBitmap(Constants.decodeImg(cl.getSubphoto(), 200, 200));
        h.txtcitizen.setText(cl.getCitizenship());
        h.txtidno.setText(cl.getIdno());
        h.txtidtype.setText(cl.getIdtype());
        if (getimage(cl.getIdphoto()))
            h.idphoto.setImageBitmap(Constants.decodeImg(cl.getIdphoto(), 200, 200));
        h.txtotherphn.setText(cl.getTelephone());
        h.txtmail.setText(cl.getEmail());
        if (getimage(cl.getSignaturephoto()))
            h.signphoto.setImageBitmap(Constants.decodeImg(cl.getSignaturephoto(), 200, 200));
        h.txtregdate.setText(cl.getRegdate());
        h.txtack.setText(cl.getAck() == 1 ? "Received on Server" : "Not Received");
        h.txtsyndate.setText(Validating.areSet(cl.getUpdated()) ? cl.getUpdated() : "Not Sent");
        h.txtregid.setText(Validating.areSet(cl.getRegid()) ? cl.getRegid() : "Not Sent");

        String status = cl.getStatus();
        if(status.equals("0")){
            status = context.getString(R.string.strpending);
            h.txtstatus.setTextColor(context.getResources().getColor(R.color.pending));
        } else if (status.equals("1")){
            status = context.getString(R.string.strapproved);
            h.txtstatus.setTextColor(context.getResources().getColor(R.color.approved));
        } else if (status.equals("2")){
            status = context.getString(R.string.strrejected);
            h.txtstatus.setTextColor(context.getResources().getColor(R.color.rejected));
        }
        h.txtstatus.setText(status);
        h.txtregid.setText(Validating.areSet(cl.getRegid()) ? cl.getRegid() : "Not Sent");

        h.txtregid.setText(cl.getRegid());
        h.txtcomment.setText(cl.getComment());

    }


    public ItemListAdapterDetails(Context context2, List<Customers> items,Dialog mDialog) {
        context = context2;
        mItems = items;
        tabletsize = context2.getResources().getBoolean(R.bool.isTablet);
        dialog = mDialog;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @Override
    public int getItemCount() {
//        System.out.println("size is "+ mItems.size());
        return mItems.size();
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txttile,txtgender,txtnm,txtsnm,txtreg,txtdis,txtstreet,txtadd,txtoccup,txtphn,txtserial,txtimei,
                txtbdt;  ImageView subphoto;
        TextView txtidno, txtcitizen,txtidtype; ImageView idphoto ;
        TextView txtregid, txtstatus,txtcomment,txtmnm;
        TextView txtotherphn,txtmail,txtregdate,txtack,txtsyndate;ImageView signphoto;

        public ItemViewHolder(View v) {
            super(v);
            txttile = (TextView) v.findViewById(R.id.txttitlev);
            txtgender = (TextView) v.findViewById(R.id.txtgenderv);
            txtnm = (TextView) v.findViewById(R.id.txtfnamev);
            txtsnm = (TextView) v.findViewById(R.id.txtsnamev);
            txtmnm = (TextView) v.findViewById(R.id.txtmnamev);
            txtreg = (TextView) v.findViewById(R.id.txtregionv);
            txtdis = (TextView) v.findViewById(R.id.txtdistrictv);
            txtstreet = (TextView) v.findViewById(R.id.txtstreetv);
            txtadd = (TextView) v.findViewById(R.id.txtaddressv);
            txtbdt = (TextView) v.findViewById(R.id.txtbdatev);
            txtoccup = (TextView) v.findViewById(R.id.txtoccupationv);
            txtphn = (TextView) v.findViewById(R.id.txtsimphonev);
            txtserial = (TextView) v.findViewById(R.id.txtsimserialv);
            txtimei = (TextView) v.findViewById(R.id.txtimeiv);
            subphoto = (ImageView)v.findViewById(R.id.imguserphotov);
            subphoto.setRotation(90);

            /* end of subscriber personal info */
            txtidno = (TextView) v.findViewById(R.id.txtidnumberv);
            txtcitizen = (TextView) v.findViewById(R.id.txtcitizenv);
            txtidtype = (TextView) v.findViewById(R.id.txtidtypev);
            idphoto = (ImageView)v.findViewById(R.id.imgidphotov);
            idphoto.setRotation(90);

            /* end of subscriber id  info */

            txtotherphn = (TextView) v.findViewById(R.id.txttelv);
            txtmail = (TextView) v.findViewById(R.id.txtemailv);
            signphoto = (ImageView)v.findViewById(R.id.imgsignphotov);
            txtregdate = (TextView) v.findViewById(R.id.txtregdatev);
            txtack = (TextView) v.findViewById(R.id.txtackv);
            txtsyndate = (TextView) v.findViewById(R.id.txtsyncdatev);

            /* end of other  info */

            txtregid = (TextView) v.findViewById(R.id.txtregidv);
            txtstatus = (TextView) v.findViewById(R.id.txtstatusv);
            txtcomment = (TextView) v.findViewById(R.id.txtcommentv);

        }
    }
    public boolean getimage(String path){
        if(Validating.areSet(path)) {
            File imgFile = new File(path);

            if (imgFile.exists()) {
                return true;
            }
        }
        return false;
    }
}
