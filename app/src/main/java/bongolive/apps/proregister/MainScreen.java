package bongolive.apps.proregister;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.splunk.mint.Mint;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Locale;

import bongolive.apps.proregister.db.ContentProviderInstance;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Log4Helper;
import bongolive.apps.proregister.utils.PermissionUtils;
import bongolive.apps.proregister.utils.UrlCon;
import bongolive.apps.proregister.utils.Validating;

public class MainScreen extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
        , LocationListener {

    private static final String TAG = MainScreen.class.getName();
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private String location;
    NavigationView mNavigationView;
    FrameLayout mContentFrame;
    private PopupWindow mPopupWindow;
    RelativeLayout mRelativeLayout;

    private static final int REQUEST_CHECK_SETTINGS = 11;
    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final int REGISTER_SUBSCRIBER = 5;
    Logger log = Log4Helper.getLogger(TAG);
    ;
    String extra = null;

    boolean tabletSize = false;
    static Account mAccount;
    private static final String PREFERENCES_FILE = "menu_settings";
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private int mCurrentSelectedPosition;

    private AppPreference appPreference;
    TextView txtv;

    /* location */
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private boolean mPermissionDenied = false;
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private Location currentlocation;
    private boolean LOCATION_ALLOWED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mint.initAndStartSession(this.getApplication(), "aa127120");
        appPreference = new AppPreference(getApplicationContext());
        appPreference.setDefaultLanguage();
        String languageToLoad = appPreference.getDefaultLanguage();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 2);
        }

        System.out.println("default language is  " + languageToLoad);
        if (!Validating.areSet(languageToLoad)) {
            languageToLoad = "sw";
        }


        location = "" + appPreference.getSettings(Constants.TRACKING_GPS);
        System.out.println("LOCATION GPS: " + location);
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.nav_drawer);

        setUpToolbar();

        buildGoogleApiClient();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);

        mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(this, PREF_USER_LEARNED_DRAWER, "false"));

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
//            updateValuesFromBundle(savedInstanceState);
        }

        extra = Constants.SEARCHSOURCE_MAIN;

        mAccount = createSyncAccount(this);

        String time = appPreference.getDefaultSync();

        if (time.isEmpty()) {
            time = getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
        }
        final int t = Integer.parseInt(time);

        periodicadapersync(mAccount, t);

        setUpNavDrawer();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mContentFrame = (FrameLayout) findViewById(R.id.content_frame);

        FrameLayout frameLayout = (FrameLayout) mNavigationView.getHeaderView(0);
        txtv = (TextView) frameLayout.getChildAt(2);
        String version = Constants.getVersionName(this);
        txtv.setText(getString(R.string.strversion) + " :  " + version);


        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                Intent intent;
                menuItem.setChecked(true);
                FragmentManager fragmentManager = getSupportFragmentManager();
                Bundle args = new Bundle();
                if (!tabletSize)
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        extra = Constants.SEARCHSOURCE_HOME;
                        Home bb = new Home();
                        args.putInt(ARG_SECTION_NUMBER, 1);
                        bb.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, bb).
                                commit();
                        break;
                    case R.id.nav_registration:
                        appPreference.clearStates();
                        appPreference.clearPhotos(null);
                        Intent intent1 = new Intent(MainScreen.this, Register.class);
                        startActivityForResult(intent1, MainScreen.REGISTER_SUBSCRIBER);

                        break;
                    case R.id.nav_registered:
                        extra = Constants.SEARCHSOURCE_REGISTEREDSIMS;
                        RegisteredSim cc = new RegisteredSim();
                        args.putInt(ARG_SECTION_NUMBER, 2);
                        cc.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, cc).
                                commit();
                        break;
                    case R.id.nav_settings:
                        intent = new Intent(MainScreen.this, FragmentManagerClass.class);
                        startActivity(intent);
//                        finish();
                        break;
                    case R.id.nav_exit:
                        final ProgressDialog progressDialog = new ProgressDialog(MainScreen.this, R.style.MyTheme);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setCancelable(false);
//                        progressDialog.setTitle(R.string.titl);
                        progressDialog.setCanceledOnTouchOutside(false);
                        progressDialog.getWindow().setGravity(Gravity.CENTER);
                        progressDialog.show();

                        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                            @Override
                            public void run() {
                                // This method will be executed once the timer is over
                                progressDialog.dismiss();
                                System.exit(0);

                            }
                        }, 400);
//                        System.exit(0);
//                        intent = new Intent(MainScreen.this, SplashScreen.class);
//                        startActivity(intent);
//                        finish();
                        break;
                    default:
                        return true;
                }
                return true;
            }
        });

        Fragment fragment = new Home();

        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.SEARCHSOURCE, Constants.SEARCHSOURCE_HOME);
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();


        boolean firstrun = appPreference.check_firstrun();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if (firstrun) {
            //get the settings for this account
            Handler handler = new Handler();
            Runnable run = new Runnable() {
                @Override
                public void run() {
//                    getSettings();
                }
            };
            handler.post(run);
        }
    }

    private void getSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.strpreparingdevice));
        builder.setCancelable(false);
        AlertDialog dialog = builder.show();
        ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(getString(R.string.strpreparingdevice));
        pd.show();

        try {
            JSONObject json = new JSONObject();
            json.put("tag", "account_settings");
            json.put(Constants.IMEI, Constants.getIMEINO(this));
            Log.e("object", "object is " + json.toString());
            UrlCon url = new UrlCon();
            JSONObject jresponse = url.getJsonObject(Constants.SERVER, json);
            if (jresponse != null && jresponse.has("account_settings")) {
                JSONObject jobj = jresponse.getJSONObject("account_settings");
                String track = jobj.getString(Constants.TRACKING_GPS);
                String print = jobj.getString(Constants.PREFRECEIPT);
                String audio = jobj.getString(Constants.RECORD_AUDIO);
                String clearf = jobj.getString(Constants.CLEARFILES);
                String exp = jobj.getString(Constants.EXPORT_DB);
                boolean[] bool = {track.equals("1") ? true : false, print.equals("1") ? true : false,
                        audio.equals("1") ? true : false, clearf.equals("1") ? true : false,
                        exp.equals("1") ? true : false};
                appPreference.saveSettings(bool);
                appPreference.set_firstrun();
                if (pd.isShowing())
                    pd.dismiss();
                if (dialog.isShowing())
                    dialog.dismiss();
            }
            if (pd.isShowing())
                pd.dismiss();
            if (dialog.isShowing())
                dialog.dismiss();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showToast(String txt) {
        Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
    }


    /**
     * Builds a GoogleApiClient. Uses {@code #addApi} to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {


        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

        if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (location.equals("true")) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainScreen.this,
                        new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 1);
//            Log.e("PERMISSION", "No perrmmission for this task");
            }
            setup_location();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        appPreference = new AppPreference(this);
        appPreference.setDefaultLanguage();
        String languageToLoad = appPreference.getDefaultLanguage();
        if (!Validating.areSet(languageToLoad)) {
            languageToLoad = "sw";
        }
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
    }

    private void setup_location() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //nothing proceed
                        LOCATION_ALLOWED = true;
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MainScreen.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        LOCATION_ALLOWED = false;
                        showToast("your device does not support location");
                        break;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        LOCATION_ALLOWED = true;
                        break;
                    case Activity.RESULT_CANCELED:
                        showToast(getString(R.string.strlocation_required));
                        LOCATION_ALLOWED = false;
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    /**
     * Updates fields based on data stored in the bundle.
     */
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.keySet().contains(Constants.LKL)) {
                if (currentlocation == null)
                    currentlocation = savedInstanceState.getParcelable(Constants.LKL);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
        outState.putParcelable(Constants.LKL, currentlocation);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION, 0);
        if (currentlocation == null)
            currentlocation = savedInstanceState.getParcelable(Constants.LKL);
        Menu menu = mNavigationView.getMenu();
        menu.getItem(mCurrentSelectedPosition).setChecked(true);
    }

    private void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    private void setUpNavDrawer() {
        if (mToolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (!tabletSize)
                mToolbar.setNavigationIcon(R.drawable.ic_drawer);
            else
                mToolbar.setNavigationIcon(null);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
        }
        mUserLearnedDrawer = true;

        if (!mUserLearnedDrawer) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            mUserLearnedDrawer = true;
            saveSharedSetting(this, PREF_USER_LEARNED_DRAWER, "true");
        }

    }

    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void periodicadapersync(Account mAccount, int time) {

        ContentResolver.addPeriodicSync(
                mAccount,
                ContentProviderInstance.AUTHORITY,
                Bundle.EMPTY, time);

    }

    public Account createSyncAccount(Context context) {
        Account newaccount = new Account(Constants.ACCOUNT, Constants.ACCOUNT_TYPE);
        AccountManager mAccountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        if (mAccountManager.addAccountExplicitly(newaccount, null, null)) {
            ContentResolver.setSyncAutomatically(newaccount, ContentProviderInstance.AUTHORITY, true);
            log.info("sync account has been created");
        } else {
            log.info("sync account already exist or failed to be created");
        }
        return newaccount;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashbaord2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sync) {
            Constants.ondemandsync(mAccount);
            return true;
        }
        if (id == R.id.action_updates) {
            mRelativeLayout = (RelativeLayout) findViewById(R.id.drawerPane);
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View customView = inflater.inflate(R.layout.content_updates, null);
            mPopupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (Build.VERSION.SDK_INT >= 21) {
                mPopupWindow.setElevation(5.0f);
            }
            Button closeUpdates = (Button) customView.findViewById(R.id.cancel);
            closeUpdates.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPopupWindow.dismiss();
                }
            });
            Button buttonContinue = (Button) customView.findViewById(R.id.contin);
            buttonContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkUpdates();
                    mPopupWindow.dismiss();
                }
            });
            mPopupWindow.showAtLocation(mRelativeLayout, Gravity.CENTER, 0, 0);
        }

        return super.onOptionsItemSelected(item);
    }

    private void checkUpdates() {
        //get destination to update file and set Uri
        //TODO: First I wanted to store my update .apk file on internal storage for my app but
        // apparently android does not allow you to open and install
        //aplication with existing package from there. So for me, alternative so
        // lution is Download directory in external storage. If there is better
        //solution, please inform us in comment
        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
        String fileName = "AppName.apk";
        destination += fileName;
        final Uri uri = Uri.parse("file://" + destination);

        //Delete update file if exists
        File file = new File(destination);
        if (file.exists())
            //file.delete() - test this, I think sometimes it doesnt work
            file.delete();

        //get url of app on server
        String url = MainScreen.this.getString(R.string.update_app_url);

        //set downloadmanager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(MainScreen.this.getString(R.string.notification_description));
        request.setTitle(MainScreen.this.getString(R.string.app_name));

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                Intent install = new Intent(Intent.ACTION_VIEW);
                install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                install.setDataAndType(uri, manager.getMimeTypeForDownloadedFile(downloadId));
                startActivity(install);

                unregisterReceiver(this);
                finish();
            }
        };
        //register receiver for when .apk download is compete
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST) {
//            showToast(getString(R.string.strlocation_denied));
//            ActivityCompat.requestPermissions(MainScreen.this,
//                    new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 1);
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            mPermissionDenied = false;
        } else {
            // Display the missing permission error dialog when the fragments resume.
//            showToast(getString(R.string.strlocation_denied));
            mPermissionDenied = true;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

            currentlocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (appPreference.getSettings(Constants.TRACKING_GPS)) {
                double lat = 0, lng = 0;
                try {
                    if (currentlocation != null) {
                        lat = currentlocation.getLatitude();
                        lng = currentlocation.getLongitude();
                    }
                    String[] v = new String[]{String.valueOf(lat), String.valueOf(lng)};
//                showToast(""+lat+", "+lng);

                    if (Validating.areSet(v))
                        appPreference.saveLocation(v);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
//        else
////            showMissingPermissionError();
//            ActivityCompat.requestPermissions(MainScreen.this,
//                    new String[]{"android.permission.ACCESS_FINE_LOCATION"}, 1);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.i(TAG, "GoogleApiClient connection has failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("connection received " + location.toString());
        currentlocation = location;
        //update location
    }

}