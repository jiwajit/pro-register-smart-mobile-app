/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proregister.db.Customers;


public class RegisteredSim extends Fragment{

	private static final String ARG_SECTION_NUMBER = "section_number";

    RecyclerView recyclerView;
    SubscriberInformationAdapterDisplayer adapter;
    RecyclerView.LayoutManager mLayoutManager;
//    ItemListAdapter adapter;
    ArrayList<ItemList> list;
    AppPreference appPreference;
    TextView txtindex;
    boolean tabletsize;
    FloatingActionButton fab;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        tabletsize = getResources().getBoolean(R.bool.isTablet);

       return inflater.inflate(R.layout.list_items_material, container, false) ;
    } 

	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        recyclerView = (RecyclerView)getActivity().findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new SubscriberInformationAdapterDisplayer(getActivity(),getList(),recyclerView);
        recyclerView.setAdapter(adapter);

        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        appPreference = new AppPreference(getContext());
        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getContext(),Register.class);
                appPreference.clearStates();
                appPreference.clearPhotos(null);
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 2);
                    Toast.makeText(getContext() ,"This process requires phone permissions being enabled" ,Toast.LENGTH_LONG).show();
                } else {
                    startActivityForResult(intent1, MainScreen.REGISTER_SUBSCRIBER);
                }
//                startActivityForResult(intent1,MainScreen.REGISTER_SUBSCRIBER);
            }
        });

        if(tabletsize) {
            /*txtbuz = (TextView) getActivity().findViewById(R.id.txtbusiness);
            txtcli = (TextView) getActivity().findViewById(R.id.txtclient);
            txtadd = (TextView) getActivity().findViewById(R.id.txtadddress);
            txtcont = (TextView) getActivity().findViewById(R.id.txtcontact);
            txtunk = (TextView) getActivity().findViewById(R.id.txtunkown);
            txtbuz.setText(getString(R.string.strbusiness));
            txtcli.setText(getString(R.string.strorderdate));
            txtadd.setText(getString(R.string.stramountpaid));
            txtcont.setText(getString(R.string.strordercost));
            txtunk.setText(getString(R.string.strlastsell));
            txtunk.setVisibility(View.GONE);*/
        }
    }
    public List<ItemList> getList() {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = cr.query(Customers.BASEURI, null, null, null, Customers.ID + " DESC");
        List<ItemList> list = new ArrayList<ItemList>();
        try {
            if (c != null && c.moveToFirst()) {
                do {
                    int colN = c.getColumnIndex(Customers.FNAME);
                    int colP = c.getColumnIndex(Customers.SURNAME);
                    int colI = c.getColumnIndex(Customers.ID);
                    System.out.println("CUSTOMER ID IS: "+colI);
                    int colD = c.getColumnIndex(Customers.CREATED);
                    int colT = c.getColumnIndex(Customers.MOBILE);
                    int colJ = c.getColumnIndex(Customers.REGION);
                    int colS = c.getColumnIndex(Customers.STATUS);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    String d = c.getString(colD);
                    String t = c.getString(colT);
                    String j = c.getString(colJ);
                    String s = c.getString(colS);

                    long i = c.getLong(colI);/*
                    String fnames = getString(R.string.strname).toUpperCase(Locale.getDefault()) +
                            ": " + n.toUpperCase(Locale.getDefault());
                    String snames = getString(R.string.strsname).toUpperCase(Locale.getDefault()) +
                            ": " + p.toUpperCase(Locale.getDefault());
                    String mobile = getString(R.string
                            .strphno).toUpperCase(Locale.getDefault()) + ": " + t;
                    String region = getString(R.string
                            .strregion).toUpperCase(Locale.getDefault()) + ": " + j;
                    String date = getString(R.string.strcreated).toUpperCase(Locale.getDefault()) +
                            ": " + d;
                    String status = getString(R.string.strregstatus).toUpperCase(Locale.getDefault()) +
                            ": " + s;*/
                        list.add(new ItemList(n,p,t,j,d,i,s));

                } while (c.moveToNext());
            } else {
                CoordinatorLayout view = (CoordinatorLayout)getActivity().findViewById(R.id.main_content);
                TextView tx = new TextView(getActivity());
                tx.setText(getString(R.string.strnodata));
                tx.setPadding(20,20,20,20);
                tx.setTextSize(16);
                tx.setGravity(Gravity.CENTER);
                tx.setTextColor(Color.BLUE);
                view.addView(tx,0);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

}
