/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bongolive.apps.proregister;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.splunk.mint.Mint;

import java.util.HashMap;
import java.util.Locale;

import bongolive.apps.proregister.db.Customers;
import bongolive.apps.proregister.db.MultimediaContents;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Validating;
import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindView;

/**
 * Simple Fragment used to display some meaningful content for each page in the sample's
 * {@link android.support.v4.view.ViewPager}.
 */
public class SubscriberContacts extends Fragment {

    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";
    private AppPreference appPreference;

    String[] finalvals = null, custcontact = null;
    String[] finalcheckvals = null;

    private EditText etotherphn, etmail, etprefix;
    private CheckBox chkconsent;
    private ImageView signphoto; // other contacts
    private String setotherphn, setmail, schkconsent, ssignphoto, schkconsent1, schvoice, schdata, schval,
            schfinance; // other contacts
    @BindView
    CheckBox chkconsent1, chkvoice, chkdata, chkvalueadded, chkfinancial;
    private static final int TAKE_SIGNATURE_PHOTO = 5;

    private static Bitmap bitmap_sign = null;
    private static Uri signphoto_uri;
    private static String signphoto_path;
    private static MenuItem menuItem;
    static boolean update;
    String[] loc = null;

    /**
     * @return a new instance of {@link SubscriberContacts}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static SubscriberContacts newInstance(boolean f, MenuItem item) {
        Bundle bundle = new Bundle();
        menuItem = item;
        update = f;
        SubscriberContacts fragment = new SubscriberContacts();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subscribercontactsfrag, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Spork.bind(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        appPreference = new AppPreference(getActivity());
                /* customer other contacts */

        etotherphn = (EditText) getActivity().findViewById(R.id.etotherph);

        etotherphn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkPhoneNumberOther(etotherphn);
            }
        });

        etmail = (EditText) getActivity().findViewById(R.id.etmail);
        chkconsent = (CheckBox) getActivity().findViewById(R.id.chkconsent);

        chkconsent1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    schkconsent1 = "true";
                    schkconsent = "true";
                    chkconsent.setChecked(true);
                    boolean ready = validate();
                    if (ready) {
                        menuItem.setVisible(true);
//                        chkvoice.setEnabled(false);
//                        chkdata.setEnabled(false);
//                        chkvalueadded.setEnabled(false);
//                        chkfinancial.setEnabled(false);
                    } else {
                        schkconsent1 = "false";
                        chkconsent1.setChecked(false);
                    }
                } else {
                    schkconsent1 = "";
                }
            }
        });
        chkdata.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    schdata = chkdata.getText().toString();
                } else {
                    schdata = "";
                }
            }
        });
        chkvoice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    schvoice = chkvoice.getText().toString();
                } else {
                    schvoice = "";
                }
            }
        });
        chkvalueadded.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    schval = chkvalueadded.getText().toString();
                } else {
                    schval = "";
                }
            }
        });
        chkfinancial.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    schfinance = chkfinancial.getText().toString();
                } else {
                    schfinance = "";
                }
            }
        });
        signphoto = (ImageView) getActivity().findViewById(R.id.signphoto);
        signphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ssignphoto != null) {
                    preview_photo(ssignphoto);
                } else {
                    add_sign_photo();
                }
            }
        });

        if (update) {
//            signphoto.setVisibility(View.GONE);
        }

        /* end of other contacta*/

        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                process_info();
                etotherphn.setText("");
                etmail.setText("");
//                signphoto.setImageBitmap(null);
//                AutoCompleteTextView citizenship = (AutoCompleteTextView) getActivity().findViewById(R.id.txtcitizen);
////                Spinner idType = (Spinner)getActivity().findViewById(R.id.spidtype);
//                EditText idNo = (EditText) getActivity().findViewById(R.id.etidno);
//                citizenship.setText("");
//                idNo.setText("");
                return false;
            }
        });
    }

    public void add_sign_photo() {

        Intent intent = new Intent(getActivity(), SignatureDrawer.class);
        intent.putExtra("source", 0);
        startActivityForResult(intent, TAKE_SIGNATURE_PHOTO);
    }

    @Override
    public void onResume() {
        super.onResume();
        String path = appPreference.getSubSignPhoto();
        if (Validating.areSet(path)) {
            signphoto_path = path;
            signphoto_uri = Uri.parse(signphoto_path);
            Bitmap bitmap = Constants.decodeImg(signphoto_path, 250, 150);
            if (bitmap == null) {
                appPreference.clearPhotos(Constants.SUBSIGNPHOTO);
                ssignphoto = null;
            }
            ssignphoto = signphoto_path;
            signphoto.setImageBitmap(bitmap);
        } else {
            System.out.println("image is not set");
        }

        String t, e;
        t = appPreference.getTel();
        etotherphn.setText(t);
        e = appPreference.getEmail();
        etmail.setText(e);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (!update) {
            setotherphn = etotherphn.getText().toString().trim();
            setmail = etmail.getText().toString().trim();

            appPreference.saveTel(setotherphn);
            appPreference.saveEmail(setmail);
        }
    }

    public void checkPhoneNumberOther(EditText e) {
        String s = e.getText().toString().trim();
        if (Validating.areSet(s)) {
            if (s.startsWith("0")) {
                e.setText("");
                e.setError(getString(R.string.phonenotvalid));
                return;
            }
        }
    }

    private boolean validate() {
        boolean ret = false;

        //SUBSCRIBER INFORMATION

        String t, g, fn, sn, mn, r, d, s, a, b, j, p, si, im, simguserphoto;
        //title of customer
        t = appPreference.getTitle();
        //gender of customer
        g = appPreference.getGender();
        //middle name of customer
        mn = appPreference.getMname();
        //first name of customer
        fn = appPreference.getFname();
        //surname of customer
        sn = appPreference.getSname();
        //region of customer
        r = appPreference.getRegion();
        //district of customer
        d = appPreference.getDistrict();
        //street of customer
        s = appPreference.getStreet();
        //address of customer
        a = appPreference.getAddress();
        //birthdate of customer
        b = appPreference.getBdate();
        //job of customer
        j = appPreference.getJob();
        //mobile number to be registered of customer
        p = appPreference.getSmartNo();
        //sim serial of customer
        si = appPreference.getSerial();
        //imei of customer
        im = appPreference.getImei();
        simguserphoto = appPreference.getSubPhoto();
        String[] custinfo = new String[]{t, g, fn, sn, r, d, s, a, b, j, p, si, im, simguserphoto, mn};//14
        // fields
        String[] custinfocheck = new String[]{t, g, fn, sn, r, d, s, b, j, p, simguserphoto};//14 fields

        //SUBSCRIBER IDENTIFICATION

        String c, idno, idty, sidphoto;
        c = appPreference.getCitizenship();
        idno = appPreference.getIdNo();
        idty = appPreference.getIdType();
        sidphoto = appPreference.getSubIdPhoto();
        String[] custid = new String[]{c, idno, idty, sidphoto};

        //OTHER CONTACTS

        String tel, e, ph;
        tel = etotherphn.getText().toString().trim();
        e = etmail.getText().toString().trim();
        ph = ssignphoto;

        if (Validating.areSet(tel)) {
            Constants.checkPhoneNumberOther(etotherphn);
        }

        String[] custcontact = new String[]{tel, e, ph};
        String[] custcontactcheck = new String[]{ph};

        String service = "";
        if (schdata != null && schdata.length() > 0)
            service += schdata;
        if (schvoice != null && schvoice.length() > 0)
            service += "|" + schvoice;
        if (schval != null && schval.length() > 0)
            service += "|" + schval;
        if (schfinance != null && schfinance.length() > 0)
            service += "|" + schfinance;
        Log.e("DATA", "SERVICE IS " + service);
        if (!update) {
            finalcheckvals = new String[]{t, g, fn, sn, r, d, s, b, j, p, simguserphoto, c, idno,
                    idty, sidphoto, ph, schkconsent, schkconsent1, service};


            if (Validating.areSet(finalcheckvals)) {
                String smartNo = appPreference.getSmartNo();
                String phoneother = appPreference.getTel();
                String imei = appPreference.getImei();
                if (smartNo.length() < 9 ) {
                    Toast.makeText(getActivity(), "sahihisha namba ya simu ya mteja", Toast.LENGTH_LONG).show();
//                    etotherphn.setError("sahihisha namba ya simu ya mteja");
                    ret = false;
                } else {
                    ret = true;
                }

                if (!imei.equals("") && imei.length() < 15) {
                    Toast.makeText(getActivity(), "sahihisha IMEI namba zisipungue 15", Toast.LENGTH_LONG).show();
                    ret = false;
                }

            } else {
//                Toast.makeText(getActivity(), "fill all fields please", Toast.LENGTH_LONG).show();

                if (!Validating.areSet(custinfocheck)) {
                    Toast.makeText(getActivity(), getString(R.string.strinfo), Toast.LENGTH_LONG).show();
                }

                if (!Validating.areSet(custid)) {
                    Toast.makeText(getActivity(), getString(R.string.strid), Toast.LENGTH_LONG).show();
                }

                if (!Validating.areSet(custcontactcheck)) {
                    Toast.makeText(getActivity(), getString(R.string.strcontacts), Toast.LENGTH_LONG).show();
                }
                ret = false;
            }
        } else {
            finalcheckvals = new String[]{t, g, fn, sn, r, d, s, b, j, p, c, idno, idty};

            if (Validating.areSet(finalcheckvals)) {
                ret = true;
            } else {
                Toast.makeText(getActivity(), R.string.strfillblanks, Toast.LENGTH_LONG).show();
                ret = false;
            }
        }

        return ret;
    }

    private boolean process_info() {
        boolean ret = false;
        final String voice = appPreference.getAudio();

        final String t, g, fn, sn, r, d, s, a, b, j, p, si, im, simguserphoto, mn;
        t = appPreference.getTitle();
        g = appPreference.getGender();
        fn = appPreference.getFname();
        sn = appPreference.getSname();
        mn = appPreference.getMname();
        r = appPreference.getRegion();
        d = appPreference.getDistrict();
        s = appPreference.getStreet();
        a = appPreference.getAddress();
        b = appPreference.getBdate();
        j = appPreference.getJob();
        p = appPreference.getSmartNo();
        si = appPreference.getSerial();
        im = appPreference.getImei();
        simguserphoto = appPreference.getSubPhoto();
        String[] custinfo = new String[]{t, g, fn, sn, r, d, s, a, b, j, p, si, im, simguserphoto};//14 fields

        final String c, idno, idty, sidphoto;
        c = appPreference.getCitizenship();
        idno = appPreference.getIdNo();
        idty = appPreference.getIdType();
        sidphoto = appPreference.getSubIdPhoto();
        String[] custid = new String[]{c, idno, idty, sidphoto};

        final String tel, e, ph;
        tel = etotherphn.getText().toString().trim();
        e = etmail.getText().toString().trim();
        ph = ssignphoto;

        loc = appPreference.getLastLocation();
        String lat = loc[0];
        String lng = loc[1];
        Log.e("location", " " + lat + " " + lng);
        String service = "";
        if (schdata != null && schdata.length() > 0)
            service += schdata;
        if (schvoice != null && schvoice.length() > 0)
            service += "|" + schvoice;
        if (schval != null && schval.length() > 0)
            service += "|" + schval;
        if (schfinance != null && schfinance.length() > 0)
            service += "|" + schfinance;
        String[] custcontact = new String[]{tel, e, ph, service};

        if (!update) {
            finalcheckvals = new String[]{t, g, fn, sn, r, d, s, b, j, p, simguserphoto, c, idno,
                    idty, sidphoto, ph, service};
            String uniqid = Constants.getLocalUniqueId(getActivity());

            if (Validating.areSet(finalcheckvals)) {
                finalvals = new String[]{t, g, fn, sn, r, d, s, a, b, j, p, si, im, c, idno, idty, tel, e, uniqid,
                        lat, lng, mn, service};
                String phone = appPreference.getSmartNo();
                String idNumber = appPreference.getIdNo();

                //Check if customer is unique in the customer table
//                if (Customers.isCustomerUnique(getActivity(), phone)) {
                    //Insert customer information into the database table
                    boolean insert = Customers.insertCustomer(getActivity(), finalvals);
                    final String txID = Mint.transactionStart("Insert_Customer");
                    HashMap<String, Object> mydata = new HashMap<String, Object>();
                    mydata.put("data", finalvals);
                    Mint.transactionStop(txID, mydata);

                    //Check if the customer information are inserted successfully into the database
                    if (insert) {
                        //Get id of the last customer inserted
                        String cusid = Customers.getId(getActivity());
                        if (Validating.areSet(cusid)) {
                            String[] mult1 = {simguserphoto, fn + Constants.IMGUSER + "_" + System.currentTimeMillis(),
                                    Constants.MULT_IMAGE, Constants
                                    .IMG_TYPE_CUSTOMER, Constants.IMGUSER_MEDIATYPE, Customers.getId(getActivity()), uniqid};
                            String[] mult2 = {sidphoto, fn + Constants.IMGID + "_" + System.currentTimeMillis(), Constants
                                    .MULT_IMAGE, Constants.IMG_TYPE_CUSTOMER,
                                    Constants.IMGID_MEDIATYPE, Customers.getId(getActivity()), uniqid};
                            String[] mult3 = {ph, fn + Constants.IMGSIGN + "_" + System.currentTimeMillis(),
                                    Constants.MULT_IMAGE,
                                    Constants.IMG_TYPE_CUSTOMER, Constants.IMGSIGN_MEDIATYPE, Customers.getId(getActivity()), uniqid};
                            if (Validating.areSet(voice)) {
                                String[] audio = {voice, fn + Constants.MULT_SOUND + "_" + System.currentTimeMillis(),
                                        Constants.MULT_SOUND,
                                        Constants.IMG_TYPE_CUSTOMER, Constants.VOICE_MEDIATYPE, Customers.getId(getActivity()),
                                        uniqid};
                                int store = MultimediaContents.storeMultimedia(getActivity(), audio);
                                if (store == 1) {
                                    System.out.println("voice stored ");
                                    appPreference.clearAudio();
                                }
                            }

                            int storemult = MultimediaContents.storeMultimedia(getActivity(), mult1);
                            int storemult1 = MultimediaContents.storeMultimedia(getActivity(), mult2);
                            int storemult2 = MultimediaContents.storeMultimedia(getActivity(), mult3);
                            if (storemult == 1 && storemult1 == 1 && storemult2 == 1) {
                                Toast.makeText(getActivity(), getString(R.string.stradded), Toast
                                        .LENGTH_SHORT).show();

                                //Perform sync
                                System.out.println("PRINTING SETTING: " + appPreference.getSettings(Constants.PREFRECEIPT));
                                Constants.ondemandsync(MainScreen.mAccount);
                                if (appPreference.getSettings(Constants.PREFRECEIPT)) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(getString(R.string.print))
                                            .setCancelable(false)
                                            .setPositiveButton(getString(R.string.stryes), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //implement printer
                                                    finalvals = new String[]{t, g, fn, mn, sn, r, d, s, a, b, j, p,
                                                            si, im, c, idno, idty, tel, e};
                                                    appPreference.clearPhotos(null);
                                                    appPreference.clearStates();
                                                    appPreference.clearIDINFO();

                                                    Intent intent = new Intent(getActivity(), PrintingCenter.class);
                                                    intent.putExtra("subscriber", finalvals);
                                                    startActivity(intent);
                                                    dialog.cancel();

                                                    Intent intenta = new Intent(getContext() ,MainScreen.class);
                                                    getActivity().setResult(Activity.RESULT_CANCELED, intenta);
                                                    startActivity(intenta);
                                                    getActivity().finish();
                                                }
                                            })
                                            .setNegativeButton(getString(R.string.strno), new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    System.out.println("clicked no print");
                                                    Mint.transactionCancel(txID, "This is the reason");
                                                    appPreference.clearPhotos(null);
                                                    appPreference.clearStates();
                                                    appPreference.clearIDINFO();
                                                    Intent intent = new Intent(getContext() ,MainScreen.class);
                                                    getActivity().setResult(Activity.RESULT_CANCELED, intent);
                                                    startActivity(intent);
                                                    getActivity().finish();
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                } else {
                                    appPreference.clearPhotos(null);
                                    appPreference.clearStates();
                                    Intent intent = new Intent();
                                    getActivity().setResult(Activity.RESULT_CANCELED, intent);
                                    getActivity().finish();
                                }
                            }
                        }
                    }
                    Mint.transactionStop(txID);
//                } else if (!Customers.isCustomerUnique(getActivity(), phone)) {
//                    Toast.makeText(getActivity(), getString(R.string.strnumbernotunique), Toast.LENGTH_SHORT).show();
//                }
            }
        } else {
            finalcheckvals = new String[]{t, g, fn, sn, r, d, s, b, j, p, c, idno, idty};
            if (Validating.areSet(finalcheckvals)) {
                finalvals = new String[]{t, g, fn, sn, r, d, s, a, b, j, p, si, im, c, idno,
                        idty, tel, e, lat, lng, mn};

                if (!Customers.isCustomerUnique(getActivity(), p)) {
                    boolean insert = Customers.update(getActivity(), finalvals);
                    if (insert) {
                        appPreference.clearStates();
                        appPreference.clearPhotos(null);
                        appPreference.clearIDINFO();
                        update = false;
                        getActivity().finish();
                    }
                }
            }
        }

        return ret;
    }

    private void preview_photo(String photo) {
        LayoutInflater infl = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.phototaker, null);

        final Dialog dg = new Dialog(getContext(), R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);

        FrameLayout camera_view = (FrameLayout) dg.findViewById(R.id.camera_view);
        if (photo == null)
            dg.dismiss();

        ImageView imageView = new ImageView(dg.getContext());

        camera_view.addView(imageView);


        Bitmap bitmap = Constants.decodeImg(photo, 400, 400);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
            imageView.setRotation(90);
        }

        ImageButton imgClose = (ImageButton) dg.findViewById(R.id.imgClose);
        ImageButton imgSave = (ImageButton) dg.findViewById(R.id.imgTake);

        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.photo);
        imgSave.setImageBitmap(bitmap1);
        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_sign_photo();
                dg.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dg.dismiss();
            }
        });
        dg.show();
    }
}
