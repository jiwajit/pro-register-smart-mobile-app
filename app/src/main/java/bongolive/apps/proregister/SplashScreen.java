package bongolive.apps.proregister;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

/**
 * This is the activity that displays the welcome screen on application startup
 * **
 */
public class SplashScreen extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 2000;
    private TextView mLogo;
    private SharedPreferences sharedpreferences;
    private static final String mypreference = "proregisterpref";
    private static final String USERNAME = "username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mLogo = (TextView) findViewById(R.id.logo);
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        final ProgressDialog progressDialog = new ProgressDialog(SplashScreen.this, R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.getWindow().setGravity(Gravity.CENTER);
        progressDialog.show();
        animation();
        final Intent login = new Intent(SplashScreen.this, LoginActivity.class);
        final Intent main = new Intent(SplashScreen.this, MainScreen.class);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                String username = getUserName();
                System.out.println("SPLASH SCREEN USERNAME: "+username);
                progressDialog.dismiss();
                if (!getUserName().equals("")) {
                    main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(main);
                    finish();
                } else {
                    login.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(main);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void animation() {
        mLogo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.animator);
        mLogo.startAnimation(anim);
    }

    private String getUserName() {
        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        String username = sharedpreferences.getString(USERNAME, "");
        System.out.println("USERNAME: "+username);
        return username;
    }
}
