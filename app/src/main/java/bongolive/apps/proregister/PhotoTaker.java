/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Log4Helper;

public class PhotoTaker extends Activity {

    private Camera mCamera = null;
    private CameraPreviewInner mCameraView = null;
    private DrawingView drawingView;
    AppPreference appPreference;
    String TAG = PhotoTaker.class.getSimpleName();
    private boolean safeToTakePicture = false;
    int type;
    int source;
    boolean hasAutoFocus;
    ImageButton imgSave;
    boolean tabletSize = false;
    org.apache.log4j.Logger log = Log4Helper.getLogger(TAG);


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabletSize = getResources().getBoolean(R.bool.isTablet);

        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        setContentView(R.layout.phototaker);
        appPreference = new AppPreference(this);
        Intent intent = getIntent();
        source = intent.getExtras().getInt("source");
        type = intent.getExtras().getInt("type");
//        drawingView = (DrawingView)findViewById(R.id.drawing_surface);
        try {
            mCamera = openFrontCamera();
        } catch (Exception e) {
            log.error("Failed to get camera: " + e.getMessage());
        }

        if (mCamera != null) {
            mCameraView = new CameraPreviewInner(this, mCamera);
            FrameLayout camera_view = (FrameLayout) findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);
            drawingView = new DrawingView(this, null);
            mCameraView.setListener();
            mCameraView.setDrawingView(drawingView);
            mCamera.setDisplayOrientation(90);
        }

        System.out.println("SOURCE IS " + source + " type is " + type);

        //btn to close the application
        ImageButton imgClose = (ImageButton) findViewById(R.id.imgClose);
        imgSave = (ImageButton) findViewById(R.id.imgTake);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("log_tag", "Panel Canceled");
                Bundle b = new Bundle();
                Intent intent = new Intent();
                intent.putExtras(b);
                setResult(RESULT_CANCELED, intent);
                finish();
            }

        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCamera.takePicture(null, null, mPicture);
                imgSave.setEnabled(false);
                safeToTakePicture = false;
            }

        });
    }

    private Camera openFrontCamera() {
        int cameraCount = 0;
        Camera cam = null;

        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Log.e("camerafacing", "camera facing " + cameraInfo.facing + " no of cams are " + Camera.getNumberOfCameras());
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            Log.e("type", "type is " + type + " CAMID " + camIdx);
            if (type == 1 && cameraInfo.facing != Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    if (Camera.getNumberOfCameras() > 1)
                        cam = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                    else
                        cam = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                } catch (RuntimeException e) {
                    Log.e(TAG, "Camera failed to open: " + e.getLocalizedMessage());
                }
            } else if (type == 2 && cameraInfo.facing != Camera.CameraInfo.CAMERA_FACING_FRONT) {
                try {
                    cam = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                } catch (RuntimeException e) {
                    Log.e(TAG, "Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        return cam;
    }

    private Camera.AutoFocusCallback mCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (success) {
                camera.takePicture(null, null, mPicture);
                safeToTakePicture = false;
                imgSave.setEnabled(false);
            }
        }
    };
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null) {
                log.error("Error creating media file, check storage permissions: ");
                safeToTakePicture = true;
                imgSave.setEnabled(true);
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                log.debug("File not found: " + e.getMessage());
            } catch (IOException e) {
                log.debug("Error accessing file: " + e.getMessage());
            }
            if (pictureFile != null) {
                String path = pictureFile.getAbsolutePath().toString();
                System.out.println(path);
                if (source == 1)
                    appPreference.saveSubscriberPhoto(path);
                if (source == 2)
                    appPreference.saveSubscriberIdPhoto(path);
                Intent intent = new Intent();
                intent.putExtra("data", path);
                setResult(RESULT_OK, intent);
                finish();
            }
            //finished saving picture
            safeToTakePicture = true;
            imgSave.setEnabled(true);
        }
    };
    public static final int MEDIA_TYPE_IMAGE = 1;

    private static File getOutputMediaFile(int type) {

        File sdcard = Environment.getExternalStorageDirectory();

        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
        if (!folder.exists())
            folder.mkdir();
        String filename = String.valueOf(System.currentTimeMillis());
        File file = new File(folder.getAbsoluteFile(), filename);

        return file;
    }

    private class CameraPreviewInner extends SurfaceView implements SurfaceHolder.Callback {

        private SurfaceHolder mHolder;
        private Camera mCamera;
        private boolean listenerSet = false;
        public Paint paint;
        private DrawingView drawingView;
        private boolean drawingViewSet = false;
        private final String TAG = bongolive.apps.proregister.PhotoTaker.CameraPreviewInner.class.getSimpleName();
        //        private final String TAG = bongolive.apps.proregister.CameraPreview.class.getSimpleName();
        org.apache.log4j.Logger log = Log4Helper.getLogger(TAG);

        public CameraPreviewInner(Context context, Camera camera) {
            super(context);

            mCamera = camera;
            mCamera.setDisplayOrientation(90);
            //get the holder and set this class as the callback, so we can get camera data here
            mHolder = getHolder();
            mHolder.addCallback(this);
            //noinspection deprecation
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_NORMAL);
            Paint paint = new Paint();
            paint.setColor(Color.GREEN);
            paint.setStrokeWidth(3);
            paint.setStyle(Paint.Style.STROKE);
        }

        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            try {
                //when the surface is created, we can set the camera to draw images in this surfaceholder
                mCamera.setPreviewDisplay(surfaceHolder);
//            mCamera.setDisplayOrientation(90);
                mCamera.startPreview();
            } catch (IOException e) {
                log.error("Failed to get camera: " + e.getMessage());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            //before changing the application orientation, you need to stop the preview, rotate and then start it again
            if (mHolder.getSurface() == null)//check if the surface is ready to receive camera data
                return;

            try {
                mCamera.stopPreview();
            } catch (Exception e) {
                log.error("Failed to stop camera to get view: " + e.getMessage());
                //this will happen when you are trying the camera if it's not running
            }

            //now, recreate the camera preview
            try {
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();
                safeToTakePicture = true;
                imgSave.setEnabled(true);
            } catch (IOException e) {
                log.error("Camera error on surfaceChanged " + e.getMessage());
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            //our app has only one screen, so we'll destroy the camera in the surface
            //if you are unsing with more screens, please move this code your activity
            this.getHolder().removeCallback(this);
            mCamera.stopPreview();
            mCamera.release();
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(
                    MeasureSpec.getSize(widthMeasureSpec),
                    MeasureSpec.getSize(heightMeasureSpec));
        }

//    @Override
//    protected void onDraw(Canvas canvas) {
//        canvas.drawBitmap(myBitmap, 0, 0, null);
//        Paint myPaint = new Paint();
//        myPaint.setColor(Color.GREEN);
//        myPaint.setStyle(Paint.Style.STROKE);
//        myPaint.setStrokeWidth(3);
//
//        for (int i = 0; i < numberOfFaceDetected; i++) {
//            Face face = myFace[i];
//            PointF myMidPoint = new PointF();
//            face.getMidPoint(myMidPoint);
//            myEyesDistance = face.eyesDistance();
//            canvas.drawRect((int) (myMidPoint.x - myEyesDistance * 2),
//                    (int) (myMidPoint.y - myEyesDistance * 2),
//                    (int) (myMidPoint.x + myEyesDistance * 2),
//                    (int) (myMidPoint.y + myEyesDistance * 2), myPaint);
//        }
//
//    }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if (!listenerSet) {
                return false;
            }
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                float x = event.getX();
                float y = event.getY();

                Rect touchRect = new Rect(
                        (int) (x - 100),
                        (int) (y - 100),
                        (int) (x + 100),
                        (int) (y + 100));

                final Rect targetFocusRect = new Rect(
                        touchRect.left * 2000 / this.getWidth() - 1000,
                        touchRect.top * 2000 / this.getHeight() - 1000,
                        touchRect.right * 2000 / this.getWidth() - 1000,
                        touchRect.bottom * 2000 / this.getHeight() - 1000);

                doTouchFocus(targetFocusRect);
                if (drawingViewSet) {
                    drawingView.setHaveTouch(true, touchRect);
                    drawingView.invalidate();

                    // Remove the square after some time
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            drawingView.setHaveTouch(false, new Rect(0, 0, 0, 0));
                            drawingView.invalidate();
                        }
                    }, 1000);
                }

            }
            return false;
        }

        public void setListener() {
            listenerSet = true;
        }

        public void setDrawingView(DrawingView dView) {
            drawingView = dView;
            drawingViewSet = true;
        }

        public void doTouchFocus(final Rect tfocusRect) {
            Log.i(TAG, "TouchFocus");
            try {
                final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
                Camera.Area focusArea = new Camera.Area(tfocusRect, 1000);
                focusList.add(focusArea);

                Camera.Parameters para = mCamera.getParameters();
                para.setFocusAreas(focusList);
                para.setMeteringAreas(focusList);
                mCamera.setParameters(para);

                mCamera.autoFocus(myAutoFocusCallback);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Unable to autofocus");
            }

        }

        /**
         * AutoFocus callback
         */
        Camera.AutoFocusCallback myAutoFocusCallback = new Camera.AutoFocusCallback() {

            @Override
            public void onAutoFocus(boolean arg0, Camera arg1) {
                if (arg0) {
                    mCamera.cancelAutoFocus();
                }
            }
        };
    }

}
