package bongolive.apps.proregister;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.ResetPasswordDialogUtils;
import bongolive.apps.proregister.utils.UrlCon;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via Phone/password.
 */
/*bongolive.smart@gmail.com
* bongolive123
* */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    ProgressDialog progressDialog;
    private static final int REQUEST_READ_CONTACTS = 0;

    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mPhoneView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView resetPassword;
    private TextView errorView;
    ResetPasswordDialogUtils dialog;
    //Shared preferences
    private SharedPreferences sharedpreferences;
    private static final String mypreference = "proregisterpref";
    private static final String USERNAME = "username";
    private static final String PHONE = "phone";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Sprunk mint api for logging and alert if app crashed
//        Mint.initAndStartSession(this.getApplication(), "13bf11ed");
        Mint.initAndStartSession(this.getApplication(), "aa127120");
        setContentView(R.layout.activity_login);
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        dialog = new ResetPasswordDialogUtils(this);
        // Set up the login form.
        mPhoneView = (AutoCompleteTextView) findViewById(R.id.phone);
        populateAutoComplete();
        mPasswordView = (EditText) findViewById(R.id.password);
//        resetPassword = (TextView) findViewById(R.id.textReset);
        errorView = (TextView) findViewById(R.id.error);
//        resetPassword.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.showDialog();
//            }
//        });
//        resetPassword.setOnHoverListener(new View.OnHoverListener() {
//            @Override
//            public boolean onHover(View v, MotionEvent event) {
//                resetPassword.setTextColor(Color.RED);
//                return true;
//            }
//        });
        final String phone = mPhoneView.getText().toString();
        final String password = mPasswordView.getText().toString();

//        DUMMY_CREDENTIALS = new String[]{phone + ":" + password};
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mPhoneSignInButton = (Button) findViewById(R.id.sign_in_button);
        mPhoneSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mPhoneView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        String txID = Mint.transactionStart("LoginTransaction");

        //Splunk mint monitoring
        HashMap<String, Object> mydata = new HashMap<String, Object>();
//        mydata.put("agentName", "Bob's B&B");
        mydata.put("agentPhone", mPhoneView.getText().toString());
        Mint.logEvent("Login_button_pressed", MintLogLevel.Info, "USER", mPhoneView.getText().toString());
        Mint.transactionStop(txID, mydata);
//        Mint.transactionStop(txID);
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPhoneView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String phone = mPhoneView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid phone address.
        if (TextUtils.isEmpty(phone)) {
            mPhoneView.setError(getString(R.string.error_field_required));
            focusView = mPhoneView;
            cancel = true;
        } else if (!isPhoneValid(phone)) {
            mPhoneView.setError(getString(R.string.error_invalid_phone));
            focusView = mPhoneView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            if (mPhoneView.getText().toString().equals("255652400670") && mPasswordView.getText().toString().equals("1234")) {
                Intent intent = new Intent(getBaseContext(), MainScreen.class);
                showProgress(true);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(USERNAME, "admin");
                editor.commit();
                startActivity(intent);
                finish();
            } else {
                mAuthTask = new UserLoginTask(phone, password);
                mAuthTask.execute((Void) null);
            }
        }
    }

    private boolean isPhoneValid(String phone) {
        if (phone.startsWith("255")) {
            return NumberUtils.isNumber(phone);
        } else if (phone.length() < 12 || !phone.startsWith("255")) {
            return NumberUtils.isNumber(phone);
        } else {
            return !NumberUtils.isNumber(phone);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mPhoneView.setAdapter(adapter);
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    public void showError() {
        mPasswordView.setError("Invalid login credentials");
        errorView.setTextColor(Color.RED);
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, JSONObject> {

        private final String mPhone;
        private final String mPassword;

        UserLoginTask(String phone, String password) {
            mPhone = phone;
            mPassword = password;
        }

        @Override
        public JSONObject doInBackground(Void... params) {

            UrlCon js = new UrlCon();
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("tag", Constants.TAGLOGIN);
                jsonObject.put(Constants.IMEI, Constants.getIMEINO(getBaseContext()));
                String array;
                JSONArray jarr = new JSONArray();
                JSONObject j = new JSONObject();
                j.put("phone", mPhone);
                j.put("password", mPassword);
                jarr.put(j);
                jsonObject.put("login_user", (Object) jarr);
                System.out.println("SENT DATA: " + jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
                JSONObject job = new JSONObject(array);
                System.out.println("RETURNED JSON DATA: " + job);
                return job;

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject success) {
            mAuthTask = null;
            try {
                if (success.toString().contains("success")) {
                    String username = success.getString("data");
                    //add data to preference for future use
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(USERNAME, username);
                    editor.commit();
                    System.out.println("USERNAME: " + username);
                    Intent intent = new Intent(getBaseContext(), MainScreen.class);
                    showProgress(false);
                    startActivity(intent);
                    finish();
                }
                if (success.toString().contains("failed")) {
                    showError();
                }
                showProgress(false);
            } catch (Exception e) {
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

