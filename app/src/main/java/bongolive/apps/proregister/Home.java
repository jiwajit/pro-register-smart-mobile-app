/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import bongolive.apps.proregister.db.Customers;


public class Home extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";

	ListView lv ;
    ArrayAdapter<String> adapter ;
    GraphView graph;
    AppPreference appPreference;
    FloatingActionButton fab;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
       return inflater.inflate(R.layout.home, container, false) ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);

        appPreference = new AppPreference(getActivity());
        graph = (GraphView) getActivity().findViewById(R.id.graph);

        graph.setTitle(getString(R.string.strtotalsubs) + " " + getString(R.string.strthisweek));
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(Customers.getTotalSubscribersGraph(getActivity()));
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return super.formatLabel(value, isValueX);
                } else {
                    return super.formatLabel(value, isValueX) ;
                }
            }
        });

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(getActivity().getResources().getStringArray(R.array.strweekdays));
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);

        graph.addSeries(series);
        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(getContext(), Register.class);
                appPreference.clearStates();
                appPreference.clearPhotos(null);
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 2);
                    Toast.makeText(getContext() ,"This process requires phone permissions being enabled" ,Toast.LENGTH_LONG).show();
                } else {
                    startActivityForResult(intent1, MainScreen.REGISTER_SUBSCRIBER);
                }
//                startActivityForResult(intent1, MainScreen.REGISTER_SUBSCRIBER);
//                startActivityForResult(intent1, MainScreen.REGISTER_SUBSCRIBER);
                /*Snackbar.make(view, getString(R.string.straddstart), Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();*/
            }
        });

    }
}
