package bongolive.apps.proregister;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Log4Helper;


public class Main_PhotoTaker extends AppCompatActivity implements SurfaceHolder.Callback {

    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    boolean previewing = false;
    LayoutInflater controlInflater = null;
    Button buttonTakePicture;
    AppPreference appPreference;
    String TAG = PhotoTaker.class.getSimpleName();
    TextView prompt;
    DrawingView drawingView;
    int type;
    int source;
    Face[] detectedFaces;
    org.apache.log4j.Logger log = Log4Helper.getLogger(TAG);
    private ScheduledExecutorService myScheduledExecutorService;
    final int RESULT_SAVEIMAGE = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_NO_TITLE); //Removing ActionBar
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_photo_taker);
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.hide();
        appPreference = new AppPreference(this);
        Intent intent = getIntent();
        source = intent.getExtras().getInt("source");
        type = intent.getExtras().getInt("type");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//        camera.setDisplayOrientation(90);

        getWindow().setFormat(PixelFormat.UNKNOWN);
        surfaceView = (SurfaceView) findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        //drawing view on face detected
        drawingView = new DrawingView(this);
        LayoutParams layoutParamsDrawing = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        this.addContentView(drawingView, layoutParamsDrawing);
        /////////////////////
        controlInflater = LayoutInflater.from(getBaseContext());
        View viewControl = controlInflater.inflate(R.layout.control, null);

        LayoutParams layoutParamsControl = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        this.addContentView(viewControl, layoutParamsControl);

        buttonTakePicture = (Button) findViewById(R.id.takepicture);
        buttonTakePicture.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
//                camera.takePicture(myShutterCallback, myPictureCallback_RAW, mPicture);
                camera.takePicture(null, null, mPicture);
//                previewing = false;
            }
        });

        //start autofocus here
        LinearLayout layoutBackground = (LinearLayout) findViewById(R.id.background);
        layoutBackground.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                buttonTakePicture.setEnabled(false);
                camera.autoFocus(myAutoFocusCallback);
            }
        });
//        prompt = (TextView) findViewById(R.id.prompt);
    }

    //Start detecting the number of faces
    FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {
        @Override
        public void onFaceDetection(Face[] faces, final Camera camera) {
            if (faces.length == 0) {
//                prompt.setText(" No Face Detected");
                drawingView.setHaveFace(false);
            } else {
//                prompt.setText(String.valueOf(faces.length) + " Face Detected");
//                prompt.setText("Face Detected");
                drawingView.setHaveFace(true);
                detectedFaces = faces;

    /*
    int maxNumFocusAreas = camera.getParameters().getMaxNumFocusAreas();
    int maxNumMeteringAreas = camera.getParameters().getMaxNumMeteringAreas();
    prompt.setText(String.valueOf(faces.length) + " Face Detected :) "
      + " maxNumFocusAreas=" + maxNumFocusAreas
      + " maxNumMeteringAreas=" + maxNumMeteringAreas
      );
      */
                //Set the FocusAreas using the first detected face
                List<Camera.Area> focusList = new ArrayList<Camera.Area>();
//                for (int i = 0; i <= detectedFaces.length; i ++) {
                Camera.Area firstFace = new Camera.Area(faces[0].rect, 1000);
                focusList.add(firstFace);
//                    break;
//                }

                if (camera.getParameters().getMaxNumFocusAreas() > 0) {
                    Parameters para = camera.getParameters();
                    para.setFocusAreas(focusList);
                    camera.setParameters(para);
                }

                if (camera.getParameters().getMaxNumMeteringAreas() > 0) {
                    Parameters para = camera.getParameters();
                    para.setMeteringAreas(focusList);
                    camera.setParameters(para);
                }

                buttonTakePicture.setEnabled(false);
                camera.getParameters().setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

                //Stop further Face Detection
                camera.stopFaceDetection();

                buttonTakePicture.setEnabled(false);
                camera.autoFocus(myAutoFocusCallback);

                //Delay call autoFocus(myAutoFocusCallback)
                myScheduledExecutorService = Executors.newScheduledThreadPool(1);
                myScheduledExecutorService.schedule(new Runnable() {
                    public void run() {
                        camera.autoFocus(myAutoFocusCallback);
                    }
                }, 1000, TimeUnit.MILLISECONDS);
            }
            drawingView.invalidate();
        }
    };

    ///////////////////////////////////////end faces

    AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback() {

        @Override
        public void onAutoFocus(boolean success, Camera arg1) {
            if (success) {
//                camera.takePicture(null, null, mPicture);
            }
            buttonTakePicture.setEnabled(true);
            camera.cancelAutoFocus();
        }
    };

    ///////////////////////////////////////////////////////////////////////////end auto focus

    ShutterCallback myShutterCallback = new ShutterCallback() {
        @Override
        public void onShutter() {
        }
    };

    PictureCallback myPictureCallback_RAW = new PictureCallback() {
        @Override
        public void onPictureTaken(byte[] arg0, Camera arg1) {
        }
    };


    private PictureCallback mPicture = new PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null) {
                log.error("Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                log.debug("File not found: " + e.getMessage());
            } catch (IOException e) {
                log.debug("Error accessing file: " + e.getMessage());
            }
            if (pictureFile != null) {
                String path = pictureFile.getAbsolutePath();
                System.out.println(path);
                if (source == 1)
                    appPreference.saveSubscriberPhoto(path);
                if (source == 2)
                    appPreference.saveSubscriberIdPhoto(path);
                System.out.println(path);
                Intent intent = new Intent();
                intent.putExtra("data", path);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    };

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (previewing) {
            camera.stopFaceDetection();
            camera.stopPreview();
            previewing = false;
        }

        if (camera != null) {
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
//                prompt.setText(String.valueOf("Max Face: " + camera.getParameters().getMaxNumDetectedFaces()));
                camera.startFaceDetection();
                previewing = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static final int MEDIA_TYPE_IMAGE = 1;

    private static File getOutputMediaFile(int type) {

        File sdcard = Environment.getExternalStorageDirectory();

        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
        if (!folder.exists())
            folder.mkdir();
        String filename = String.valueOf(System.currentTimeMillis());
        File file = new File(folder.getAbsoluteFile(), filename);

        return file;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        camera = Camera.open();
        //Set face detection on camera
        camera.setFaceDetectionListener(faceDetectionListener);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
//        if (detectedFaces.length>=0){
//        camera.stopFaceDetection();
//        }
        camera.stopPreview();
        camera.release();
        camera = null;
        previewing = false;
    }


    /*
     *
     * Inner class responsible to draw a rectangle when face detected
     *
     * */

    private class DrawingView extends View {

        boolean haveFace;
        Paint drawingPaint;

        public DrawingView(Context context) {
            super(context);
            haveFace = false;
            drawingPaint = new Paint();
            drawingPaint.setColor(Color.GREEN);
            drawingPaint.setStyle(Paint.Style.STROKE);
            drawingPaint.setStrokeWidth(2);
        }

        public void setHaveFace(boolean h) {
            haveFace = h;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            // TODO Auto-generated method stub
            if (haveFace) {
                // Camera driver coordinates range from (-1000, -1000) to (1000, 1000).
                // UI coordinates range from (0, 0) to (width, height).
                int vWidth = getWidth();
                int vHeight = getHeight();
                for (int i = 0; i < detectedFaces.length; i++) {
                    if (i == 0) {
                        drawingPaint.setColor(Color.TRANSPARENT);
                    } else {
                        drawingPaint.setColor(Color.TRANSPARENT);
                    }
                    int l = detectedFaces[i].rect.left;
                    int t = detectedFaces[i].rect.top;
                    int r = detectedFaces[i].rect.right;
                    int b = detectedFaces[i].rect.bottom;
                    int left = (l + 1000) * vWidth / 2000;
                    int top = (t + 1000) * vHeight / 2000;
                    int right = (r + 1000) * vWidth / 2000;
                    int bottom = (b + 1000) * vHeight / 2000;
                    canvas.drawRect(left, top, right, bottom, drawingPaint);
                }
            } else {
                canvas.drawColor(Color.TRANSPARENT);
            }
        }
    }
}
