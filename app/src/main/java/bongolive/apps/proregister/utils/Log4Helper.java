/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 *
 */
package bongolive.apps.proregister.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import org.apache.log4j.Logger;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * @author nasznjoka
 *2014
 * 12:49:39 AM
 */
public class Log4Helper {
	private final static LogConfigurator mLogConfigrator = new LogConfigurator();

	public static void configure( String fileName, String filePattern, int maxBackupSize, long
			maxFileSize ) {
		mLogConfigrator.setFileName( fileName );
		mLogConfigrator.setMaxFileSize( maxFileSize );
		mLogConfigrator.setFilePattern(filePattern);
		mLogConfigrator.setMaxBackupSize(maxBackupSize);
		mLogConfigrator.setUseLogCatAppender(true);
//		mLogConfigrator.configure();

	}
	public static Logger getLogger(String name) {
		Logger logger = Logger.getLogger(name);
		return logger;
	}
}