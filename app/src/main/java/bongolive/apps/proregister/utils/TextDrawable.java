/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 * 
 */
package bongolive.apps.proregister.utils;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

/**
 * @author nasznjoka
 *2014
 * 12:49:39 AM
 */
public class TextDrawable extends Drawable {

	private final String text;
	private final Paint paint;

	public TextDrawable(String text) {
		this.text = text;
		this.paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTextSize(30f);
		paint.setAntiAlias(true);
		paint.setTextAlign(Paint.Align.LEFT);
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawText(text, 0, 6, paint);
	}

	@Override
	public void setAlpha(int alpha) {
		paint.setAlpha(alpha);
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
		paint.setColorFilter(cf);
	}

	@Override
	public int getOpacity() {
		return PixelFormat.TRANSLUCENT;
	}
}