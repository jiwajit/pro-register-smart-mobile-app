/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister.utils;

import android.accounts.Account;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import bongolive.apps.proregister.R;
import bongolive.apps.proregister.db.ContentProviderInstance;


public class Constants {

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE = "pro.co.tz.operator";
    public static final String DEVICE_ACCOUNTTYPE = "acount_type";
    public static final String ACCOUNT = "SimRegistration";
//        public static final String SERVER = "http://www.pro.co.tz/proregister/api/service";
//    public static final String SERVER = "http://41.223.17.71/api/service";
//    public static final String SERVER = "http://41.223.17.71/api/service";
//    public static final String SERVER = "http://ekyc.smart.co.tz/api/service";
    public static final String SERVER = "http://192.168.1.128/proregisternestle/api/service";
//    public static final String SERVER = "http://192.168.2.40/proregisternestle/api/service";
    //    public static final String SERVER = "http://172.25.200.174/proregister/api/service";
//        public static final String SERVER = "http://192.168.2.14/proregister/api/service";
//        public static final String SERVER = "http://amotel.pro.co.tz/api/service";
    public static final String SERVERAUTHENTICITY = "http://www.pro.co.tz/operator/index" +
            ".php/en/api/web_service/authenticate";
    //    public static final String SERVERAUTHENTICITY = "http://192.168.2.21/operator/index" +
//            ".php/en/api/web_service/authenticate";
    public static final String AUTHTOKEN = "auth_key";
    public static final String AUTHTOKENSERVER = "authentication_key";
    public static final String DEVICEACCOUNTTYPESERVER = "account";
    public static final String FIRSTRUN = "first_run";
    public static final String TAGREGISTRATION = "register_device";
    public static final String AUTHENTICATETAG = "authenticate";
    public static final String ACCOUNT_TYPE_NORMAL = "normal";
    public static final String ACCOUNT_TYPE_PREMIUM = "premium";
    public static final String LASTSYNC = "last_sync";
    public static final String FRESHSYNC = "current_sync";
    public static final String TAGSPRODUCT = "assign_products";
    public static final String SEARCHSOURCE = "search_initilizer";
    public static final String SEARCHSOURCE_REGISTEREDSIMS = "registeredsims_fragment";
    public static final String SEARCHSOURCE_CUSTOMERS = "customers_fragment";
    public static final String SEARCHSOURCE_REGISTERSIM = "register_fragment";
    public static final String SEARCHSOURCE_MAIN = "dashboard";
    public static final String SEARCHSOURCE_HOME = "home_fragment";
    public static final String SALESBYCUSTOMER = "sales_customer";
    public static final String SALESBYPRODUCT = "sales_product";
    public static final String TAXBYPRODUCT = "tax_report";
    public static final String PRODUCTREORDER = "reorder_level";
    public static final String REPORTS = "reports";
    public static final String STOCKLEVEL = "stock_movement";
    public static final String BUSINESSNAME = "account_company_name";
    public static final String CONTACTP = "account_company_contactp";
    public static final String PHONE = "account_company_phone";
    public static final String KEY_DBPATH = "database_path";
    public static final String KEY_DB = "database_name";
    public static final CharSequence PREF_EXPORT_KEY = "pref_export_key";
    public static final CharSequence PREF_IMPORT_KEY = "pref_import_extenal_key";
    public static final String PREFPRODUCTSKU = "product_sku";
    public static final String CUSTOMERINFO = "customer_info";
    public static final String CUSTOMERID = "customer_id";
    public static final String CUSTOMERCONTACTS = "customer_contacts";
    public static final String SAVESTATE = "saved_state";
    public static final String VOICENOTE = "voice_note";
    public static final String MASTER_DIR = "ProRegister";
    public static final String LKL = "last_location";
    public static final String MASTERDIR = "proregister";
    public static final String RECORD_AUDIO = "allow_audio_recording";
    public static final String TRACKING_GPS = "allow_gps_recording";
    public static final int TAKE_PHOTO = 120;
    public static String CLEARFILES = "allow_clearfiles";
    public static String EXPORT_DB = "export_db";
    public static String TAGORDERS = "upload_orders";
    public static String TAGSITEM = "upload_orders_items";
    public static String TAGSENDCUSTOMER = "upload_subscribers";
    public static String TAGLOGIN = "user_login";
    public static String TAGRESETPASSWORD = "user_reset";
    public static String TAGRESETCODE = "user_";
    public static String TAGSENDPRODUCT = "upload_product";
    public static String TAGSENDSTOCK = "upload_stock";
    public static String TAGGETCUSTOMER = "assign_customers";
    public static String TAGVALIDATECUSTMER = "status_data";
    public static String TAGSETTINGS = "assign_settings";
    public static String TAGLOCATIONUPDATES = "device_tracking";
    public static String TAGMULTIMEDIA = "multi_media_data";
    public static String DEFAULTTRACKINTERVAL = "5400000";
    public static String SUCCESS = "success";
    public static String FAILURE = "failure";
    public static String MULT_IMAGE = "image";
    public static String MULT_SOUND = "sound";
    public static String MULT_VIDEO = "video";
    public static String ACKVALUE = "1";
    public static String ACK = "ack";
    public static String TAGACK = "acktag";
    public static String IMEI = "imei";
    public static String ACKREFERENCE = "ref";
    public static String ACKKEY = "key";
    public static String PREFCURRENCY = "default_currency_key";
    public static String PREFSYNC = "sync_frequency";
    public static String PREFLANG = "default_language";
    public static String PREFDISCOUNT = "enable_discount_key";
    public static String PREFTAX = "include_vat_key";
    public static String PREFRECEIPT = "print_receipt";
    public static String SENDLOGS = "send_logreport";
    public static String PREFFIXEDPRICE = "fixed_price_key";
    public static String PREFLIMITPRICE = "limit_discount_key";
    public static String PREFADDRESS = "address";
    public static String PREFSUBINFOSTATE = "info_state";
    public static String PREFSUBIDSTATE = "id_state";
    public static String PREFSUBSIGNSTATE = "sign_state";
    public static String PREFALLOWPRINTWITHOUTSTANDINGPAYMENT = "print_recpt_with_nopayment";
    public static String RCP = "RCP";
    public static String PAYMENTS = "PAYMENTS";
    public static String TRN = "TRN";
    public static String PMT = "PMT";//PAYMENTS
    public static String TAX_GROUP = "TAX_GROUP";
    public static String NAME = "NAME";
    public static String SPRICE = "SPRICE";
    public static String QTY = "QTY";
    public static String MEAS_UNIT = "MEAS_UNIT";
    public static String IMGUSER = "_cut_photo";
    public static String IMGUSER_MEDIATYPE = "0";
    public static String IMGID = "_id_photo";
    public static String IMGID_MEDIATYPE = "1";
    public static String IMGSIGN = "_sign_photo";
    public static String IMGSIGN_MEDIATYPE = "2";
    public static String VOICE_MEDIATYPE = "3";
    public static String PAY_AMOUNT = "PAY_AMOUNT";
    public static String PDISCOUNT = "PDISCOUNT";
    public static String NDISCOUNT = "NDISCOUNT";
    public static String PMARKUP = "PMARKUP";
    public static String CLIENT_DATA = "CLIENT_DATA";

    public static final int CONTEXTMENU_EDIT = 1;
    public static final int CONTEXTMENU_DELETE = 2;

    public static String IMG_TYPE_CUSTOMER = "0";
    public static String IMG_TYPE_AGENT = "1";
    public static String CLIENTID = "ID";
    public static String SUBPHOTO = "subphoto";
    public static String SUBIDPHOTO = "idphoto";
    public static String SUBSIGNPHOTO = "signphoto";


    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * Authtoken type string.
     */
    public static final String AUTHTOKEN_TYPE = "com.example.android.samplesync";
    private static String TAG = "prosales_account";

    public static final long SYNC_PER_USER(Context context) {
        long sync = 0;
        try {
            sync = Integer.parseInt(Constants.getIMEINO(context).substring(0, 8)) / 2;
            return sync;
        } catch (NumberFormatException e) {
            e.toString();
        }
        return 0;
    }
    /*
    public static Account createSyncAccount(Context context)
	{ 
		Account newaccount = new Account(ACCOUNT, ACCOUNT_TYPE) ;
		AccountManager mAccountManager = (AccountManager)context.getSystemService(Context.ACCOUNT_SERVICE) ;
		 
		if(mAccountManager.addAccountExplicitly(newaccount, null, null)) 
		{ 
			ContentResolver.setSyncAutomatically(newaccount, ContentProviderInstance.AUTHORITY, true);
			Log.v(TAG, "PROSALES ACCOUNT IS GOOD TO GO") ;
		} else {
			Log.v(TAG, "PROSALES MOBILE  ACCOUNT FAILED TO BE SET UP") ;
		}
		return newaccount;
		
	}*/

    public static String getIMEINO(Context ctx) {
        TelephonyManager tManager = (TelephonyManager) ctx
                .getSystemService(Context.TELEPHONY_SERVICE);
        String imeiid = tManager.getDeviceId();
        return imeiid;
    }

    public static boolean isNotConnected(Context ctx) {
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static String getDate() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "yyyy-MM-dd HH:mm"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String getDateOnly() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "MM-dd HH:mm"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String getDateOnly2() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String getYearDateMonth() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "yyyy-MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }


    public static String getDateRange() {

        Calendar c = Calendar.getInstance();   // this takes current date
        c.set(Calendar.DAY_OF_MONTH, 1);
        System.out.println(c.getTime());

        Date update = c.getTime();
        String returnstring = null;
        try {

            String[] formats = new String[]{
                    "yyyy-MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static String compareDateWeek() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(update);
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            Date newDate = calendar.getTime();

            String[] formats = new String[]{
                    "yyyy-MM-dd HH:mm"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(newDate);
            }
        } finally {

        }
        return returnstring;
    }

    public static String compareDateMonth() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(update);
            calendar.add(Calendar.DAY_OF_MONTH, -30);
            Date newDate = calendar.getTime();

            String[] formats = new String[]{
                    "yyyy-MM"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(newDate);
            }
        } finally {

        }
        return returnstring;
    }


    public static void LogException(Exception ex) {
        Log.d(TAG + " Exception",
                TAG + "Exception -- > " + ex.getMessage() + "\n");
        ex.printStackTrace();
    }

    public static String getPriceAfterBeforeTaxs(double price, double tax, int taxyes) {
        double finalprice = 0;
        switch (taxyes) {
            case 1:
                finalprice = price + price * (tax / 100);

                return String.valueOf(finalprice);
            case 0:
                return String.valueOf(price);
        }
        return null;
    }

    public static double calculate_grandtotal(double price, int qty) {

        BigDecimal outpt = new BigDecimal(price).multiply(new BigDecimal(qty));//price*qty

        return outpt.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static boolean validateCsv(String myString) {
        if (myString.length() > 3) {
            String format = myString.substring(myString.length() - 3);
            if (format.equals("csv")) {
                return true;
            }
        }
        return false;
    }

    public static double calculateTotalNoTax(double totals, double taxrate) {

        BigDecimal output = new BigDecimal(0);
        BigDecimal taxamount = new BigDecimal(calculateTax(totals, taxrate));
        if (taxrate == 0) {
            output = new BigDecimal(totals);
        } else if (taxrate > 0) {
            output = new BigDecimal(totals).subtract(taxamount);
        }
        return output.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double calculateTax(double price, double taxrate) {
        //this will return tax only
        BigDecimal rate = new BigDecimal(taxrate).divide(new BigDecimal(100));//price*qty
        BigDecimal output = new BigDecimal(price).multiply(rate);

        return output.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double totalsnotax(double price, double taxamount) {//calculate the total cost with no tax
        BigDecimal output = new BigDecimal(price).subtract(new BigDecimal(taxamount));
        return output.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }


    public static String getbase64photo(String imagpath) {
        String mCurrentPhotoPath = null;
//        Bitmap bm = BitmapFactory.decodeFile(imagpath);
        Bitmap bm = decodeFile(imagpath);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        mCurrentPhotoPath = Base64.encodeToString(ba, Base64.DEFAULT);
//        Log.v("fileuri",mCurrentPhotoPath);
//        Log.v("fileuri",imagpath);
        return mCurrentPhotoPath;
    }

    public static Bitmap decodeFile(String filePath) {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);

        return bitmap;
    }


    public static String getBase64Vid(final String url) {
        try {
            File file = new File(url);
            byte[] bFile = new byte[(int) file.length()];
            Log.v("filelength", " " + file.length());
            FileInputStream inputStream = new FileInputStream(url);
            inputStream.read(bFile);
            inputStream.close();
//        Log.v("filecontent", Base64.encodeToString(bFile, Base64.NO_PADDING));
//        return Base64.encodeb(bFile, Base64.DEFAULT);

            return bongolive.apps.proregister.utils.Base64.encodeBytes(bFile);
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }

    public static String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = input.getBytes();
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
        }

        return hexStr;
    }

    public static String generateBatch() {
        String batch = null;

        long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
        batch = String.valueOf(number);
        return batch;
    }

    public static void setDateTimeField(Context context, final EditText fromDateEtxt) {

        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
    }

    public static void setDateTime(Context context, final TextView fromDateEtxt) {

        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
    }


    public static String getVersionName(Context context) {
        try {
            String versionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
            return versionName;
        } catch (PackageManager.NameNotFoundException ne) {
            ne.printStackTrace();
        }
        return "";
    }

    public static String getReportName(String report) {
        File sdcard = Environment.getExternalStorageDirectory();

        File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");

        if (!folder.exists()) {
            folder.mkdirs();
        }


        File file = new File(folder.getAbsoluteFile(), report + " on " + getDate() + ".csv");
        System.out.println("filename " + file);
        return file.toString();
    }

    public static boolean isSdMounted() {
        String state = Environment.getExternalStorageState();

        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;

        }
        return false;
    }


    public static String getLocalUniqueId(Context context) {
        try {
            String id = UUID.randomUUID().toString();
            id = id + "." + getIMEINO(context).concat(".").concat(getDateOnly());
            return id;
        } catch (NullPointerException e) {
            e.printStackTrace();
            return UUID.randomUUID().toString();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();
        /// if no network is available networkInfo will be null
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeImg(String path, int reqWidth, int reqHeight) {
        File file = new File(path);
        if (file.exists()) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(path, options);
        } else
            return null;
    }

    public static boolean isValidPhoneNo(String phone) {
        boolean ret = false;
        if (Validating.areSet(phone)) {
            if (phone.length() == 10) {
                if (phone.substring(0, 1).matches("[0]"))
                    ret = true;
            }
        }
        return ret;
    }

    public static void createFile(String str) {
        File sdcard = Environment.getExternalStorageDirectory();

        File folder = new File(sdcard.getAbsoluteFile(), MASTER_DIR);
        if (!folder.exists())
            folder.mkdir();
        String filename = String.valueOf(System.currentTimeMillis());
        filename = filename + ".txt";
        try {
            File file = new File(folder.getAbsoluteFile(), filename);
            FileWriter fl = new FileWriter(file);
            fl.append(str);
            fl.flush();
            fl.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkPhoneNumber(Context context, EditText e) {
        String s = e.getText().toString().trim();
        if (Validating.areSet(s)) {
            if (s.length() > 0) {
                if (!s.startsWith("7")) {
                    e.setText("");
                    e.setError(""+R.string.phonevalidation);
                    return;
                } else if (s.length() > 1) {
                    if (!s.startsWith("79")) {
                        e.setText("");
                        e.setError(""+R.string.phonevalidation);
                        return;
                    } else if (s.length() > 2) {
                        if (!s.startsWith("79")) {
                            e.setText("");
                            e.setError(""+R.string.phonevalidation);
                        }
                    }
                }
            }
        }
    }

    public static void checkPhoneNumberOther(EditText e) {
        String s = e.getText().toString().trim();
        if (Validating.areSet(s)) {
            if (s.startsWith("0")) {
                e.setText("");
                e.setError(""+R.string.phonenotvalid);
                return;
            }
        }
    }

    public static void sendEmail(Context context, File db) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getString(R.string.support_email)});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "ISSUE");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Explain a bit");
        File sdcard = Environment.getExternalStorageDirectory();
        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
        String fileName = folder + "/" + "smart.log";
        File file = new File(fileName);
        if (!file.exists() || !file.canRead()) {
            return;
        }
        Uri uri = Uri.fromFile(file);
        Uri uri1 = Uri.fromFile(db);
        ArrayList<Uri> uris = new ArrayList<Uri>();
        uris.add(uri);
        uris.add(uri1);

        emailIntent.putExtra(Intent.EXTRA_STREAM, uris);
        context.startActivity(Intent.createChooser(emailIntent, "Pick an Email provider"));
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    public static void ondemandsync(Account mAccount) {
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, ContentProviderInstance.AUTHORITY, settingsBundle);
    }

}