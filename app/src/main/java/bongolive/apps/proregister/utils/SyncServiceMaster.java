package bongolive.apps.proregister.utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 *
 */
public class SyncServiceMaster extends Service
{
	private static SyncAdapterMaster syncadapter  = null;
	private static final Object osyncadapterlock = new Object();
	
	@Override
	public void onCreate()
	{
		synchronized (osyncadapterlock) {
		 if(syncadapter == null)
		 {
			 syncadapter = new SyncAdapterMaster(getApplicationContext(), true);
		 }
		}
	}
	
	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 * Return an object that allows the system to invoke
     * the sync adapter.
	 */
	@Override
	public IBinder onBind(Intent intent) { 
		
		return syncadapter.getSyncAdapterBinder() ;
	}

}
