/**
 *
 */
package bongolive.apps.proregister.utils;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import bongolive.apps.proregister.AppPreference;
import bongolive.apps.proregister.R;
import bongolive.apps.proregister.db.Agent;
import bongolive.apps.proregister.db.Customers;
import bongolive.apps.proregister.db.DatabaseHandler;
import bongolive.apps.proregister.db.MultimediaContents;
import bongolive.apps.proregister.db.Tracking;

/**
 * This sync adapter is the one responsible to handle the sending of data to the server
 */

/**
 * @param
 * @author nasznjoka
 *         <p/>
 *         Oct 8, 2014
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SyncAdapterMaster extends AbstractThreadedSyncAdapter {

    private static final String TAG = "SyncAdapter";
    AppPreference appPreference;
    ContentResolver mContentResolver;

    /**
     * @param context
     * @param autoInitialize
     */
    public SyncAdapterMaster(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }

    //android 3.0 and above compatibility
    public SyncAdapterMaster(Context context, boolean autoInitialize, boolean allowParallelSync) {
        super(context, autoInitialize, allowParallelSync);
        mContentResolver = context.getContentResolver();
    }

    /* (non-Javadoc)
     * @see android.content.AbstractThreadedSyncAdapter#onPerformSync(android.accounts.Account, android.os.Bundle, java.lang.String, android.content.ContentProviderClient, android.content.SyncResult)
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        appPreference = new AppPreference(getContext());
        /*
         * This is where all the methods for uploading and download are done
		 */
        Log.d(TAG, "*********** start of *********   onPerformSync for account[" + account.name + "]");


        try {
            Log.d(TAG, "Customers processing");
            Customers.sendCustomersToServer(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.d(TAG, "Media processing");
            MultimediaContents.processInformation(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Log.d(TAG, "Status processing");
            Customers.processValidation(getContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.d(TAG, "wipe data processing");
            try {
                JSONObject job = new JSONObject();
                job.put("tag", "is_goat");
                job.put(Constants.IMEI, Constants.getIMEINO(getContext()));
                UrlCon urlCon = new UrlCon();
                JSONObject response = urlCon.getJsonObject(Constants.SERVER, job);
                if (response != null && response.has(Constants.SUCCESS)) {
                    if (response.has("is_goat")) {
                        DatabaseHandler.getInstance(getContext()).getWritableDatabase().delete
                                (Customers.TABLENAME, null, null);
                        DatabaseHandler.getInstance(getContext()).getWritableDatabase().delete
                                (Agent.TABLENAME, null, null);
                        DatabaseHandler.getInstance(getContext()).getWritableDatabase().delete
                                (MultimediaContents.TABLENAME, null, null);
                        DatabaseHandler.getInstance(getContext()).getWritableDatabase().delete
                                (Tracking.TABLENAME, null, null);
                        File sdcard = Environment.getExternalStorageDirectory();
                        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                        if (folder.exists()) {
                            boolean del = Constants.deleteDir(folder);
                            Log.e("DIRDEL", "?" + del);
                        }

                        if (Customers.getCustomerCount(getContext()) == 0) {
                            sdcard = Environment.getExternalStorageDirectory();
                            folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                            if (!folder.exists()) {
                                JSONObject obj = new JSONObject();
                                obj.put("tag", "gotify");
                                obj.put(Constants.IMEI, Constants.getIMEINO(getContext()));
                                urlCon = new UrlCon();
                                urlCon.getJsonObject(Constants.SERVER, obj);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.d(TAG, "Status processing");
            try {
                JSONObject json = new JSONObject();
                json.put("tag", "account_settings");
                json.put(Constants.IMEI, Constants.getIMEINO(getContext()));
                Log.e("object", "object is " + json.toString());
                UrlCon url = new UrlCon();
                JSONObject jresponse = url.getJsonObject(Constants.SERVER, json);
                if (jresponse != null && jresponse.has("account_settings")) {
                    JSONObject jobj = jresponse.getJSONObject("account_settings");
                    String track = jobj.getString(Constants.TRACKING_GPS);
                    String print = jobj.getString(Constants.PREFRECEIPT);
                    String audio = jobj.getString(Constants.RECORD_AUDIO);
                    String clearf = jobj.getString(Constants.CLEARFILES);
                    String exp = jobj.getString(Constants.EXPORT_DB);
                    boolean[] bool = {track.equals("1") ? true : false, print.equals("1") ? true : false,
                            audio.equals("1") ? true : false, clearf.equals("1") ? true : false,
                            exp.equals("1") ? true : false};
                    appPreference.saveSettings(bool);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        long time = 0;
        time = Long.parseLong(!appPreference.getDefaultSync().isEmpty() ? appPreference.getDefaultSync() : getContext()
                .getResources().getStringArray(R.array.pref_sync_frequency_values)[0]);
        System.out.println("next sync is after " + time);
//        syncResult.delayUntil = time;
        Log.d(TAG, "*********** end of *********   onPerformSync for account[" + account.name + "]");
    }

}
