/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 *
 */
package bongolive.apps.proregister.utils;

import android.util.Log;

/**
 * @author nasznjoka
 *         2014
 *         12:49:39 AM
 */
public class Validating {

    public static boolean areSet(String... strings) {
        for (String s : strings)
            if (s == null || s.isEmpty() || s.length() < 0 || s.equals(""))
                return false;


        return true;
    }


    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    public boolean isCardValid(CharSequence card) {

        return android.util.Patterns.EMAIL_ADDRESS.matcher(card).matches();

    }

    public static String stripit(String string) {
        String input;
        input = string.replace(" ", "");
        input = input.trim();
        return input;

    }

}
