package bongolive.apps.proregister.utils;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import java.io.File;
import java.io.IOException;

import bongolive.apps.proregister.AppPreference;

public class AudioService extends Service {
    private static final String TAG = AudioService.class.getName();

    int channelRate = 44100;
    private MediaRecorder mediaRecorder = null;
    private boolean isRecording = false;
    private static String path = null;
    private AppPreference appPreference;
    boolean started = false;
    public AudioService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
//        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate() {
        super.onCreate();
        startRecording();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        stopRecording();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStart");

        return super.onStartCommand(intent, flags, startId);
    }

    private void startRecording() {
        if(mediaRecorder != null)
            mediaRecorder.release();

        mediaRecorder = new MediaRecorder();
//        Toast.makeText(this, "start recording sound",Toast.LENGTH_LONG).show();
        try {
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setAudioEncodingBitRate(16);
            mediaRecorder.setAudioSamplingRate(channelRate);
            mediaRecorder.setMaxDuration(90000);

            File sdcard = Environment.getExternalStorageDirectory() ;
            File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTERDIR);
            folder.mkdir();
            String filename = String.valueOf(System.currentTimeMillis());
            File file = new File(folder.getAbsoluteFile(), filename ) ;
            mediaRecorder.setOutputFile(file.toString());
            mediaRecorder.prepare();
            mediaRecorder.start();
            path = file.toString();
            started = true;
        } catch (IllegalStateException e) {
            Log.e(TAG, "prepare() failed "+e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void stopRecording() {
        appPreference = new AppPreference(this);
        if(mediaRecorder != null) {
            if(Validating.areSet(path)) {
                File file = new File(path);
                if(file.exists())
                    appPreference.saveAudio(path);
            }
            if(started)
                mediaRecorder.stop();
//            Toast.makeText(this, "stopping recording sound",Toast.LENGTH_LONG).show();
            mediaRecorder.release();
            mediaRecorder = null;
        }
    }
}
