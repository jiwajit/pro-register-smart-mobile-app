package bongolive.apps.proregister.utils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proregister.InputResetCode;
import bongolive.apps.proregister.LoginActivity;
import bongolive.apps.proregister.R;

public class ResetPasswordDialogUtils {

    private RadioGroup radioResetPassGroup;
    private UserLoginTask mAuthTask = null;
    private LoginActivity mDialogUniversalWarningActivity;
    private Dialog mDialog;
    private TextView mDialogOKButton;
    private TextView mDialogCancelButton;
    EditText email, phone;
    private RadioButton byphone, byemail, resetMethod;

    public ResetPasswordDialogUtils(
            LoginActivity mDialogUniversalWarningActivity) {
        this.mDialogUniversalWarningActivity = mDialogUniversalWarningActivity;
    }

    public void showDialog() {
        if (mDialog == null) {
            mDialog = new Dialog(mDialogUniversalWarningActivity);
        }

        mDialog.setContentView(R.layout.layoutresetpassworddialogutils);
        mDialog.setCancelable(false);
        mDialog.show();
        mDialogOKButton = (TextView) mDialog.findViewById(R.id.dialog_universal_warning_ok);
        mDialogCancelButton = (TextView) mDialog.findViewById(R.id.dialog_universal_warning_cancel);
        initDialogButtons();
    }

    ProgressDialog progressDialog;

    private void initDialogButtons() {

        email = (EditText) mDialog.findViewById(R.id.email);
        phone = (EditText) mDialog.findViewById(R.id.phone);
        byphone = (RadioButton) mDialog.findViewById(R.id.radioPhone);
        byemail = (RadioButton) mDialog.findViewById(R.id.radioEmail);

        radioResetPassGroup = (RadioGroup) mDialog.findViewById(R.id.radiogroup);
        int selectedId = radioResetPassGroup.getCheckedRadioButtonId();
        resetMethod = (RadioButton) mDialog.findViewById(selectedId);
        final String methodChosen = resetMethod.getText().toString();
//        radioResetPassGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
//        {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if(methodChosen.equals("By Phone")){
//                    email.setEnabled(false);
//                }
//                if(methodChosen.equals("By Email")){
//                    phone.setEnabled(false);
//                }
//            }
//        });

        mDialogOKButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(mDialog.getContext(), R.style.MyTheme);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.getWindow().setGravity(Gravity.CENTER);

                email = (EditText) mDialog.findViewById(R.id.email);
                phone = (EditText) mDialog.findViewById(R.id.phone);
                String useremail = email.getText().toString();
                String phones = phone.getText().toString();
                radioResetPassGroup = (RadioGroup) mDialog.findViewById(R.id.radiogroup);
                int selectedId = radioResetPassGroup.getCheckedRadioButtonId();
                resetMethod = (RadioButton) mDialog.findViewById(selectedId);
                String methodChosen = resetMethod.getText().toString();
                if (methodChosen.contains("By Phone") && !phones.equals("") && NumberUtils.isNumber(phones) == true) {
                    System.out.println("Phone: " + phones + " CHOSEN METHOD: " + methodChosen);
                    mAuthTask = new UserLoginTask(phones, "by_phone");
                    progressDialog.show();
                    mAuthTask.execute((Void) null);
                } else if (methodChosen.contains("By Phone") && phones.equals("") && NumberUtils.isNumber(phones) == false) {
                    phone.setError("fill the phone number");
                    email.setError("");
                }

                if (methodChosen.contains("By Email") && !useremail.equals("") && useremail.contains("@")) {

                    System.out.println("EMAIL: " + useremail + " CHOSEN METHOD: " + methodChosen);
                    mAuthTask = new UserLoginTask(useremail, "by_email");
//                    progressDialog.show();
                    mAuthTask.execute((Void) null);
                } else if (methodChosen.contains("By Email") && useremail.equals("") && !useremail.contains("@")) {
                    email.setError("fill valid email address");
                    phone.setError("");
                }
            }
        });

        mDialogCancelButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
    }

    public void dismissDialog() {
        mDialog.dismiss();
    }

    public class UserLoginTask extends AsyncTask<Void, Void, JSONObject> {

        private final String mPhone;
        private final String mChosen;

        UserLoginTask(String phone, String mChosen) {
            this.mPhone = phone;
            this.mChosen = mChosen;
        }

        @Override
        public JSONObject doInBackground(Void... params) {

            UrlCon js = new UrlCon();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("tag", Constants.TAGRESETPASSWORD);
                jsonObject.put(Constants.IMEI, Constants.getIMEINO(mDialog.getContext()));

                String array;
                JSONArray jarr = new JSONArray();
                JSONObject j = new JSONObject();
                j.put("credential", mPhone);
                j.put("method", mChosen);
                jarr.put(j);
                jsonObject.put("reset_user", (Object) jarr);
                System.out.println("SENT DATA: " + jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
                JSONObject job = new JSONObject(array);
                System.out.println("RETURNED JSON DATA: " + job);
                return job;
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            mAuthTask = null;
            try {
                if (result.toString().contains("success")) {
//                    String results = result.getString("data");
                    System.out.println("SUCCESS");
                    progressDialog.dismiss();
                    Intent intent = new Intent(mDialog.getContext(), InputResetCode.class);
                    mDialog.getContext().startActivity(intent);
                    dismissDialog();
                } else if (result.toString().contains("failed")) {
                    progressDialog.dismiss();
                    String methodChosen = resetMethod.getText().toString();
                    if (methodChosen.contains("By Phone")) {
                        phone.setError("Phone not available");
                    }
                    if (methodChosen.contains("By Email")) {
                        email.setError("Email not available");
                    }
                }else{
                    progressDialog.dismiss();
                }
//                progressDialog.dismiss();
            } catch (Exception e) {
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }
}
