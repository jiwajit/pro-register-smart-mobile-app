/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bongolive.apps.proregister.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author nasznjoka
 */
public class UrlCon {
    static String response = null;
    static JSONObject jObj = null;
    static InputStream is;
    static String json = "";
    private final String POSTMETHOD = "POST";
    org.apache.log4j.Logger log = Log4Helper.getLogger(UrlCon.class.getSimpleName());

    public UrlCon() {

    }

    public String getJson(String url, JSONObject params) {
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn = (HttpURLConnection) _url.openConnection();
            urlConn.setRequestMethod(POSTMETHOD);
            urlConn.setRequestProperty("Content-Type", "applicaiton/json; charset=utf-8");
            urlConn.setRequestProperty("Accept", "applicaiton/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConn.getInputStream();
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            log.error("MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            log.error("IOException " + e.getMessage());
        }

        //return response;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            log.info(response);
        } catch (Exception e) {
            log.error("Buffer error Error converting result " + e.getMessage());
        }

        return response;

    }

    public JSONObject getJsonObject(String url, JSONObject params) {

        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn = (HttpURLConnection) _url.openConnection();
            urlConn.setRequestMethod(POSTMETHOD);
            urlConn.setRequestProperty("Content-Type", "applicaiton/json; charset=utf-8");
            urlConn.setRequestProperty("Accept", "applicaiton/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if (urlConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConn.getInputStream();
            } else {
                is = urlConn.getErrorStream();
            }


        } catch (MalformedURLException e) {
            log.error("MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            log.error("IOException " + e.getMessage());
        }

        //return response;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            log.info(json.toString());
        } catch (Exception e) {
            log.error("Buffer error Error converting result " + e.getMessage());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            log.error("Buffer error Error parsing data " + e.getMessage());
            System.out.println("JSON ERROR: ");
        }

        // return JSON String`
        return jObj;

    }

}
