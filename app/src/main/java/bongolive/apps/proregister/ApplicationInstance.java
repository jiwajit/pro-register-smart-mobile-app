/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proregister;

import android.app.Application;
import android.os.Environment;

import java.io.File;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Log4Helper;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
    public class ApplicationInstance extends Application {

        @Override
        public void onCreate() {
            super.onCreate();

            configureLog4j();
        }

        private void configureLog4j() {

            File sdcard = Environment.getExternalStorageDirectory();
            File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
            String fileName = folder+"/" +"smart.log";
            String filePattern = "%d - [%c] - %p : %m%n";
            int maxBackupSize = 10;
            long maxFileSize = 1024 * 1024;

            Log4Helper.configure(fileName, filePattern, maxBackupSize, maxFileSize);
        }

    }