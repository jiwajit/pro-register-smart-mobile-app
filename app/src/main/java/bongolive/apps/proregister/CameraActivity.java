/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bongolive.apps.proregister;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.widget.FrameLayout;

import org.apache.log4j.Logger;

import bongolive.apps.proregister.utils.Log4Helper;

public class CameraActivity extends Activity {
    String TAG = CameraActivity.class.getSimpleName();
    Camera mCamera;
    private CameraPreview mCameraView = null;
    private DrawingView drawingView;
    Logger log = Log4Helper.getLogger(TAG);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phototaker);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        try {
            mCamera = openFrontCamera();
        } catch (Exception e) {
            log.error("Failed to get camera: " + e.getMessage());
        }
        if (mCamera != null) {
            mCameraView = new CameraPreview(this, mCamera);//create a SurfaceView to show camera data
            FrameLayout camera_view = (FrameLayout) findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);//add the SurfaceView to the layout
            drawingView = new DrawingView(this,null);
            mCameraView.setListener();
            mCameraView.setDrawingView(drawingView);
        }

    }
    private Camera openFrontCamera() {
        int cameraCount = 0;
        Camera cam = null;
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing != Camera.CameraInfo.CAMERA_FACING_BACK) {
                try {
                    cam = Camera.open(camIdx);
                } catch (RuntimeException e) {
                    log.error("Camera failed to open: " + e.getLocalizedMessage());
                }
            }
        }

        return cam;
    }


}
