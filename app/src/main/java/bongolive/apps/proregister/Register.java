/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.File;
import java.util.Locale;

import bongolive.apps.proregister.utils.AudioService;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.PermissionUtils;
import bongolive.apps.proregister.utils.Validating;


public class Register extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
        , LocationListener {
    private static final int REGISTER_SUBSCRIBER = 5;
    boolean tabletSize = false;
    static MenuItem mMenuItem;
    AppPreference appPreference;
    static boolean source = false;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static String TAG = Register.class.getName();

    /* location */
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private boolean mPermissionDenied = false;
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private Location currentlocation;
    private boolean LOCATION_ALLOWED;
    private static final int REQUEST_CHECK_SETTINGS = 11;
    private String[] loc = null;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_tabs);

        Intent intent = getIntent();
        if (intent.hasExtra("update"))
            source = true;
        else
            source = false;

        System.out.println("source is " + source);
        System.out.println("EXTRAS " + intent.getStringExtra("update"));

        appPreference = new AppPreference(this);
        appPreference.setDefaultLanguage();
        String languageToLoad = appPreference.getDefaultLanguage();
        if (!Validating.areSet(languageToLoad)) {
            languageToLoad = "sw";
        }
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        tabletSize = getResources().getBoolean(R.bool.isTablet);

        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        try {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        loc = appPreference.getLastLocation();
        if (loc[0].equals("0") || loc[1].equals("0"))
            buildGoogleApiClient();

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        String path = appPreference.getAudio();
        if (appPreference.getSettings(Constants.RECORD_AUDIO)) {
            if (ContextCompat.checkSelfPermission(Register.this, Manifest.permission.RECORD_AUDIO)
                    == PackageManager.PERMISSION_GRANTED) {
                PackageManager pm = getPackageManager();
                boolean micPresent = pm.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);
                if (micPresent)
                    if (Validating.areSet(path)) {
                        File file = new File(path);
                        if (!file.exists()) {
                            System.out.println("start  recording");
                            startService(new Intent(Register.this, AudioService.class));
                            Handler handler = new Handler();

                            Runnable run = new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("stop recording");
                                    stopService(new Intent(Register.this, AudioService.class));
                                }
                            };
                            handler.postDelayed(run, 90000);
                        }
                    } else {
                        System.out.println("start  recording");
                        startService(new Intent(Register.this, AudioService.class));
                        Handler handler = new Handler();

                        Runnable run = new Runnable() {
                            @Override
                            public void run() {
                                System.out.println("stop recording");

                                stopService(new Intent(Register.this, AudioService.class));

                            }
                        };
                        handler.postDelayed(run, 90000);
                    }
            }
        }
    }


    /**
     * Builds a GoogleApiClient. Uses {@code #addApi} to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        appPreference = new AppPreference(this);
        appPreference.setDefaultLanguage();
        String languageToLoad = appPreference.getDefaultLanguage();
        if (!Validating.areSet(languageToLoad)) {
            languageToLoad = "sw";
        }
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
    }

    private void showToast(String txt) {
        Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        LOCATION_ALLOWED = true;
                        break;
                    case Activity.RESULT_CANCELED:
                        showToast(getString(R.string.strlocation_required));
                        LOCATION_ALLOWED = false;
                        finish();
                        break;
                    default:
                        break;
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.i("event", "captured");
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            if (source) {
                appPreference.clearPhotos(null);
                appPreference.clearStates();
                source = false;
            }
            finish();

            return true;
        } else
            return super.onKeyUp(keyCode, event);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        mMenuItem = menu.findItem(R.id.action_save);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            if (source) {
                appPreference.clearPhotos(null);
                appPreference.clearStates();
                source = false;
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            i = i + 1;
            if (i == 1)
                return SubscriberInfo.newInstance(source);
            else if (i == 2)
                return SubscriberIdentity.newInstance(source);
            else if (i == 3)
                return SubscriberContacts.newInstance(source, mMenuItem);
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.strcustomerinfo);
                case 1:
                    return getString(R.string.strcustomerid);
                case 2:
                    return getString(R.string.strcustomercontacts);
            }
            return null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

/*TODO
* work on the main navigation drawer for tablets to have the same appeal as the old nav system
*
* */


    private void showMissingPermissionError() {
        showToast(getString(R.string.strlocation_denied));
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

            if (currentlocation == null)
                currentlocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);

        } else
            showMissingPermissionError();
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.i(TAG, "GoogleApiClient connection has failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("connection received " + location.toString());
        currentlocation = location;
        try {

            double lat = currentlocation.getLatitude();
            double lng = currentlocation.getLongitude();

            String[] v = new String[]{String.valueOf(lat), String.valueOf(lng)};
//                showToast(""+lat+", "+lng);
            if (Validating.areSet(v))
                appPreference.saveLocation(v);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        //update location
    }
}
