/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bongolive.apps.proregister;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Validating;

/**
 * Simple Fragment used to display some meaningful content for each page in the sample's
 * {@link android.support.v4.view.ViewPager}.
 */
public class SubscriberIdentity extends Fragment {

    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";
    private static final int TAKE_ID_PHOTO = 4;
    EditText etotheridtype;
    private int rotationAngle;
    private EditText etidno;
    ImageView idphoto ; //customer id
    String setidno, sidphoto;// customer id
    private AutoCompleteTextView autocitizen;
    String idtype,fcitizen,fidtype;
    private Spinner spidtype;
    ArrayAdapter<String> idadapter,citizenadapter;
    private static Uri idphoto_uri;
    private static String idphto_path;
    private AppPreference appPreference;
    static boolean update;
    /**
     * @return a new instance of {@link SubscriberIdentity}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static SubscriberIdentity newInstance(boolean f) {
        Bundle bundle = new Bundle();
        update = f;
        SubscriberIdentity fragment = new SubscriberIdentity();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subscriberidfrag, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        appPreference = new AppPreference(getActivity());
        spidtype = (Spinner)getActivity().findViewById(R.id.spidtype);
        autocitizen = (AutoCompleteTextView)getActivity().findViewById(R.id.autoctz);
        etotheridtype = (EditText)getActivity().findViewById(R.id.etother);


        /* customer identification*/
        etidno = (EditText)getActivity().findViewById(R.id.etidno);
        idphoto = (ImageView)getActivity().findViewById(R.id.idimgphoto);
        idphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sidphoto != null) {
                    preview_photo(sidphoto);
                } else {
                    if (Constants.isSdMounted()) {
                        add_id_photo();
                    } else {
                        Toast.makeText(getActivity(), "no sd card ", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        if(update){
//            idphoto.setVisibility(View.GONE);
        }
        /* end of customer identification */

        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for( Locale loc : locale ){
            country = loc.getDisplayCountry();
            if( country.length() > 0 && !countries.contains(country) ){
                countries.add( country );
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        String[] idtype = getResources().getStringArray(R.array.idtype);

        idadapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1, idtype);
        citizenadapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item, countries);


        autocitizen.setAdapter(citizenadapter);
        spidtype.setAdapter(idadapter);
        autocitizen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                autocitizen.setText(pro);
                fcitizen = pro;
            }
        });

        spidtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (position == idadapter.getCount() - 1) {
                        etotheridtype.setVisibility(View.VISIBLE);
                        String ty = etotheridtype.getText().toString();
                        if (!ty.isEmpty())
                            fidtype = ty;
                    } else if (position != idadapter.getCount() - 1) {
                        etotheridtype.setVisibility(View.GONE);
                        etotheridtype.setText("");
                        fidtype = idadapter.getItem(position);
                    } else {
                        fidtype = "";
                    }
                } else {
                    fidtype = "";
                }
            }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

    public void add_id_photo(){
        if(getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //open camera
            Intent intent = new Intent(getActivity(),PhotoTaker.class);
            intent.putExtra("source", 2);
            intent.putExtra("type", 2);
            startActivityForResult(intent, TAKE_ID_PHOTO);
        } else {
            Toast.makeText(getActivity(),"this device does not have camera", Toast.LENGTH_SHORT).show();
        }
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case TAKE_ID_PHOTO:
//                Log.e("URI","Uri is "+idphoto_uri);
//                if (idphoto_uri != null) {
//                    String imageFilePath = idphoto_uri.getPath();
//                    Log.e("IMGPATH","path is "+imageFilePath);
//                    if (imageFilePath != null && imageFilePath.length() > 0) {
//                        // File myFile = new File(imageFilePath);
//                        idphto_path = imageFilePath;
//                        appPreference.saveSubscriberIdPhoto(idphto_path);
//                        Bitmap bitmap = Constants.decodeImg(idphto_path, 150, 150);
//                        if(bitmap == null) {
//                            appPreference.clearPhotos(Constants.SUBIDPHOTO);
//                            sidphoto = null;
//                        }
//                        sidphoto = idphto_path;
//                        idphoto.setImageBitmap(bitmap);
//                        idphoto.setRotation(90);
//                        try {
//                            // if (bmp != null)
//                            // bmp.recycle();
//                            int mobile_width = 480;
//                            BitmapFactory.Options options = new BitmapFactory.Options();
//                            // options.inJustDecodeBounds = true;
//                            // BitmapFactory.decodeFile(imageFilePath, options);
//                            int outWidth = options.outWidth;
//                            // int outHeight = options.outHeight;
//                            int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);
//
//                            if (ratio == 0) {
//                                ratio = 1;
//                            }
//                            ExifInterface exif = new ExifInterface(imageFilePath);
//
//                            String orientString = exif
//                                    .getAttribute(ExifInterface.TAG_ORIENTATION);
//                            int orientation = orientString != null ? Integer
//                                    .parseInt(orientString)
//                                    : ExifInterface.ORIENTATION_NORMAL;
//                            System.out.println("Orientation : " + orientation);
//                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
//                                rotationAngle = 90;
//                            if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
//                                rotationAngle = 180;
//                            if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
//                                rotationAngle = 270;
//
//                            System.out.println("Rotation : " + rotationAngle);
//
//                            options.inJustDecodeBounds = false;
//                            options.inSampleSize = ratio;
//
//                            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath,
//                                    options);
//                            File myFile = new File(imageFilePath);
//                            FileOutputStream outStream = new FileOutputStream(
//                                    myFile);
//                            if (bmp != null) {
//                                bmp.compress(Bitmap.CompressFormat.JPEG, 100,
//                                        outStream);
//                                outStream.flush();
//                                outStream.close();
//
//                                Matrix matrix = new Matrix();
//                                matrix.setRotate(rotationAngle,
//                                        (float) bmp.getWidth() / 2,
//                                        (float) bmp.getHeight() / 2);
//
//                                bmp = Bitmap.createBitmap(bmp, 0, 0,
//                                        bmp.getWidth(), bmp.getHeight(), matrix,
//                                        true);
//
//                                // ivStuffPicture.setImageBitmap(bmp);
//
//                                idphto_path = MediaStore.Images.Media.insertImage(
//                                        getActivity().getContentResolver(), bmp,
//                                        Calendar.getInstance().getTimeInMillis()
//                                                + ".jpg", null);
//                                /*appPreference.saveSubscriberPhoto(custphoto_path);
//                                Bitmap bitmap = Constants.decodeImg(custphoto_path, 150, 150);
//                                if(bitmap == null) {
//                                    appPreference.clearPhotos(Constants.SUBPHOTO);
//                                    simguserphoto = null;
//                                }
//                                simguserphoto = custphoto_path;
//                                imguserphoto.setImageBitmap(bitmap);*/
//                            }
//
//                        } catch (OutOfMemoryError e) {
//                            System.out.println("out of bound");
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                } else {
//                    Toast.makeText(
//                            getContext(),
//                            getResources().getString(
//                                    R.string.toast_unable_to_selct_image),
//                            Toast.LENGTH_LONG).show();
//                }
//                break;
//        }
//
//    }

    private void add_id_photos() {
        Calendar cal = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        idphoto_uri = Uri.fromFile(file);
        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, idphoto_uri);
        startActivityForResult(i, TAKE_ID_PHOTO);
    }
    @Override
    public void onResume() {
        super.onResume();
        String path = appPreference.getSubIdPhoto();
        if(Validating.areSet(path)) {
            idphto_path = path;
            idphoto_uri = Uri.parse(idphto_path);
            Bitmap bitmap = Constants.decodeImg(idphto_path,150,150);
            if(bitmap == null) {
                appPreference.clearPhotos(Constants.SUBIDPHOTO);
                sidphoto = null;
            }
            idphoto.setImageBitmap(bitmap);
//            idphoto.setRotation(90);
            sidphoto = idphto_path;
        }
        String c,idno,idty;
        c = appPreference.getCitizenship();
        idno = appPreference.getIdNo();
        idty = appPreference.getIdType();
        if(Validating.areSet(idty)){
            int sele = idadapter.getPosition(idty);
            spidtype.setSelection(sele);
        }

        autocitizen.setText(c);
        fcitizen = c;

        etidno.setText(idno);
    }

    @Override
    public void onDestroy() {
        etidno.setText("");
        autocitizen.setText("");
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();

        if(!update) {
            System.out.println("paused identity");
            setidno = etidno.getText().toString().trim();

            appPreference.saveCitizen(fcitizen);
            appPreference.saveIdNo(setidno);
            appPreference.saveIdType(fidtype);
//            etidno.setText("");
//            autocitizen.setText("");
//            idphoto.setImageBitmap(null);
//            spidtype.setId(0);
//            spidtype.setSelection(0);
//            autocitizen.setHint("citizenship");
        }
    }

    private void preview_photo(String photo) {
        add_id_photo();
        /*LayoutInflater infl = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.phototaker, null);

        final Dialog dg = new Dialog(getContext(), R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);

        FrameLayout camera_view = (FrameLayout) dg.findViewById(R.id.camera_view);
        if(photo == null)
            dg.dismiss();

        ImageView imageView = new ImageView(dg.getContext());

        camera_view.addView(imageView);


        Bitmap bitmap = Constants.decodeImg(photo, 400, 400);
        if(bitmap != null) {
            imageView.setImageBitmap(bitmap);
            imageView.setRotation(90);
        }

        ImageButton imgClose = (ImageButton) dg.findViewById(R.id.imgClose);
        ImageButton imgSave = (ImageButton) dg.findViewById(R.id.imgTake);

        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.photo);
        imgSave.setImageBitmap(bitmap1);
        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_id_photo();
                dg.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dg.dismiss();
            }
        });
        dg.show();*/
    }
}
