package bongolive.apps.proregister;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.UrlCon;

public class InputResetCode extends AppCompatActivity {

    private EditText resetcode;
    private TextView ok, cancel;
    private UserCodeTask mAuthTask = null;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_reset_code);
        resetcode = (EditText) findViewById(R.id.rstcode);
        final String code = resetcode.getText().toString();
//        Toast.makeText(getBaseContext(), "" + code, Toast.LENGTH_LONG).show();
        ok = (TextView) findViewById(R.id.warning_ok);
        cancel = (TextView) findViewById(R.id.warning_cancel);
        progressDialog = new ProgressDialog(getBaseContext(), R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.getWindow().setGravity(Gravity.CENTER);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resetcode.getText().equals("")) {
                    resetcode.setError("write the code you received");
                } else {
                    getData();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getData() {
        mAuthTask = new UserCodeTask(resetcode.getText().toString());
        mAuthTask.execute((Void) null);
//        progressDialog.show();
    }


    private class UserCodeTask extends AsyncTask<Void, Void, JSONObject> {

        private String mCode;

        UserCodeTask(String code) {
            this.mCode = code;

        }

        @Override
        public JSONObject doInBackground(Void... params) {

            UrlCon js = new UrlCon();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("tag", Constants.TAGRESETCODE);
                jsonObject.put(Constants.IMEI, Constants.getIMEINO(getBaseContext()));
                String array;
                JSONArray jarr = new JSONArray();
                JSONObject j = new JSONObject();
                j.put("credential", mCode);
                jarr.put(j);
                jsonObject.put("reset_code", jarr);
                System.out.println("SENT DATA: " + jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject);
                JSONObject job = new JSONObject(array);
                System.out.println("RETURNED JSON DATA: " + job);
                return job;
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                if (result.toString().contains("success")) {
                    System.out.println("SUCCESS");
                    Intent intent = new Intent(getBaseContext(), InputNewPassword.class);
                    startActivity(intent);
                    finish();
                } else if (result.toString().contains("failed")) {

                }
//                progressDialog.dismiss();
            } catch (Exception e) {
            }

        }

        @Override
        protected void onCancelled() {
        }
    }

}
