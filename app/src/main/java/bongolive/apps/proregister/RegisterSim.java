/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proregister;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import bongolive.apps.proregister.db.Customers;
import bongolive.apps.proregister.db.MultimediaContents;
import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Validating;


public class RegisterSim extends Fragment {

    String[] finalvals = null, custinfo = null, custid = null, custcontact = null;
    String[] finalcheckvals = null;
    LinearLayout layinfo, layid, laycontac;
    Spinner sptitle, spgender, spidtype;
    String fgender, ftitle, fidtype, fcitizen, fregion;
    AutoCompleteTextView autocitizen, autoregion;

    EditText etnm, etsnm, etdis, etstreet, etadd, etjob, etphn, etserial, etimei;
    ImageView imguserphoto;
    TextView etbdt;
    //customer info
    String setnm, setsnm, setdis, setstreet, setadd, setbdt, setjob, setphn, setserial, setimei, simguserphoto; //customer info

    EditText etotherphn, etmail;
    CheckBox chkconsent;
    ImageView signphoto; // other contacts
    String setotherphn, setmail, schkconsent, ssignphoto; // other contacts

    EditText etidno;
    ImageView idphoto; //customer id
    String setidno, sidphoto;// customer id

    EditText etotheridtype;

    ArrayAdapter<String> titleadapter, genderadapter, idadapter, citizenadapter, regionadapter;


    private static final int TAKE_CUSTOMER_PHOTO = 3;
    private static final int TAKE_ID_PHOTO = 4;
    private static final int TAKE_SIGNATURE_PHOTO = 5;
    private static final int PRINT_DETAILS = 6;
    private static Bitmap bitmap_cust = null, bitmap_id = null, bitmap_sign = null;
    private static Uri custphot_uri, idphoto_uri, signphoto_uri;
    private static String custphoto_path, idphto_path, signphoto_path;
    View _rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.addcustomer, container, false);
        } else {
            ((ViewGroup) _rootView.getParent()).removeView(_rootView);
        }
        return _rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        Customers.resetsync(getActivity());

        layinfo = (LinearLayout) getActivity().findViewById(R.id.laycutinfo);
        layid = (LinearLayout) getActivity().findViewById(R.id.layid);
        laycontac = (LinearLayout) getActivity().findViewById(R.id.laycontact);

        sptitle = (Spinner) getActivity().findViewById(R.id.sptitle);
        spgender = (Spinner) getActivity().findViewById(R.id.spgender);
        spidtype = (Spinner) getActivity().findViewById(R.id.spidtype);
        autocitizen = (AutoCompleteTextView) getActivity().findViewById(R.id.autoctz);
        autoregion = (AutoCompleteTextView) getActivity().findViewById(R.id.autoregion);
        etotheridtype = (EditText) getActivity().findViewById(R.id.etother);

        /* customer information */
        etnm = (EditText) getActivity().findViewById(R.id.etname);
        etsnm = (EditText) getActivity().findViewById(R.id.etsname);
        etdis = (EditText) getActivity().findViewById(R.id.etdistrict);
        etstreet = (EditText) getActivity().findViewById(R.id.etstreet);
        etadd = (EditText) getActivity().findViewById(R.id.etadd);
        etbdt = (TextView) getActivity().findViewById(R.id.txtbdat);
        etjob = (EditText) getActivity().findViewById(R.id.etjob);
        etphn = (EditText) getActivity().findViewById(R.id.etphno);
        etserial = (EditText) getActivity().findViewById(R.id.etsimserial);
        etimei = (EditText) getActivity().findViewById(R.id.etimei);
        imguserphoto = (ImageView) getActivity().findViewById(R.id.imgphoto);
        imguserphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isSdMounted()) {
                    add_customer_photo();
                } else {
                    Toast.makeText(getActivity(), "no sd card ", Toast.LENGTH_LONG).show();
                }
            }
        });

        etbdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.setDateTime(getActivity(), etbdt);
            }
        });

        etbdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Constants.setDateTime(getActivity(), etbdt);
                }
            }
        });

        etbdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String minvalue = "1900-06-30";
                String maxvalue = "2014-06-30";
                setbdt = etbdt.getText().toString();

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date entered = df.parse(setbdt);
                    Date mindate = df.parse(minvalue);
                    Date maxdate = df.parse(maxvalue);
                    Date common = df.parse("0000-00-00");
                    Log.v("dates", " MIN DATE " + mindate + " MAX DATE " +
                            maxdate + " entered value is " + entered);
                    if (entered.compareTo(mindate) <= 0) {
                        Toast.makeText(getActivity(), getString(R.string.strmin)
                                        + " " + minvalue,
                                Toast.LENGTH_LONG).show();
                        etbdt.setText(getString(R.string.strbdate));
                        setbdt = "";
                        Constants.setDateTime(getActivity(), etbdt);
                    }
                    if (entered.compareTo(common) != 0) {
                        if (entered.compareTo(maxdate) > 0) {
                            Toast.makeText(getActivity(), getString(R.string.strmax)
                                            + " " + maxvalue,
                                    Toast.LENGTH_LONG).show();
                            etbdt.setText(getString(R.string.strbdate));
                            setbdt = "";
                            Constants.setDateTime(getActivity(), etbdt);
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        /* end of customer information*/

        /* customer identification*/
        etidno = (EditText) getActivity().findViewById(R.id.etidno);
        idphoto = (ImageView) getActivity().findViewById(R.id.idimgphoto);
        idphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isSdMounted()) {
                    add_id_photo();
                } else {
                    Toast.makeText(getActivity(), "no sd card ", Toast.LENGTH_LONG).show();
                }
            }
        });
        /* end of customer identification */

        /* customer other contacts */

        etotherphn = (EditText) getActivity().findViewById(R.id.etotherph);
        etmail = (EditText) getActivity().findViewById(R.id.etmail);
        chkconsent = (CheckBox) getActivity().findViewById(R.id.chkconsent);
        chkconsent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    schkconsent = "true";
                } else {
                    schkconsent = "";
                }
            }
        });
        signphoto = (ImageView) getActivity().findViewById(R.id.signphoto);
        signphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Handler handler = new Handler();
                Runnable run = new Runnable() {
                    @Override
                    public void run() {
                        add_sign_photo();
                    }
                };
                handler.post(run);
            }
        });


        /* end of other contacta*/


        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for (Locale loc : locale) {
            country = loc.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        final String[] titles = getResources().getStringArray(R.array.title);
        String[] idtype = getResources().getStringArray(R.array.idtype);
        String[] gender = getResources().getStringArray(R.array.gender);
        String[] regions = getResources().getStringArray(R.array.allregions);

        titleadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titles);
        idadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, idtype);
        genderadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, gender);
        regionadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, regions);

        citizenadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, countries);


        autocitizen.setAdapter(citizenadapter);
        spidtype.setAdapter(idadapter);
        sptitle.setAdapter(titleadapter);
        spgender.setAdapter(genderadapter);
        autoregion.setAdapter(regionadapter);

        autocitizen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                autocitizen.setText(pro);
                fcitizen = pro;
            }
        });
        autoregion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                autoregion.setText(pro);
                fregion = pro;
            }
        });

        spidtype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (position == idadapter.getCount() - 1) {
                        etotheridtype.setVisibility(View.VISIBLE);
                        String ty = etotheridtype.getText().toString();
                        if (!ty.isEmpty())
                            fidtype = ty;
                    } else if (position != idadapter.getCount() - 1) {
                        etotheridtype.setVisibility(View.GONE);
                        etotheridtype.setText("");
                        fidtype = idadapter.getItem(position);
                    } else {
                        fidtype = "";
                    }
                } else {
                    fidtype = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sptitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    ftitle = titleadapter.getItem(position);
                    if (ftitle.equals(titles[2]) || ftitle.equals(titles[3]) || ftitle.equals(titles[4])) {
                        spgender.setSelection(2);
                        fgender = genderadapter.getItem(2);
                    } else if (ftitle.equals(titles[1])) {
                        spgender.setSelection(1);
                        fgender = genderadapter.getItem(1);
                    } else {
                        spgender.setSelection(0);
                        fgender = "";
                    }
                } else {
                    ftitle = "";
                    fgender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spgender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    fgender = genderadapter.getItem(position);
                } else {
                    fgender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        autocitizen.setThreshold(1);
        autoregion.setThreshold(1);

        if (savedInstanceState != null) {
            String[] a = savedInstanceState.getStringArray(Constants.CUSTOMERINFO);
            String[] b = savedInstanceState.getStringArray(Constants.CUSTOMERID);
            String[] c = savedInstanceState.getStringArray(Constants.CUSTOMERCONTACTS);
            etnm.setText(a[0]);
            etsnm.setText(a[1]);
        }

        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.register, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {

            setnm = etnm.getText().toString().trim();
            setsnm = etsnm.getText().toString().trim();
            setdis = etdis.getText().toString().trim();
            setstreet = etstreet.getText().toString().trim();
            setadd = etadd.getText().toString().trim();
            setjob = etjob.getText().toString().trim();
            setserial = etserial.getText().toString().trim();
            setimei = etimei.getText().toString().trim();
            setphn = etphn.getText().toString().trim();

            setidno = etidno.getText().toString().trim();

            setotherphn = etotherphn.getText().toString().trim();
            setmail = etmail.getText().toString().trim();


            custinfo = new String[]{ftitle, fgender, setnm, setsnm, fregion, setdis, setstreet, setadd, setbdt, setjob, setphn,
                    setserial, setimei, simguserphoto};//14 fields
            custid = new String[]{fcitizen, setidno, fidtype, sidphoto};
            custcontact = new String[]{setotherphn, setmail, ssignphoto, schkconsent};
            finalcheckvals = new String[]{ftitle, fgender, setnm, setsnm, fregion, setdis, setstreet, setbdt, setjob, setphn,
                    simguserphoto, fcitizen, setidno, fidtype, sidphoto, ssignphoto, schkconsent};
            String uniqid = Constants.getLocalUniqueId(getActivity());

            if (Validating.areSet(finalcheckvals)) {
                finalvals = new String[]{ftitle, fgender, setnm, setsnm, fregion, setdis, setstreet, setadd, setbdt, setjob, setphn,
                        setserial, setimei, fcitizen, setidno, fidtype, setotherphn, setmail, uniqid};

                if (!Customers.isCustomerUnique(getActivity(), setphn)) {
                    boolean insert = Customers.insertCustomer(getActivity(), finalvals);
                    if (insert) {
                        String cusid = Customers.getId(getActivity());
                        if (Validating.areSet(cusid)) {
                            String[] mult1 = {simguserphoto, setnm + Constants.IMGUSER + "_" + System.currentTimeMillis(),
                                    Constants.MULT_IMAGE, Constants
                                    .IMG_TYPE_CUSTOMER, Constants.IMGUSER_MEDIATYPE, Customers.getId(getActivity()), uniqid};
                            String[] mult2 = {sidphoto, setnm + Constants.IMGID + "_" + System.currentTimeMillis(), Constants.MULT_IMAGE, Constants.IMG_TYPE_CUSTOMER,
                                    Constants.IMGID_MEDIATYPE, Customers.getId(getActivity()), uniqid};
                            String[] mult3 = {ssignphoto, setnm + Constants.IMGSIGN + "_" + System.currentTimeMillis(), Constants.MULT_IMAGE,
                                    Constants
                                            .IMG_TYPE_CUSTOMER, Constants.IMGSIGN_MEDIATYPE, Customers.getId(getActivity()), uniqid};

                            int storemult = MultimediaContents.storeMultimedia(getActivity(), mult1);
                            int storemult1 = MultimediaContents.storeMultimedia(getActivity(), mult2);
                            int storemult2 = MultimediaContents.storeMultimedia(getActivity(), mult3);
                            if (storemult == 1 && storemult1 == 1 && storemult2 == 1) {
                                Toast.makeText(getActivity(), getString(R.string.stradded), Toast
                                        .LENGTH_SHORT).show();
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(getString(R.string.print))
                                        .setCancelable(false)
                                        .setPositiveButton(getString(R.string.stryes), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //implement printer
                                                finalvals = new String[]{ftitle, fgender, setnm, setsnm, fregion, setdis, setstreet, setadd, setbdt, setjob, setphn,
                                                        setserial, setimei, fcitizen, setidno, fidtype, setotherphn, setmail};

                                                Intent intent = new Intent(getActivity(), PrintingScreen.class);
                                                intent.putExtra("subscriber", finalvals);
                                                startActivity(intent);
                                                dialog.cancel();
                                                autocitizen.setText("");
                                                autoregion.setText("");
                                                etotheridtype.setText("");
                                                etidno.setText("");
                                                signphoto.setImageBitmap(null);
                                                imguserphoto.setImageBitmap(null);
                                                idphoto.setImageBitmap(null);
                                                etotherphn.setText("");
                                                etmail.setText("");
                                                chkconsent.setChecked(false);
                                                etnm.setText("");
                                                etsnm.setText("");
                                                etserial.setText("");
                                                etdis.setText("");
                                                etadd.setText("");
                                                etjob.setText("");
                                                etimei.setText("");
                                                spgender.setAdapter(genderadapter);
                                                spidtype.setAdapter(idadapter);
                                                sptitle.setAdapter(titleadapter);
                                                etstreet.setText("");
                                                etbdt.setText("");
                                                etphn.setText("");
//                                                getActivity().finish();
                                            }
                                        })
                                        .setNegativeButton(getString(R.string.strno), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                System.out.println("clicked no print");
                                                autocitizen.setText("");
                                                autoregion.setText("");
                                                etotheridtype.setText("");
                                                etidno.setText("");
                                                signphoto.setImageBitmap(null);
                                                imguserphoto.setImageBitmap(null);
                                                idphoto.setImageBitmap(null);
                                                etotherphn.setText("");
                                                etmail.setText("");
                                                chkconsent.setChecked(false);
                                                etnm.setText("");
                                                etsnm.setText("");
                                                etserial.setText("");
                                                etdis.setText("");
                                                etadd.setText("");
                                                etjob.setText("");
                                                etimei.setText("");
                                                spgender.setAdapter(genderadapter);
                                                spidtype.setAdapter(idadapter);
                                                sptitle.setAdapter(titleadapter);
                                                etstreet.setText("");
                                                etbdt.setText("");
                                                etphn.setText("");
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();
                            }
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.strnumbernotunique), Toast.LENGTH_SHORT).show();
                    etphn.setTextColor(Color.RED);
                    etphn.requestFocus();
                }
            } else {
                Toast.makeText(getActivity(), getString(R.string.strfillblanks), Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void add_customer_photo() {
        if (getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //open camera
            Log.v("picture", "capturing picture");
           /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, custphot_uri);
            startActivityForResult(intent, TAKE_CUSTOMER_PHOTO);*/
            Intent intent = new Intent(getActivity(), SignatureDrawer.class);
            intent.putExtra("source", 1);
            startActivityForResult(intent, TAKE_CUSTOMER_PHOTO);
        } else {
            Toast.makeText(getActivity(), "this device does not have camera", Toast.LENGTH_SHORT).show();
        }
    }

    public void add_id_photo() {
        if (getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            //open camera
            Log.v("picture", "capturing picture");
           /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, idphoto_uri);
            startActivityForResult(intent, TAKE_ID_PHOTO);*/
            Intent intent = new Intent(getActivity(), SignatureDrawer.class);
            intent.putExtra("source", 1);
            startActivityForResult(intent, TAKE_ID_PHOTO);
        } else {
            Toast.makeText(getActivity(), "this device does not have camera", Toast.LENGTH_SHORT).show();
        }
    }

    public void add_sign_photo() {
        Intent intent = new Intent(getActivity(), SignatureDrawer.class);
        intent.putExtra("source", 0);
        startActivityForResult(intent, TAKE_SIGNATURE_PHOTO);
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
//        super.onActivityResult(reqCode,resCode,data);
        Log.v("reqcode", "req code is " + reqCode);
        if (resCode == Activity.RESULT_OK && data != null)
            switch (reqCode) {
                case TAKE_CUSTOMER_PHOTO:
                    Log.v("reqcode", "req code is " + reqCode);
                    if (resCode == getActivity().RESULT_OK && data != null) {
                        custphoto_path = data.getExtras().getString("data");
                        custphot_uri = Uri.parse(custphoto_path);

                        imguserphoto.setImageURI(custphot_uri);
                        simguserphoto = custphoto_path;

                        String[] p = custphoto_path.split("/");
                        int n = p.length;
                        String[] vals = {custphoto_path, p[n - 1], Constants.MULT_IMAGE};
                        Log.v("imagepath", custphoto_path + " imagename " + p[n - 1]);

                    } else {
                        System.out.println("data came as null bhoi!!");
                    }
                    break;
                case TAKE_ID_PHOTO:
                    Log.v("reqcode", "req code is " + reqCode);
                    if (resCode == getActivity().RESULT_OK && data != null) {
                        idphto_path = data.getExtras().getString("data");
                        idphoto_uri = Uri.parse(idphto_path);

                        idphoto.setImageURI(idphoto_uri);
                        sidphoto = idphto_path;

                        String[] p = idphto_path.split("/");
                        int n = p.length;
                        String[] vals = {idphto_path, p[n - 1], Constants.MULT_IMAGE};
                        Log.v("imagepath", idphto_path + " imagename " + p[n - 1]);

                    } else {
                        System.out.println("data came as null bhoi!!");
                    }
                    break;
                case TAKE_SIGNATURE_PHOTO:
                    Log.v("reqcode", "req code is " + reqCode);
                    if (resCode == getActivity().RESULT_OK && data != null) {
                        signphoto_path = data.getExtras().getString("data");
                        signphoto_uri = Uri.parse(signphoto_path);

                        signphoto.setImageURI(signphoto_uri);
                        ssignphoto = signphoto_path;

                        String[] p = signphoto_path.split("/");
                        int n = p.length;
                        String[] vals = {signphoto_path, p[n - 1], Constants.MULT_IMAGE};
                        Log.v("imagepath", signphoto_path + " imagename " + p[n - 1]);

                    } else {
                        System.out.println("data came as null bhoi!!");
                    }
                    break;
            }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        setnm = etnm.getText().toString();
        setsnm = etsnm.getText().toString();
        setdis = etdis.getText().toString();
        setstreet = etstreet.getText().toString();
        setadd = etadd.getText().toString();
        setjob = etjob.getText().toString();
        setserial = etserial.getText().toString();
        setimei = etimei.getText().toString();
        setphn = etphn.getText().toString();


        setidno = etidno.getText().toString();

        setotherphn = etotherphn.getText().toString();
        setmail = etmail.getText().toString();


        custinfo = new String[]{ftitle, fgender, setnm, setsnm, fregion, setdis, setstreet, setadd, setbdt, setjob, setphn,
                setserial,
                setimei, simguserphoto};
        custid = new String[]{fcitizen, setidno, fidtype, sidphoto};
        custcontact = new String[]{setotherphn, setmail, ssignphoto, schkconsent};

        outState.putStringArray(Constants.CUSTOMERINFO, custinfo);
        outState.putStringArray(Constants.CUSTOMERID, custid);
        outState.putStringArray(Constants.CUSTOMERCONTACTS, custcontact);
    }
}
