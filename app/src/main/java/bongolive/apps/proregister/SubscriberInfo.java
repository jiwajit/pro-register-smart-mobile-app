/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bongolive.apps.proregister;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import bongolive.apps.proregister.utils.Constants;
import bongolive.apps.proregister.utils.Log4Helper;
import bongolive.apps.proregister.utils.Validating;
import io.github.sporklibrary.Spork;
import io.github.sporklibrary.annotations.BindView;

/**
 * Simple Fragment used to display some meaningful content for each page in the sample's
 * {@link android.support.v4.view.ViewPager}.
 */
public class SubscriberInfo extends Fragment {

    private static final String KEY_TITLE = "title";
    private static final String KEY_INDICATOR_COLOR = "indicator_color";
    private static final String KEY_DIVIDER_COLOR = "divider_color";
    String[] finalvals = null, custinfo = null, custid = null, custcontact = null;
    String[] finalcheckvals = null;
    LinearLayout layinfo, layid, laycontac;
    Spinner sptitle, spgender, spidtype;
    String fgender, ftitle, fregion;
    AutoCompleteTextView autoregion;
    Logger log = Log4Helper.getLogger(SubscriberInfo.class.getSimpleName());
    private int rotationAngle;
    boolean clicked = false;

    Dialog dialog;
    SnapIt snapIt;
    EditText etnm, etsnm, etdis, etstreet, etadd, etjob, etphn, etserial, etimei;
    ImageView imguserphoto;
    TextView etbdt, etprefix;
    @BindView
    EditText etmname;
    //customer info
    String setnm, setmn, setsnm, setdis, setstreet, setadd, setbdt, setjob, setphn, setserial, setimei,
            simguserphoto; //customer info
    boolean dialogison = false;

    AppPreference appPreference;
    ArrayAdapter<String> titleadapter, genderadapter, regionadapter;

    private static final int TAKE_CUSTOMER_PHOTO = 3;
    private static Bitmap bitmap_cust = null;
    private static Uri custphot_uri;
    private static String custphoto_path;
    FrameLayout frameLayout;
    LinearLayout mContent;
    Button mClear, mGetSign, mCancel;
    private Camera mCamera;
    static boolean update;
    private String filePath = null, profileImageFilePath;

    /**
     * @return a new instance of {@link SubscriberInfo}, adding the parameters into a bundle and
     * setting them as arguments.
     */
    public static SubscriberInfo newInstance(boolean f) {
        Bundle bundle = new Bundle();
        update = f;
        SubscriberInfo fragment = new SubscriberInfo();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.subscriberinfofrag, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        Spork.bind(this);
        appPreference = new AppPreference(getActivity());
        sptitle = (Spinner) getActivity().findViewById(R.id.sptitle);
        spgender = (Spinner) getActivity().findViewById(R.id.spgender);

        autoregion = (AutoCompleteTextView) getActivity().findViewById(R.id.autoregion);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
        }

        /* customer information */
        etnm = (EditText) getActivity().findViewById(R.id.etname);
        etsnm = (EditText) getActivity().findViewById(R.id.etsname);
        etdis = (EditText) getActivity().findViewById(R.id.etdistrict);
        etstreet = (EditText) getActivity().findViewById(R.id.etstreet);
        etadd = (EditText) getActivity().findViewById(R.id.etadd);
        etbdt = (TextView) getActivity().findViewById(R.id.txtbdat);
        etjob = (EditText) getActivity().findViewById(R.id.etjob);
        etphn = (EditText) getActivity().findViewById(R.id.etphnoinfo);
        etphn.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String pn = etphn.getText().toString();
                    if (Validating.areSet(pn)) {
                        String pref1 = pn.substring(0, 1);
//                        if(pn.length()<9){
//                            etphn.setText("Write correct phone number");
//                        }
                        if (!pref1.equals("7"))
                            etphn.setText("");
                    }
                }
            }
        });
        final boolean[] seven = {false};
        boolean nine = false;
        boolean eight = false;
        final String pn = etphn.getText().toString();
        etphn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {
                /*if (s != null && s.length() > 0 && s.length() < 4) {
                    String number = s.toString();
                    if (number.startsWith("7")) {
                        if (number.length() > 1)
                            if (!number.startsWith("79")) {
                                etphn.setText("7");
                                if (number.length() > 2)
                                    if (!number.startsWith("798")) {
                                        etphn.setText("79");//if second number is not 4
                                        etphn.setSelection(etphn.length()); // move the cursor to 1
                                        // position
                                    }
                            }

                    } else
                        etphn.setText("");
                }*/
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkPhoneNumber(etphn);
            }
        });
        etserial = (EditText) getActivity().findViewById(R.id.etsimserial);
        etimei = (EditText) getActivity().findViewById(R.id.etimei);
        imguserphoto = (ImageView) getActivity().findViewById(R.id.imgphoto);
        if (update) {
//            imguserphoto.setVisibility(View.GONE);
//            etphn.setFocusable(false);
//            etphn.setFocusableInTouchMode(false);
        }


/*
        *//*
        *  retain state
        *//*

        Bundle instate = new Bundle();

        if(instate != null){
            if(instate.containsKey(Constants.SAVESTATE)) {
                String[] data = instate.getStringArray(Constants.SAVESTATE);
                System.out.println("on start and array length " + data.length);
            } else
                System.out.println("bundle is empty on activitycreated");
        }
        *//*
        *  end of state restoring
        */

        imguserphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (simguserphoto != null) {
                    preview_photo(simguserphoto);
                } else {
                    if (Constants.isSdMounted()) {
                        add_customer_photo();
                    } else {
                        Toast.makeText(getActivity(), "no sd card ", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        etbdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.setDateTime(getActivity(), etbdt);
            }
        });

        etbdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Constants.setDateTime(getActivity(), etbdt);
                }
            }
        });

        etbdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String minvalue = "1900-06-30";
                String maxvalue = "2014-06-30";
                setbdt = etbdt.getText().toString();

                if (Validating.areSet(setbdt)) {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        //    ////
                        LocalDate today = LocalDate.now();
                        LocalDate now = new LocalDate();
                        String age = now.toString();
//                        Toast.makeText(getActivity(), "TODAY DATE: "+age.substring(0 ,4), Toast.LENGTH_LONG).show();
                        //entered date value on info
                        Date entered = df.parse(setbdt);
                        String enteredDate = setbdt.substring(0, 4);
//                        Toast.makeText(getActivity(), "TODAY DATE: "+age.substring(0 ,4)+" ENTERED DATE: "+enteredDate, Toast.LENGTH_LONG).show();
                        Date mindate = df.parse(minvalue);
                        Date maxdate = df.parse(maxvalue);
                        Date common = df.parse("0000-00-00");
                        int enteredDateValue = Integer.parseInt(enteredDate);
                        int todayDate = Integer.parseInt(age.substring(0, 4));
                        int minimumDateValue = Integer.parseInt(minvalue.substring(0, 4));
                        //get difference between current date and entered date
                        int dateDifference = todayDate - enteredDateValue;
                        Log.v("dates", " MIN DATE " + mindate + " MAX DATE " +
                                maxdate + " entered value is " + entered);
//                        if (entered.compareTo(mindate) <= 0) {
//                            Toast.makeText(getActivity(), getString(R.string.strmin) + " " + minvalue, Toast.LENGTH_LONG).show();
//                            etbdt.setText(getString(R.string.strbdate));
//                            setbdt = "";
//                            Constants.setDateTime(getActivity(), etbdt);
//                        }
//                        if (entered.compareTo(common) != 0) {
//                            if (entered.compareTo(maxdate) > 0) {
//                                Toast.makeText(getActivity(), getString(R.string.strmax) + " " + maxvalue, Toast.LENGTH_LONG).show();
//                                etbdt.setText(getString(R.string.strbdate));
//                                setbdt = "";
//                                Constants.setDateTime(getActivity(), etbdt);
//                            }
//                        }

                        if (dateDifference < 0) {
                            Toast.makeText(getActivity(), getString(R.string.strmax) + " " + todayDate, Toast.LENGTH_LONG).show();
                            etbdt.setText(getString(R.string.strbdate));
                            setbdt = "";
                            Constants.setDateTime(getActivity(), etbdt);
                        }
                        if (0 <= dateDifference && dateDifference <= 17) {
                            Toast.makeText(getActivity(), getString(R.string.strmin1) + " "  , Toast.LENGTH_LONG).show();
                            etbdt.setText(getString(R.string.strbdate));
                            setbdt = "";
                            Constants.setDateTime(getActivity(), etbdt);
                        }
                        if ((enteredDateValue - minimumDateValue) < 0) {
                            Toast.makeText(getActivity(), getString(R.string.strmin) + " " + minimumDateValue, Toast.LENGTH_LONG).show();
                            etbdt.setText(getString(R.string.strbdate));
                            setbdt = "";
                            Constants.setDateTime(getActivity(), etbdt);
                        }
//                        if (dateDifference == 0) {
//                            Toast.makeText(getActivity(), getString(R.string.strbdate), Toast.LENGTH_LONG).show();
//                            etbdt.setText(getString(R.string.strbdate));
//                            setbdt = "";
//                            Constants.setDateTime(getActivity(), etbdt);
//                        }
                    } catch (ParseException e) {
                        log.warn("Date parsing exception " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });

        /* end of customer information*/

        final String[] titles = getResources().getStringArray(R.array.title);
        String[] gender = getResources().getStringArray(R.array.gender);
        String[] regions = getResources().getStringArray(R.array.allregions);

        titleadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, titles);
        genderadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, gender);
        regionadapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, regions);

        sptitle.setAdapter(titleadapter);
        spgender.setAdapter(genderadapter);
        autoregion.setAdapter(regionadapter);

        autoregion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                autoregion.setText(pro);
                fregion = pro;
            }
        });

        sptitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    ftitle = titleadapter.getItem(position);
                    if (ftitle.equals(titles[2]) || ftitle.equals(titles[3]) || ftitle.equals(titles[4])) {
                        spgender.setSelection(2);
                        fgender = genderadapter.getItem(2);
                    } else if (ftitle.equals(titles[1])) {
                        spgender.setSelection(1);
                        fgender = genderadapter.getItem(1);
                    } else {
                        spgender.setSelection(0);
                        fgender = "";
                    }
                } else {
                    ftitle = "";
                    fgender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spgender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    fgender = genderadapter.getItem(position);
                } else {
                    fgender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        autoregion.setThreshold(1);
    }


    public void checkPhoneNumber(EditText e) {
        String s = e.getText().toString().trim();
        if (Validating.areSet(s)) {
            if (s.length() > 0) {
                if (!s.startsWith("7")) {
                    e.setText("");
                    e.setError(getString(R.string.phonevalidation));
                    return;
                } else if (s.length() > 1) {
                    if (!s.startsWith("79")) {
                        e.setText("");
                        e.setError(getString(R.string.phonevalidation));
                        return;
                    } else if (s.length() > 2) {
                        if (!s.startsWith("79")) {
                            e.setText("");
                            e.setError(getString(R.string.phonevalidation));
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.TAKE_PHOTO:
                Log.e("URI", "Uri is " + custphot_uri);
                if (custphot_uri != null) {
                    String imageFilePath = custphot_uri.getPath();
                    Log.e("IMGPATH", "path is " + imageFilePath);
                    if (imageFilePath != null && imageFilePath.length() > 0) {
                        // File myFile = new File(imageFilePath);
                        custphoto_path = imageFilePath;
                        appPreference.saveSubscriberPhoto(custphoto_path);
                        Bitmap bitmap = Constants.decodeImg(custphoto_path, 150, 150);
                        if (bitmap == null) {
                            appPreference.clearPhotos(Constants.SUBPHOTO);
                            simguserphoto = null;
                        }
                        simguserphoto = custphoto_path;
                        imguserphoto.setImageBitmap(bitmap);
                        imguserphoto.setRotation(90);
                        try {
                            // if (bmp != null)
                            // bmp.recycle();
                            int mobile_width = 480;
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            // options.inJustDecodeBounds = true;
                            // BitmapFactory.decodeFile(imageFilePath, options);
                            int outWidth = options.outWidth;
                            // int outHeight = options.outHeight;
                            int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);

                            if (ratio == 0) {
                                ratio = 1;
                            }
                            ExifInterface exif = new ExifInterface(imageFilePath);

                            String orientString = exif
                                    .getAttribute(ExifInterface.TAG_ORIENTATION);
                            int orientation = orientString != null ? Integer
                                    .parseInt(orientString)
                                    : ExifInterface.ORIENTATION_NORMAL;
                            System.out.println("Orientation : " + orientation);
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
                                rotationAngle = 90;
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
                                rotationAngle = 180;
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
                                rotationAngle = 270;

                            System.out.println("Rotation : " + rotationAngle);

                            options.inJustDecodeBounds = false;
                            options.inSampleSize = ratio;

                            Bitmap bmp = BitmapFactory.decodeFile(imageFilePath,
                                    options);
                            File myFile = new File(imageFilePath);
                            FileOutputStream outStream = new FileOutputStream(
                                    myFile);
                            if (bmp != null) {
                                bmp.compress(Bitmap.CompressFormat.JPEG, 100,
                                        outStream);
                                outStream.flush();
                                outStream.close();

                                Matrix matrix = new Matrix();
                                matrix.setRotate(rotationAngle,
                                        (float) bmp.getWidth() / 2,
                                        (float) bmp.getHeight() / 2);

                                bmp = Bitmap.createBitmap(bmp, 0, 0,
                                        bmp.getWidth(), bmp.getHeight(), matrix,
                                        true);

                                // ivStuffPicture.setImageBitmap(bmp);

                                custphoto_path = MediaStore.Images.Media.insertImage(
                                        getActivity().getContentResolver(), bmp,
                                        Calendar.getInstance().getTimeInMillis()
                                                + ".jpg", null);
                                /*appPreference.saveSubscriberPhoto(custphoto_path);
                                Bitmap bitmap = Constants.decodeImg(custphoto_path, 150, 150);
                                if(bitmap == null) {
                                    appPreference.clearPhotos(Constants.SUBPHOTO);
                                    simguserphoto = null;
                                }
                                simguserphoto = custphoto_path;
                                imguserphoto.setImageBitmap(bitmap);*/
                            }

                        } catch (OutOfMemoryError e) {
                            System.out.println("out of bound");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(
                            getContext(),
                            getResources().getString(
                                    R.string.toast_unable_to_selct_image),
                            Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    private void add_customer_photos() {
        Calendar cal = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        custphot_uri = Uri.fromFile(file);
        Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, custphot_uri);
        startActivityForResult(i, Constants.TAKE_PHOTO);
    }

    public void add_customer_photo() {
        if (getActivity().getApplication().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.v("picture", "capturing picture");

            Intent intent = new Intent(getActivity(), PhotoTaker.class);
            intent.putExtra("source", 1);
            intent.putExtra("type", 1);
            startActivityForResult(intent, TAKE_CUSTOMER_PHOTO);
        } else {
            Toast.makeText(getActivity(), "this device does not have camera", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String path = appPreference.getSubPhoto();
        if (Validating.areSet(path)) {
            custphoto_path = path;
            custphot_uri = Uri.parse(custphoto_path);

            Bitmap bitmap = Constants.decodeImg(custphoto_path, 150, 150);
            if (bitmap == null) {
                appPreference.clearPhotos(Constants.SUBPHOTO);
                simguserphoto = null;
            }
            simguserphoto = custphoto_path;
            imguserphoto.setImageBitmap(bitmap);
//            imguserphoto.setRotation(90);
        }

        String t, g, fn, mn, sn, r, d, s, a, b, j, p, si, im;
        t = appPreference.getTitle();
        if (Validating.areSet(t)) {
            int selected = titleadapter.getPosition(t);
            sptitle.setSelection(selected);
        }
        g = appPreference.getGender();
        if (Validating.areSet(g)) {
            int selected = genderadapter.getPosition(t);
            spgender.setSelection(selected);
        }
        fn = appPreference.getFname();
        etnm.setText(fn);
        sn = appPreference.getSname();
        etsnm.setText(sn);
        mn = appPreference.getMname();
        etmname.setText(mn);
        r = appPreference.getRegion();
        fregion = r;
        autoregion.setText(fregion);
        d = appPreference.getDistrict();
        etdis.setText(d);
        s = appPreference.getStreet();
        etstreet.setText(s);
        a = appPreference.getAddress();
        etadd.setText(a);
        b = appPreference.getBdate();
        if (Validating.areSet(b))
            etbdt.setText(b);
        j = appPreference.getJob();
        etjob.setText(j);
        p = appPreference.getSmartNo();
        etphn.setText(p);
        si = appPreference.getSerial();
        etserial.setText(si);
        im = appPreference.getImei();
        etimei.setText(im);
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("paused info");
        if (!update) {
            setnm = etnm.getText().toString().trim();
            setsnm = etsnm.getText().toString().trim();
            setmn = etmname.getText().toString().trim();
            setdis = etdis.getText().toString().trim();
            setstreet = etstreet.getText().toString().trim();
            setadd = etadd.getText().toString().trim();
            setjob = etjob.getText().toString().trim();
            setserial = etserial.getText().toString().trim();
            setimei = etimei.getText().toString().trim();
            setphn = etphn.getText().toString().trim();

            appPreference.saveTitle(ftitle);
            appPreference.saveGender(fgender);
            appPreference.saveFn(setnm);
            appPreference.saveMn(setmn);
            appPreference.saveSn(setsnm);
            appPreference.saveReg(fregion);
            appPreference.saveDist(setdis);
            appPreference.saveStreet(setstreet);
            appPreference.saveAddress(setadd);
            appPreference.saveBdate(setbdt);
            appPreference.saveJob(setjob);
            appPreference.savePhone(setphn);
            appPreference.saveSerial(setserial);
            appPreference.saveImei(setimei);
//            etnm.setText("");
//            etsnm.setText("");
//            etmname.setText("");
//            etdis.setText("");
//            etstreet.setText("");
//            etadd.setText("");
//            etjob.setText("");
//            etserial.setText("");
//            etimei.setText("");
//            etphn.setText("");
        }

    }

    private void preview_photo(String photo) {
        add_customer_photo();
        /*LayoutInflater infl = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.phototaker, null);

        final Dialog dg = new Dialog(getContext(), R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);

        FrameLayout camera_view = (FrameLayout) dg.findViewById(R.id.camera_view);
        if(photo == null)
            dg.dismiss();

        ImageView imageView = new ImageView(dg.getContext());

        camera_view.addView(imageView);


        Bitmap bitmap = Constants.decodeImg(photo, 400, 400);
        if(bitmap != null) {
            imageView.setImageBitmap(bitmap);
            imageView.setRotation(90);
        }

        ImageButton imgClose = (ImageButton) dg.findViewById(R.id.imgClose);
        ImageButton imgSave = (ImageButton) dg.findViewById(R.id.imgTake);

        Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(),R.drawable.photo);
        imgSave.setImageBitmap(bitmap1);
        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_customer_photo();
                dg.dismiss();
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dg.dismiss();
            }
        });
        dg.show();*/
    }
}
